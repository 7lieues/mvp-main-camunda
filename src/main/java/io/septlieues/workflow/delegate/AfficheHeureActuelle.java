package io.septlieues.workflow.delegate;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class AfficheHeureActuelle implements JavaDelegate {
    private static final Logger LOG = LoggerFactory.getLogger(AfficheHeureActuelle.class);
    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {
        // System.out.println("********** Classe : AfficheHeureActuelle **********");
        SimpleDateFormat s = new SimpleDateFormat("HH:mm:ss");
        Date date = new Date();
        System.out.println("********** Il est actuellement " + s.format(date) + " **********");
    }
}
