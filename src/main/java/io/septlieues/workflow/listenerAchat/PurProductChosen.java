package io.septlieues.workflow.listenerAchat;


import io.septlieues.workflow.services.RasaService;
import io.septlieues.workflow.utils.ProcessConstantsAchat;
import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.delegate.DelegateTask;
import org.camunda.bpm.engine.delegate.TaskListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

@Component
public class PurProductChosen implements TaskListener{

    private static final Logger LOG = LoggerFactory.getLogger(PurProductChosen.class);

    @Autowired
    IdentityService identityService;

    @Autowired
    private RasaService rasaService;

    @Autowired
    private Environment env;

    @Autowired
    private JavaMailSender javaMailSender;

    @Value("${rasa.event.slot.task_id_pur.name}")
    private String rasaTaskIdSlot;

    @Override
    public void notify(DelegateTask delegateTask) {

        // On recup le taskId et on l'écrit dans le context Cam
        String taskId = delegateTask.getId();
        delegateTask.getExecution().setVariable(ProcessConstantsAchat.TASK_ID,taskId);
        LOG.info("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        LOG.info("!!!!!!!!!" + taskId);
        LOG.info("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");

        try {

            // On crée la requete set slot du taskId
            String TrackerId = (String) delegateTask.getExecution().getVariable(ProcessConstantsAchat.TRACKER_ID);
            LOG.info("!!!!!!!!!! Tracker ID : " + TrackerId);
            LOG.info("!!!!!!!!!! Task ID : " + taskId);
            LOG.info("!!!!!!!!!! rasa.event.slot.task_id_pur.name : " + rasaTaskIdSlot);

            // On execute le set slot du taskId
            rasaService.rasaSetSlot(rasaTaskIdSlot, taskId, TrackerId);

        } catch (Exception e) {
            LOG.warn("Could not send email to assignee", e);
        }
    }
}
