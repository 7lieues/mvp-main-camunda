package io.septlieues.workflow.listenerAchat;


import io.septlieues.workflow.model.RequestStatus;
import io.septlieues.workflow.services.UmbrellaNotification;
import io.septlieues.workflow.utils.ProcessConstantsAchat;
import org.camunda.bpm.engine.HistoryService;
import org.camunda.bpm.engine.ProcessEngines;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.ExecutionListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class ExecutionEndListenerAchat implements ExecutionListener {
    private static final Logger LOG = LoggerFactory.getLogger(ExecutionStartListenerAchat.class);
    @Autowired
    RestTemplate restTemplate;

    @Value("${umbrella.host}")
    private String umbrellaHost;
    @Autowired
    private UmbrellaNotification umbrellaNotification;

    @Override
    public void notify(DelegateExecution delegateExecution) throws Exception {
        String process_definition_id = (String) delegateExecution.getProcessDefinitionId();
        String trackerId = (String) delegateExecution.getVariable(ProcessConstantsAchat.TRACKER_ID);
        String applicant = (String) delegateExecution.getVariable(ProcessConstantsAchat.APPLICANT);
        String step_name = "Fin du workflow";
        String request_id = null;
        if (trackerId.contains("::")) {
            request_id = trackerId.split("::")[1];
        } else request_id = trackerId;
        String process_ext_id = delegateExecution.getProcessInstanceId();
        HistoryService historyService = ProcessEngines.getDefaultProcessEngine().getHistoryService();
        int historicalStepCount = historyService.createHistoricTaskInstanceQuery()
                .processInstanceId(process_ext_id).list().size();
        LOG.info("================================historicalStepCount================================ "+historicalStepCount);
        String requestId = trackerId.split("::")[1];
        try {
            umbrellaNotification.umbrellaStartRequest(requestId, RequestStatus.finished.getStatus(), process_ext_id, process_definition_id,"4","4",step_name,applicant);
        } catch (Exception e) {
            LOG.error("ERROR TO SEND REQUEST UMBRELLA FINISH USER TASK "+e);
        }
    }
}
