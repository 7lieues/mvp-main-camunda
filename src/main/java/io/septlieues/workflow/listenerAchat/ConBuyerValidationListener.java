package io.septlieues.workflow.listenerAchat;


import io.septlieues.workflow.model.RasaEvent;
import io.septlieues.workflow.services.RasaService;
import io.septlieues.workflow.utils.ProcessConstantsAchat;
import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.delegate.DelegateTask;
import org.camunda.bpm.engine.delegate.TaskListener;
import org.camunda.bpm.engine.identity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;


@Component
public class ConBuyerValidationListener implements TaskListener {

    private static final Logger LOG = LoggerFactory.getLogger(ConBuyerValidationListener.class);

    @Autowired
    IdentityService identityService;

    @Autowired
    private RasaService rasaService;

    @Autowired
    private Environment env;

    @Autowired
    private JavaMailSender javaMailSender;

    // Slot à créer coté rasa à différencier du slot taskid Mng
    @Value("${rasa.event.slot.task_id_pur.name}")
    private String rasaTaskIdSlot;

    @Override
    public void notify(DelegateTask delegateTask) {

        // On recup l'assignee et le taskId, puis on écrit le taskId dans le contexte Cam
        String assignee = delegateTask.getAssignee();
        String taskId = delegateTask.getId();
        delegateTask.getExecution().setVariable(ProcessConstantsAchat.TASK_ID,taskId);
        LOG.info("********** Task ID : " + taskId);

        if (assignee != null) {
            User user = identityService.createUserQuery().userId(assignee).singleResult();
            if (user != null) {
                try {

                    // On crée l'event, le header, puis l'url de la requete
                    RasaEvent rasaEvent = new RasaEvent("slot", rasaTaskIdSlot, taskId);
                    HttpHeaders requestHeader = new HttpHeaders();
                    HttpEntity<RasaEvent> requestEntitySetSlot = new HttpEntity<>(rasaEvent, requestHeader);
                    
                    // On recup du context Cam le acheteurTrackerId, puis on s'en sert pour set slot le taskId coté rasa
                    String acheteurTrackerId = (String) delegateTask.getExecution().getVariable(ProcessConstantsAchat.ACHETEUR_TRACKER_ID);
                    rasaService.setSlot(acheteurTrackerId,requestEntitySetSlot);
                    
                    // En parallele on envoi le mail de notif
                    // SimpleMailMessage mail = new SimpleMailMessage();
                    // LOG.info("********** Mail manager à notifier : "+user.getEmail());
                    // mail.setTo(user.getEmail());
                    // mail.setFrom(env.getProperty("spring.mail.username"));
                    // mail.setSubject("Test user task.");
                    // mail.setText("Lien vers le user task : https://dev.7lieues.io/mvp-achat-camunda/app/tasklist/default/#/task=" + taskId);
                    // javaMailSender.send(mail);
                    // LOG.info("***************Done**************");

                } catch (Exception e) {
                    LOG.warn("Could not send email to assignee", e);
                }

            } else {
                    LOG.info("user is null");
            }
        }
    }
}