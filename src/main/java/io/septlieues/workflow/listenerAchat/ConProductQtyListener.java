package io.septlieues.workflow.listenerAchat;


import io.septlieues.workflow.services.RasaService;
import io.septlieues.workflow.utils.ProcessConstantsAchat;
import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.delegate.DelegateTask;
import org.camunda.bpm.engine.delegate.TaskListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
public class ConProductQtyListener implements TaskListener{

    // Récupération du task ID et envoi à RASA

    private static final Logger LOG = LoggerFactory.getLogger(ConProductQtyListener.class);

    @Autowired
    IdentityService identityService;

    @Autowired
    private RasaService rasaService;

    @Autowired
    private Environment env;

    @Value("${rasa.event.con.task_id.name}")
    private String rasaTaskIdSlot;

    @Override
    public void notify(DelegateTask delegateTask) {

        // Récupération du task ID
        String trackerId = (String) delegateTask.getExecution().getVariable(ProcessConstantsAchat.TRACKER_ID);
        String taskId = delegateTask.getId();
        delegateTask.getExecution().setVariable(ProcessConstantsAchat.TASK_ID,taskId);

        try {
            // On crée la requete set slot du taskId
            rasaService.rasaSetSlot(rasaTaskIdSlot, taskId, trackerId);
        } catch (Exception e) {
            LOG.warn("Could not send email to assignee", e);
        }
    }
}
