package io.septlieues.workflow.listenerAchat;


import io.septlieues.workflow.model.RequestStatus;
import io.septlieues.workflow.services.UmbrellaNotification;
import io.septlieues.workflow.utils.ProcessConstantsAchat;
import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.ExecutionListener;
import org.camunda.bpm.engine.identity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;


@Component
public class ExecutionStartListenerAchat implements ExecutionListener {
    private static final Logger LOG = LoggerFactory.getLogger(ExecutionStartListenerAchat.class);
    @Autowired
    RestTemplate restTemplate;

    @Value("${umbrella.host}")
    private String umbrellaHost;
    @Autowired
    private UmbrellaNotification umbrellaNotification;

    @Autowired
    IdentityService identityService;

    @Override
    public void notify(DelegateExecution delegateExecution) throws Exception {


        LOG.info("!!!!!!!!!!!!!!! checkpoint executestartlistener !!!!!!!!!!!!!!");
        String process_definition_id = (String) delegateExecution.getProcessDefinitionId();
        String trackerId = (String) delegateExecution.getVariable(ProcessConstantsAchat.TRACKER_ID);
        String adressMail = trackerId.split("::")[0];
        //String eventName = delegateExecution.getEventName();
        String step_name = "Initialisation du workflow";
        String request_id = null;
        if (trackerId.contains("::")) {
            request_id = trackerId.split("::")[1];
        } else request_id = trackerId;
        String process_ext_id = delegateExecution.getProcessInstanceId();
        /*HistoryService historyService = ProcessEngines.getDefaultProcessEngine().getHistoryService();
        int historicalStepCount = historyService.createHistoricTaskInstanceQuery()
                .processInstanceId(process_ext_id).list().size();
        LOG.info("================================historicalStepCount================================ "+historicalStepCount);*/
        User user = identityService.createUserQuery().userId(adressMail).singleResult();
        String applicant = user.getFirstName();
        LOG.info("applicant: " + applicant);
        delegateExecution.setVariable(ProcessConstantsAchat.APPLICANT,applicant);
        String test = (String) delegateExecution.getVariable(ProcessConstantsAchat.APPLICANT);
        LOG.info("test: "+ test);
        String requestId = trackerId.split("::")[1];
        try {
            umbrellaNotification.umbrellaStartRequest(requestId, RequestStatus.started.getStatus(), process_ext_id, process_definition_id,"1","1",step_name,applicant);
        } catch (Exception e) {
            LOG.error("ERROR TO SEND REQUEST UMBRELLA START PROCESS " + e);
        }
    }
}
