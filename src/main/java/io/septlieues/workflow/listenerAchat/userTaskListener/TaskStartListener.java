package io.septlieues.workflow.listenerAchat.userTaskListener;


import io.septlieues.workflow.model.RequestStatus;
import io.septlieues.workflow.services.UmbrellaNotification;
import io.septlieues.workflow.utils.ProcessConstantsAchat;
import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.delegate.DelegateTask;
import org.camunda.bpm.engine.delegate.TaskListener;
import org.camunda.bpm.engine.identity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class TaskStartListener implements TaskListener {
    // private static final Logger LOG = LoggerFactory.getLogger(TaskStartListener.class);
    private static final Logger LOG = LoggerFactory.getLogger(TaskCompleteListener.class);

    @Autowired
    RestTemplate restTemplate;

    @Value("${umbrella.host}")
    private String umbrellaHost;


    @Autowired
    private UmbrellaNotification umbrellaNotification;

    @Autowired
    IdentityService identityService;

    @Override
    public void notify(DelegateTask delegateTask) {

        LOG.info("!!!!!!!!!!!!! checkpoint start listener !!!!!!!!!!!!!!!!");

        String process_definition_id = (String) delegateTask.getProcessDefinitionId();
        String trackerId = (String) delegateTask.getExecution().getVariable(ProcessConstantsAchat.TRACKER_ID);
        String step_name = "Attente validation manager";

        String request_id = null;
        String adressMail = trackerId.split("::")[0];
        if (trackerId.contains("::")) {
            request_id = trackerId.split("::")[1];
        } else request_id = trackerId;
        String process_ext_id = delegateTask.getProcessInstanceId();
        try {
            String requestId = trackerId.split("::")[1];
            User user = identityService.createUserQuery().userId(adressMail).singleResult();
            String applicant = user.getFirstName();
            delegateTask.getExecution().setVariable(ProcessConstantsAchat.APPLICANT,applicant);
            LOG.info("requestId   ="+requestId);
            LOG.info("applicantTest   ="+applicant);
            LOG.info("RequestStatus.before_validation.getStatus()  ="+RequestStatus.before_validation.getStatus());
            LOG.info("process_ext_id  ="+process_ext_id);
            LOG.info("process_definition_id  ="+process_definition_id);
            umbrellaNotification.umbrellaStartRequest(requestId, RequestStatus.before_validation.getStatus(), process_ext_id, process_definition_id,"2","2",step_name,applicant);

        } catch (Exception e) {
            LOG.error("ERROR TO SEND REQUEST UMBRELLA START USER TASK "+e);
        }
    }
}
