package io.septlieues.workflow.listenerAchat.userTaskListener;


import io.septlieues.workflow.services.UmbrellaNotification;
import io.septlieues.workflow.utils.ProcessConstantsAchat;
import org.camunda.bpm.engine.delegate.DelegateTask;
import org.camunda.bpm.engine.delegate.TaskListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import io.septlieues.workflow.model.RequestStatus;

@Component
public class TaskCompleteListener implements TaskListener {
    //private static final java.util.logging.Logger LOG = LoggerFactory.getLogger(TaskCompleteListener.class);
    private static final Logger LOG = LoggerFactory.getLogger(TaskCompleteListener.class);


    @Autowired
    RestTemplate restTemplate;

    @Value("${umbrella.host}")
    private String umbrellaHost;


    @Autowired
    private UmbrellaNotification umbrellaNotification;

    @Override
    public void notify(DelegateTask delegateTask) {

        LOG.info("!!!!!!!!!!!!!!!! checkpoint complete !!!!!!!!!!!!!!!!");
        LOG.info("cocoo");
        String process_definition_id = (String) delegateTask.getProcessDefinitionId();
        String trackerId = (String) delegateTask.getExecution().getVariable(ProcessConstantsAchat.TRACKER_ID);
        String applicant = (String) delegateTask.getVariable(ProcessConstantsAchat.APPLICANT);
        LOG.info("manager applicant :" + applicant);
        String AdressMailManager = (String) delegateTask.getVariable(ProcessConstantsAchat.MANAGER);
        String step_name = "Validation manager effectuée";
        String request_id = null;
        if (trackerId.contains("::")) {
            request_id = trackerId.split("::")[1];
        } else request_id = trackerId;
        String process_ext_id = delegateTask.getProcessInstanceId();
        String manager_decision = null;
        Boolean approved = (Boolean) delegateTask.getExecution().getVariable(ProcessConstantsAchat.APPROVED);
        if (approved) {
            manager_decision = "1::".concat(AdressMailManager);
            LOG.info("manager decision:" + approved);
        } else {
            manager_decision = "0::".concat(AdressMailManager);
        }
         try {
             String requestId = trackerId.split("::")[1];
             LOG.info("manager RequestStatus.on_validation.getStatus() :" + RequestStatus.on_validation.getStatus());
             LOG.info("manager process_ext_id :" + process_ext_id);
             LOG.info("manager process_definition_id :" + process_definition_id);
             LOG.info("manager manager_decision :" + manager_decision);
             LOG.info("manager step_name :" + step_name);
             LOG.info("manager requestId :" + requestId);
             //umbrellaNotification.umbrellaStartRequest(requestId, RequestStatus.before_validation.getStatus(), process_ext_id, process_definition_id,"2","2",step_name,applicant);
            umbrellaNotification.umbrellaCompleteRequest(requestId, approved, RequestStatus.on_validation.getStatus(), process_ext_id, process_definition_id, manager_decision,"3","3",step_name, applicant);
        } catch (Exception e) {
             LOG.error("ERROR TO SEND REQUEST UMBRELLA COMPLETE USER TASK ");
        }

    }

}
