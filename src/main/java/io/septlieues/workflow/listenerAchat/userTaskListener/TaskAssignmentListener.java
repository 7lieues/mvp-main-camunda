package io.septlieues.workflow.listenerAchat.userTaskListener;


import io.septlieues.workflow.services.RasaService;
import io.septlieues.workflow.utils.ProcessConstantsAchat;
import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.delegate.DelegateTask;
import org.camunda.bpm.engine.delegate.TaskListener;
import org.camunda.bpm.engine.identity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

@Component
public class TaskAssignmentListener implements TaskListener{

    private static final Logger LOG = LoggerFactory.getLogger(TaskAssignmentListener.class);

    @Autowired
    IdentityService identityService;

    @Autowired
    private RasaService rasaService;

    @Autowired
    private Environment env;

    @Autowired
    private JavaMailSender javaMailSender;

    @Value("${rasa.event.slot.task_id_pur.name}")
    private String rasaTaskIdSlot;

    @Override
    public void notify(DelegateTask delegateTask) {

        String assignee = delegateTask.getAssignee();
        String taskId = delegateTask.getId();
        delegateTask.getExecution().setVariable(ProcessConstantsAchat.TASK_ID,taskId);
        String managerTrackerId = (String) delegateTask.getExecution().getVariable(ProcessConstantsAchat.MANAGER_TRACKER_ID);
        LOG.info("oooooooooooooooooooooooooooo le manager tracker id:"+ managerTrackerId);
        
        if (assignee != null) {
            User user = identityService.createUserQuery().userId(assignee).singleResult();
            if (user != null) {
                try {
                    rasaService.rasaSetSlot(rasaTaskIdSlot, taskId, managerTrackerId);
                } catch (Exception e) {
                    LOG.warn("Could not send email to assignee", e);
                }

            } else {
                LOG.info("user is null");
            }
        }
    }
}
