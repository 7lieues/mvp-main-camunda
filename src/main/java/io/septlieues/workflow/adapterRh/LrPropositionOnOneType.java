package io.septlieues.workflow.adapterRh;

import com.google.gson.Gson;

import io.septlieues.workflow.model.RasaEvent;
import io.septlieues.workflow.services.BddService;
import io.septlieues.workflow.services.RasaService;
import io.septlieues.workflow.services.UmbrellaNotification;
import io.septlieues.workflow.utils.ProcessConstantsRh;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

/*une class qui informe l'utilisateur du rejet de sa demande de congé suite à une insuffisance du solde de congé par mail */

@Component
public class LrPropositionOnOneType implements JavaDelegate {
  private static final Logger LOG = LoggerFactory.getLogger(LrPropositionOnOneType.class);

  @Autowired
  private RasaService rasaService;
  @Autowired
  private Environment env;

  @Autowired
  private BddService bddService;

  @Autowired
  private Gson gson;

  @Autowired
  private UmbrellaNotification umbrellaNotification;

  @Value("${rasa.recap.conges.proposition}")
  private String rasaRecapCongesProposition;

  @Value("${rasa.recap.solde.proposition}")
  private String rasaRecapSoldeProposition;

  @Value("${rasa.proposition.on.one.type}")
  private String rasaUtterName;

  @Override
  public void execute(DelegateExecution ctx) throws Exception {

    String TrackerId = (String) ctx.getVariable(ProcessConstantsRh.TRACKER_ID_LR);
    String email = TrackerId.split(":")[0];
    LOG.info("********* mail du user : " + email);

    String conges = (String) ctx.getVariable(ProcessConstantsRh.CONGES);
    String solde = (String) ctx.getVariable(ProcessConstantsRh.SOLDE);

    HttpHeaders requestHeader = new HttpHeaders();
    requestHeader.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);


    RasaEvent rasaEvent = new RasaEvent("slot", rasaRecapCongesProposition, conges);
    HttpEntity<RasaEvent> requestEntitySetSlot = new HttpEntity<>(rasaEvent, requestHeader);

    RasaEvent rasaEvent2 = new RasaEvent("slot", rasaRecapSoldeProposition, solde);
    HttpEntity<RasaEvent> requestEntitySetSlot2 = new HttpEntity<>(rasaEvent2, requestHeader);


    rasaService.setSlot(TrackerId, requestEntitySetSlot);
    rasaService.setSlot(TrackerId, requestEntitySetSlot2);
    
    rasaService.utter(rasaUtterName, TrackerId);
  }
}
