package io.septlieues.workflow.adapterRh;

import com.google.gson.Gson;

import io.septlieues.workflow.services.BddService;
import io.septlieues.workflow.services.RasaService;
import io.septlieues.workflow.utils.ProcessConstantsRh;
import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;


@Component
public class LrNotifUserDenyProposition implements JavaDelegate {
    private static final Logger LOG = LoggerFactory.getLogger(LrNotifUserDenyProposition.class);

    @Autowired
    private RasaService rasaService;

    @Autowired
    IdentityService identityService;

    @Autowired
    private Environment env;

    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    private Gson gson;

    @Autowired
    private BddService bddService;
    
    @Value("${rasa.notify.user.deny.proposition}")
    private String rasaUtterName;


    @Override
    public void execute(DelegateExecution ctx) throws Exception {

        // On récup du context Cam le trackerId puis on fabrique le mail du user avec.
        String TrackerId = (String) ctx.getVariable(ProcessConstantsRh.TRACKER_ID_LR);
        String email = TrackerId.split(":")[0];


        // On execute le utter lié à rasaUtterName lié plus haut dans @Value
        try {
            rasaService.utter(rasaUtterName, TrackerId);
        } catch (Exception e) {
            LOG.error("error when notify user : " + TrackerId, e);
        }

        // On envoie le mail de refus au mail fabriqué plus haut.
        try {
            SimpleMailMessage mail = new SimpleMailMessage();
            LOG.info("********** Email user : "+ email);
            mail.setTo(email);
            mail.setFrom(env.getProperty("spring.mail.username"));
            mail.setSubject("refus proposition congés.");
            mail.setText("proposition de congé refusée.");
            // javaMailSender.send(mail);
        } catch (Exception e) {
            LOG.warn("Could not send email to user", e);
        }

        }

}
