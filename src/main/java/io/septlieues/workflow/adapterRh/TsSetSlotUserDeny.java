package io.septlieues.workflow.adapterRh;

import com.google.gson.Gson;

import io.septlieues.workflow.model.RasaEvent;
import io.septlieues.workflow.services.RasaService;
import io.septlieues.workflow.utils.ProcessConstantsRh;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

@Component
public class TsSetSlotUserDeny implements JavaDelegate {
    private static final Logger LOG = LoggerFactory.getLogger(TsSetSlotUserDeny.class);

    @Autowired
    private RasaService rasaService;

    @Autowired
    private Gson gson;

    @Value("${rasa.deny.comment.ts.user}")
    private String rasaDenyCommentTsUser;

    @Override
    public void execute(DelegateExecution ctx) throws Exception {

        String deny_comment = (String) ctx.getVariable(ProcessConstantsRh.COMMENT);
        //si un commentaire doit etre ajouté du coté rasa on peut facilement ajouté comment dans le ProcessConstant class aprés getVariable
        String trackerId = (String) ctx.getVariable(ProcessConstantsRh.TRACKER_ID_TS);

        try {
            //création header
            HttpHeaders requestHeader = new HttpHeaders();
            requestHeader.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);

            // affichage du commentaire de refus dans les logs
            LOG.info("********** timeSheet Deny : " + deny_comment);

            //paramétrage slot coté rasa "ent_slot_ts_user_comment" avec le commentaire de refus manager
            RasaEvent rasaEvent = new RasaEvent("slot", rasaDenyCommentTsUser, deny_comment);
            HttpEntity<RasaEvent> requestEntitySetSlot = new HttpEntity<>(rasaEvent, requestHeader);
            
            //deploiement requête
            rasaService.setSlot(trackerId, requestEntitySetSlot);

        } catch (Exception e) {
            LOG.error("error when set slot user deny timeSheet  : " + deny_comment, e);

        }

    }
}
