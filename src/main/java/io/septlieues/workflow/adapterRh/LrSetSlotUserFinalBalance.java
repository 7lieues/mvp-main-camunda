package io.septlieues.workflow.adapterRh;


import io.septlieues.workflow.model.RasaEvent;
import io.septlieues.workflow.services.BddService;
import io.septlieues.workflow.services.RasaService;
import io.septlieues.workflow.services.UmbrellaNotification;
import io.septlieues.workflow.utils.ProcessConstantsRh;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

/* une classe qui prépare le slot qui contient le solde final (aprés retrait ) et l'envoie à Rasa */

@Component
public class LrSetSlotUserFinalBalance implements JavaDelegate {
  private static final Logger LOG = LoggerFactory.getLogger(LrSetSlotUserFinalBalance.class);
  @Autowired
  private BddService bddService;
  @Autowired
  private RasaService rasaService;
  
  @Autowired
  private UmbrellaNotification umbrellaNotification;

  @Value("${rasa.user.final.balance}")
  private String rasaUserFinalBalance;

  @Override
  public void execute(DelegateExecution ctx) throws Exception {
    //récupération depuis le contexte de camunda
    String final_balance = (String) ctx.getVariable(ProcessConstantsRh.FINAL_BALANCE);
    String trackerId = (String) ctx.getVariable(ProcessConstantsRh.TRACKER_ID_LR);

    try {
      //création header
      HttpHeaders requestHeader = new HttpHeaders();
      requestHeader.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
      
      //paramétrage demande rasa de remplir le slot "ent_lr_user_final_balance" avec le final balance
      RasaEvent rasaEvent = new RasaEvent("slot",rasaUserFinalBalance,final_balance);
      HttpEntity<RasaEvent> requestEntitySetSlot = new HttpEntity<>( rasaEvent,requestHeader);
      
      //déploiement de la requête
      rasaService.setSlot(trackerId, requestEntitySetSlot);

    } catch (Exception e) {
      LOG.error("error when set slot rasa for final balance  : " + final_balance,e );
    }
  }
}
