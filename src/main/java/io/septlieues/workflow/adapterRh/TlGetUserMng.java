package io.septlieues.workflow.adapterRh;


import io.septlieues.workflow.services.KeycloakUserService;
import io.septlieues.workflow.utils.ProcessConstantsRh;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.camunda.bpm.extension.keycloak.json.JSONArray;
import org.camunda.bpm.extension.keycloak.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

// une classe pour avoir le manager de user j'ai gardé le mme code,
// car je vais recevoir un track Id pareil de la part de rasa et aprés on récupère le mail manager du keycklock
@Component
public class TlGetUserMng implements JavaDelegate {
  private static final Logger LOG = LoggerFactory.getLogger(TlGetUserMng.class);

  @Autowired
  KeycloakUserService keycloakUserService;

  @Override
  public void execute(DelegateExecution ctx) throws Exception {

    // On récup le tracker id à partir du context Cam, puis récupérer certaines parties avec des split qu'on va stoker dans les variables suivants : 
    // adressMail, trackerTemp, requestID
    String trackerId = (String) ctx.getVariable(ProcessConstantsRh.TRACKER_ID_TL);
    String addressMail = trackerId.split("::")[0];
    String trackerTemp = trackerId.split("::")[1];
    String requestID = trackerTemp.split("::")[0];

    try {
      // On utilise le service keycloak afin de récupérer les données du ldap
      String manager = ((JSONArray) new JSONObject(keycloakUserService.getKeycloakUserAttributesByUserEmail(addressMail)).get("manager")).getString(0);
      String managerEmail = keycloakUserService.searchUserByCustomAttribute(
        "LDAP_ENTRY_DN",
        manager
      );

      // On va ensuite écrire le mail du manager et l'id de la requête dans le contexte Camunda
      ctx.setVariable(ProcessConstantsRh.MANAGER_EMAIL, managerEmail);
      ctx.setVariable(ProcessConstantsRh.REQUEST_ID, requestID);
      String BU = (String) ProcessConstantsRh.BU;
      LOG.info("********** BU  : " + BU);
      LOG.info("********** Mail du manager : " + managerEmail);
      
      // On va créer le trackerId du manager à partir de son email, puis on l'écrit dans le context Camunda
      String managerTrackerId = managerEmail.concat("::").concat(requestID).concat("::").concat("1").concat("::").concat(BU);
      ctx.setVariable(ProcessConstantsRh.MANAGER_TRACKER_ID, managerTrackerId);
      LOG.info("********** Tracker ID manager : " + managerTrackerId);
    
    } catch (Exception e) {
      LOG.error(" can not find user ", e);
    }
  }
}
