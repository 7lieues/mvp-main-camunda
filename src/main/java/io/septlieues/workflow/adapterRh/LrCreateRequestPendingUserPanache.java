package io.septlieues.workflow.adapterRh;

import io.septlieues.workflow.model.RasaEvent;
import io.septlieues.workflow.services.BddService;
import io.septlieues.workflow.services.RasaService;
import io.septlieues.workflow.services.UmbrellaNotification;
import io.septlieues.workflow.utils.ProcessConstantsRh;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

/* une classe qui prépare le slot qui contient le solde final (aprés retrait ) et l'envoie à Rasa */

@Component
public class LrCreateRequestPendingUserPanache implements JavaDelegate {
  private static final Logger LOG = LoggerFactory.getLogger(LrCreateRequestPendingUserPanache.class);
  @Autowired
  private BddService bddService;
  @Autowired
  private RasaService rasaService;

  @Autowired
  private UmbrellaNotification umbrellaNotification;

  @Value("${rasa.user.final.balance}")
  private String rasaUserFinalBalance;

  @Override
  public void execute(DelegateExecution ctx) throws Exception {
    //récupération depuis le contexte de camunda
    String trackerId = (String) ctx.getVariable(ProcessConstantsRh.TRACKER_ID_LR);
    LOG.info("!!!!!!!!!!!!! trackerId = " + trackerId);
    String final_balance = (String) ctx.getVariable(ProcessConstantsRh.FINAL_BALANCE);
    LOG.info("!!!!!!!!!!!!! final balance = " + final_balance);
    String firstLeaveType=(String) ctx.getVariable(ProcessConstantsRh.FIRST_LEAVE_TYPE);
    LOG.info("!!!!!!!!!!!!! firstLeaveType = " + firstLeaveType);
    String secondLeaveType=(String) ctx.getVariable(ProcessConstantsRh.SECOND_LEAVE_TYPE);
    LOG.info("!!!!!!!!!!!!! secondLeaveType = " + secondLeaveType);

    String firstStock = (String) ctx.getVariable(ProcessConstantsRh.FIRST_BALANCE);
    int firstStockInt = Integer.parseInt(firstStock);
    String secondStock = (String) ctx.getVariable(ProcessConstantsRh.SECOND_BALANCE);
    int secondStockInt = Integer.parseInt(secondStock);
    String leaveDays = (String) ctx.getVariable(ProcessConstantsRh.LEAVE_NUMBER_DAYS);
    int leaveDaysInt = Integer.parseInt(leaveDays);

    String LeaveStart = (String) ctx.getVariable(ProcessConstantsRh.LEAVE_START);

    secondStockInt = secondStockInt - (leaveDaysInt - firstStockInt);

    int nbrDaysSecondCreate = leaveDaysInt - firstStockInt;
    LOG.info("second stock int : " + nbrDaysSecondCreate);

    String requestID = "";

    LOG.info("!!!!!!!!!!!!! firstStock = " + firstStock);
    LOG.info("!!!!!!!!!!!!! secondStock = " + secondStock);
    LOG.info("!!!!!!!!!!!!! leaveDays = " + leaveDays);


    JSONObject CreateRequest = umbrellaNotification.createLeaveRequestNbrDays(trackerId, firstLeaveType, LeaveStart, firstStockInt, 0);
    LOG.info("Json create n°1 : " + CreateRequest);
    LeaveStart = (String) CreateRequest.get(String.valueOf("end_date_next_create"));
    requestID += CreateRequest.get(String.valueOf("leave_request_id")) + ",";
    LOG.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
    LOG.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
    
    JSONObject CreateRequest2 = umbrellaNotification.createLeaveRequestNbrDays(trackerId, secondLeaveType, LeaveStart, nbrDaysSecondCreate, 0);
    requestID += CreateRequest2.get(String.valueOf("leave_request_id")) + ",";
    LOG.info("Json create n°2 : " + CreateRequest2);

    int tarte = requestID.lastIndexOf(",");
    requestID = requestID.substring(0, tarte);
    LOG.info("liste de request id : " + requestID);
    ctx.setVariable(ProcessConstantsRh.LEAVE_REQUEST_ID, requestID);

    String second_balance = String.valueOf(secondStockInt);

    try {
      //création header
      HttpHeaders requestHeader = new HttpHeaders();
      requestHeader.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);

      //paramétrage demande rasa de remplir le slot "ent_lr_user_final_balance" avec le final balance
      RasaEvent rasaEvent = new RasaEvent("slot",rasaUserFinalBalance,second_balance);
      HttpEntity<RasaEvent> requestEntitySetSlot = new HttpEntity<>( rasaEvent,requestHeader);

      //déploiement de la requête
      rasaService.setSlot(trackerId, requestEntitySetSlot);

    } catch (Exception e) {
      LOG.error("error when set slot rasa for final balance  : " + second_balance,e );
    }
  }
}
