package io.septlieues.workflow.adapterRh;

import io.septlieues.workflow.services.RasaService;
import io.septlieues.workflow.utils.ProcessConstantsRh;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

@Component
public class TlNotifUserDenyType implements JavaDelegate {
  private static final Logger LOG = LoggerFactory.getLogger( TlNotifUserDenyType.class);

  @Autowired
  private RasaService rasaService;

  @Autowired
  private Environment env;

  @Autowired
  private JavaMailSender javaMailSender;

  @Value("${rasa.notify.user.deny.type}")
  private String rasaTypeDeny;

  @Override
  public void execute(DelegateExecution ctx) throws Exception {

    // On récupère à partir du context Cam -> l'id du tracker, la distance, le type de voyage, TypeApproved
    String TrackerId = (String) ctx.getVariable(ProcessConstantsRh.TRACKER_ID_TL);
    String email = TrackerId.split(":")[0];
    String travelType = (String) ctx.getVariable(ProcessConstantsRh.TRAVEL_TYPE);
    int travelDistance = (int) ctx.getVariable(ProcessConstantsRh.TRAVEL_DISTANCE);
    boolean  typeApproved = (boolean) ctx.getVariable(ProcessConstantsRh.TYPE_APPROVED);
    LOG.info( "LE TYPE APPROVED aprés le passage par le dmn :  " +typeApproved  ); 

    // On va demandé à Cam de demandé à Rasa d'executer le utter lié au "rasaTypeDeny" qu'on a écrit plus haut dans le @Value
    try {
      rasaService.utter(rasaTypeDeny, TrackerId);
    } catch (Exception e) {
        LOG.error("********** Error when notify user : " + TrackerId, e);
    }

    // Le Try suivant va simplement s'occuper d'envoyer un mail de notification de refus avec les motifs Distance/Type à l'email que nous avons récupéré plus haut   
    try {
      SimpleMailMessage mail = new SimpleMailMessage();
      LOG.info("********** Email : " + email);
      mail.setTo(email);
      mail.setFrom(env.getProperty("spring.mail.username"));
      mail.setSubject("Réservation de voyage refusée.");
      mail.setText("Demande de voyage refusée pour une distance de  : " + travelDistance +" avec le moyen de transport : " + travelType);
      // javaMailSender.send(mail);
    } catch (Exception e) {
      LOG.warn("Could not send email to user", e);
    }
  }
}
