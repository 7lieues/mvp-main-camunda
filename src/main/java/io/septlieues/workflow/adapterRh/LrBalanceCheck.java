package io.septlieues.workflow.adapterRh;

import io.septlieues.workflow.doa.RepoDaysLeaveStocks;
import io.septlieues.workflow.doa.RepoDaysLeaveTypes;
import io.septlieues.workflow.doa.RepoUsers;
import io.septlieues.workflow.model.DayLeaveStock;
import io.septlieues.workflow.model.DayLeaveType;
import io.septlieues.workflow.model.User;
import io.septlieues.workflow.services.BddService;

import io.septlieues.workflow.utils.ProcessConstantsRh;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class LrBalanceCheck implements JavaDelegate {
  private static final Logger LOG = LoggerFactory.getLogger(LrBalanceCheck.class);

  @Autowired
  private BddService bddService;

  @Autowired
  private RepoUsers repoUsers;

  @Autowired
  private RepoDaysLeaveTypes repoDaysLeaveTypes;

  @Autowired
  RepoDaysLeaveStocks repoDaysLeaveStocks;

  @Override
  public void execute(DelegateExecution ctx) throws Exception {
    //récupération de trackerid, leavetype et leave numberdays depuis le contexte camunda et création mail user à partir du tracker id
    String trackerId = (String) ctx.getVariable(ProcessConstantsRh.TRACKER_ID_LR);
    String addressMail = trackerId.split("::")[0];
    String leaveType = (String) ctx.getVariable(ProcessConstantsRh.LEAVE_TYPE);
    String leaveNumberDays = (String) ctx.getVariable(ProcessConstantsRh.LEAVE_NUMBER_DAYS  );
  
    //convertion de format et initialisation des variables tampon
    int leaveValueDays = Integer.parseInt(leaveNumberDays);
    int user_id = 0;
    int balance_bd = 0;
    int final_balance = 0;

    try {
      //récupération user id 
      //user_id = Integer.parseInt(bddService.sqlRequestOneCondition(ProcessConstantsRh.SQL_PRIMARY_KEY_USER_ID, ProcessConstantsRh.SQL_USER, ProcessConstantsRh.SQL_EMAIL, addressMail));
      //LOG.info("********** getting user_id from data base via son email " + user_id);
      User user = repoUsers.retrieveUserByEmail(addressMail);
      LOG.info("********** getting user_id from data base via son email " + user.getId());
      //on stock ce user_id dans le contexte de camunda
      String userId=String.valueOf(user.getId());
      ctx.setVariable(ProcessConstantsRh.USER_ID, userId);

      //on récupère l'id du leave type
      DayLeaveType dayLeaveType =  repoDaysLeaveTypes.retrieveDayLeaveTypeByTypeConge(leaveType);
      //int leave_type_id = Integer.parseInt(bddService.sqlRequestOneCondition(ProcessConstantsRh.SQL_LR_TYPE_ID, ProcessConstantsRh.SQL_TABLE_LEAVE_TYPE, ProcessConstantsRh.SQL_DESCRIPTION, leaveType));
      ctx.setVariable(ProcessConstantsRh.LEAVE_TYPE_ID, dayLeaveType.getId());

      //on récupère ensuite le balance avec l'id du leave type que l'on vient de récupérer
      List<DayLeaveStock> listdayLeaveStock =repoDaysLeaveStocks.findAll();
      DayLeaveStock dayLeaveStock  = listdayLeaveStock.stream().filter(e -> e.getDaysLeaveTypeId() == dayLeaveType.getId() && e.getUserId() == user.getId()).findFirst().get();
      //balance_bd = Integer.parseInt(bddService.sqlRequestTwoCondition(ProcessConstantsRh.SQL_SOLDE, ProcessConstantsRh.SQL_TABLE_LEAVE_STOCK, ProcessConstantsRh.SQL_TABLE_LEAVE_TYPE_ID, String.valueOf(dayLeaveType.getId()), ProcessConstantsRh.SQL_USER_ID, String.valueOf(user_id)));
      String balance =String.valueOf(dayLeaveStock.getBalance());
      //on l'écrit dans le contexte de camunda
      ctx.setVariable(ProcessConstantsRh.BALANCE, balance);
      LOG.info("********** User Balance :" + balance);

      //check si le solde est suffisant par rapport au nombre de jours demandés
      if (balance_bd >= leaveValueDays) {
        final_balance = balance_bd - leaveValueDays;
        LOG.info("********** final_balance : " + final_balance);
        String finalBalance=String.valueOf(final_balance);
        ctx.setVariable(ProcessConstantsRh.FINAL_BALANCE, finalBalance);

        boolean sufficientBalance = true;
        LOG.info("********** SUFFICIENT BALANCE ********* ");
        ctx.setVariable(ProcessConstantsRh.SUFFICIENT_BALANCE, sufficientBalance);
      } else {
        boolean sufficientBalance = false;
        ctx.setVariable(ProcessConstantsRh.SUFFICIENT_BALANCE, sufficientBalance);
        LOG.info("********** INSUFFICIENT BALANCE ********* ");
      }
    } catch (Exception e) {
      LOG.error(" Erreur with balance value block ", e);
    }
  }
}
