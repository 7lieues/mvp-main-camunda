package io.septlieues.workflow.adapterRh;


import io.septlieues.workflow.services.RasaService;
import io.septlieues.workflow.utils.ProcessConstantsRh;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class TlNotifMngValidation implements JavaDelegate {
    private static final Logger LOG = LoggerFactory.getLogger(TlNotifMngValidation.class);

    @Autowired
    private RasaService rasaService;
    @Value("${rasa.notify_request_travel.utter.name}")
    private String rasaNotifManager;

    @Value("${rasa.notify_need_validation}")
    private String rasaNotifUser;


    @Override
    public void execute(DelegateExecution ctx) throws Exception {

        // Cette petite classe va simplement demander à rasa d'executer le utter de notification au manager lié plus haut dans le @Value
        String managerTrackerId = (String) ctx.getVariable(ProcessConstantsRh.MANAGER_TRACKER_ID);

        //on récupère le tracker id du user
        String trackerId = (String) ctx.getVariable(ProcessConstantsRh.TRACKER_ID_TL);
        ctx.setVariable(ProcessConstantsRh.MESSAGE_LOOP, rasaNotifManager);
        
        LOG.info( "********** Cette classe notif le manager d'une demande de reservation  "  );
        try {
            rasaService.utter(rasaNotifManager, managerTrackerId);

            //on demande à rasa de lancer l'action "utter_tl_validated_resp" sur le tracker id du user
            rasaService.utter(rasaNotifUser, trackerId);
        } catch (Exception e) {
            LOG.error("error when notify manager : " + managerTrackerId, e);
        }

    }
}
