package io.septlieues.workflow.adapterRh;

import com.google.gson.Gson;

import io.septlieues.workflow.model.RasaEvent;
import io.septlieues.workflow.services.RasaService;
import io.septlieues.workflow.utils.ProcessConstantsRh;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

@Component
public class LrSetSlotUserBalancePanache implements JavaDelegate {
  private static final Logger LOG = LoggerFactory.getLogger(LrSetSlotUserBalancePanache.class);

  @Autowired
  private RasaService rasaService;

  @Autowired
  private Gson gson;

  @Value("${rasa.leave.days.user}")
  private String rasatUserDays;

  @Value("${rasa.leave.days.balance.user}")
  private String rasaUserBalance;

  @Override
  public void execute(DelegateExecution ctx) throws Exception {

    //récupération du trackerid user, de son balance et du nombre de jours demandés depuis le context de camunda
    String trackerId = (String) ctx.getVariable(ProcessConstantsRh.TRACKER_ID_LR);
    String balance = (String) ctx.getVariable(ProcessConstantsRh.ADDITION_BALANCE);
    String leaveNumberDays = (String) ctx.getVariable(ProcessConstantsRh.LEAVE_NUMBER_DAYS);
    

    try {
      //création header
      HttpHeaders requestHeader = new HttpHeaders();
      requestHeader.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);

      //paramétrage requête à rasa de remplir le slot "ent_lr_user_days" avec leaveNumberDays
      RasaEvent rasaEvent1 = new RasaEvent( "slot",rasatUserDays,leaveNumberDays);
      HttpEntity<RasaEvent> requestEntitySetSlot1 = new HttpEntity<>(rasaEvent1,requestHeader );
      //déploiement de la requête 
      rasaService.setSlot(trackerId, requestEntitySetSlot1);

      //paramétrage requête à rasa de remplir le slot "ent_lr_user_balance" avec balance
      RasaEvent rasaEvent2 = new RasaEvent("slot", rasaUserBalance, balance);
      HttpEntity<RasaEvent> requestEntitySetSlot2 = new HttpEntity<>( rasaEvent2, requestHeader);
      //déploiement de la requête
      rasaService.setSlot(trackerId, requestEntitySetSlot2);

    } catch (Exception e) {
      LOG.error(
        "error when set slot rasa for user balance  : " , e );
    }
  }
}
