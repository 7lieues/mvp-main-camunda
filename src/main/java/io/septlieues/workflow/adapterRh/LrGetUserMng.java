package io.septlieues.workflow.adapterRh;


import io.septlieues.workflow.services.KeycloakUserService;
import io.septlieues.workflow.utils.ProcessConstantsRh;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.camunda.bpm.extension.keycloak.json.JSONArray;
import org.camunda.bpm.extension.keycloak.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class LrGetUserMng implements JavaDelegate {
    private static final Logger LOG = LoggerFactory.getLogger(LrGetUserMng.class);

    @Autowired
    KeycloakUserService keycloakUserService;

    @Override
    public void execute(DelegateExecution ctx) throws Exception {

        //récupération tracker id depuis le context de camunda et fabriquation mail etc ...
        String trackerId = (String) ctx.getVariable(ProcessConstantsRh.TRACKER_ID_LR);
        String addressMail = trackerId.split("::")[0];
        String trackerTemp = trackerId.split("::")[1];
        String requestID = trackerTemp.split("::")[0];

        LOG.info("!!!!!!!!!!!!!!!! checkpoint get user mng !!!!!!!!!!!!!!!!!!!!");
        
        //vérification valeur
        LOG.info("********** Tracker ID du demandeur : "+ trackerId);
        LOG.info("********** Mail du demandeur : "+ addressMail);
        LOG.info("********** Bout du tracker ID : "+ trackerTemp);
        LOG.info("********** Request ID : "+ requestID);

        try {
            
            //récupération mail manager depuis keycloak
            String manager = ((JSONArray) new JSONObject(keycloakUserService.getKeycloakUserAttributesByUserEmail(addressMail)).get("manager")).getString(0);
            String managerEmail= keycloakUserService.searchUserByCustomAttribute("LDAP_ENTRY_DN",manager);
            LOG.info("********** Mail du manager : " + managerEmail);
            //inscription du mail manager et du request id dans le context de camunda
            ctx.setVariable(ProcessConstantsRh.MANAGER_EMAIL, managerEmail);
            ctx.setVariable(ProcessConstantsRh.REQUEST_ID, requestID);


        } catch (Exception e) {
            LOG.error(" can not find user ", e);
        }


    }
}