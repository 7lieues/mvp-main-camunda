package io.septlieues.workflow.adapterRh;

import io.septlieues.workflow.services.BddService;
import io.septlieues.workflow.services.UmbrellaNotification;
import io.septlieues.workflow.utils.ProcessConstantsRh;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/*une classe qui retourne le nombre de jour de congé demandé par l'utilisateur 
aprés la filtration des jours de weekend et jour fériers de la période entre la date de début de congé et la date de fin . */


@Component
public class LrGetLeaveValue implements JavaDelegate {
  @Autowired
  private BddService bddService;

  @Autowired
  private UmbrellaNotification umbrellaNotification;

  private static final Logger LOG = LoggerFactory.getLogger(LrGetLeaveValue.class);
  
  @Override
  public void execute(DelegateExecution ctx) throws Exception {

    // récupération dans le contexte camunda de la date de début/fin puis vérification valeurs logs
    String LeaveStart = (String) ctx.getVariable(ProcessConstantsRh.LEAVE_START);
    String LeaveEnd = (String) ctx.getVariable(ProcessConstantsRh.LEAVE_END);
    String LeaveType = (String) ctx.getVariable(ProcessConstantsRh.LEAVE_TYPE);
    String TrackerID = (String) ctx.getVariable(ProcessConstantsRh.TRACKER_ID_LR);
    LOG.info("LeaveType : " + LeaveType);
    LOG.info("Start :" + LeaveStart);
    LOG.info("End : " + LeaveEnd);
    LOG.info("le tracker ID : " + TrackerID);

    // requête read pour récupérer le nombre de jours demandés et le solde du user sur le leave type choisi
    JSONObject ReadRequest = umbrellaNotification.readLeaveRequest(TrackerID, LeaveType, LeaveStart, LeaveEnd);
    LOG.info("test affichage du Json : " + ReadRequest);

    
    // //le formateur de date pour les local dates
    // DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
    // // OLIV : cas d'erreur à gérer :
    // // Date retour antérieure à date départ
    // // Format de date non conforme à dd/mm/yyyy

    // //convertion des dates début/fin au format aaaa-mm-jj
    // LocalDate leave_start = LocalDate.parse(LeaveStart, formatter);
    // LocalDate leave_end = LocalDate.parse(LeaveEnd, formatter);
    // LOG.info("********** date de début format aaaa-mm-jj : " + leave_start);
    // LOG.info("********** date de fin format aaaa-mm-jj : " + leave_end);

    // //création d'un compteur pour incrémenter la boucle ci-dessous
    // LocalDate compteur = leave_start;

    // //on construit la liste qui contient l'ensemble de la demande utilisateur + check jours de week end
    // try {
    //   List<LocalDate> totalDates = new ArrayList<>();
    //   while (!compteur.isAfter(leave_end)) {
    //     if (
    //       (compteur.getDayOfWeek() != DayOfWeek.SATURDAY) &&
    //       (compteur.getDayOfWeek() != DayOfWeek.SUNDAY)
    //     ) {
    //       totalDates.add(compteur);
    //     }
    //     compteur = compteur.plusDays(1);
    //   }

    //   int AllLeaveDays = totalDates.size();

    //   //on récupère les jours fériés de la base de données
    //   List<LocalDate> holidays = bddService.getHolidays("days_holidays", "holidays_date", leave_start, leave_end);
    //   int holidaySize = holidays.size();
    //   LOG.info("********** Le nombre de jour de féries entre  : " + leave_start + " et : " + leave_end + " est : " + holidaySize);

    //   // on élimines les jours fériés des jours de congé :
    //   totalDates.removeAll(holidays);

    //   int NumberOfLeaveDays = totalDates.size();
    //   LOG.info("********** nombre de jours ouvrés sans jours fériés : " + NumberOfLeaveDays);

    //   //on rentre dans le constext camunda la liste des jours demandés par le user et on les affichent pour vérifier leurs valeurs
    //   ctx.setVariable(ProcessConstants.LIST_DATE_LR, totalDates);
    //   List verif = (List) ctx.getVariable(ProcessConstants.LIST_DATE_LR);
    //   LOG.info("vérification date demandé:   " + verif);

    //   //on écrit ensuite la valeur dans le contexte de camunda sans oublier de vérifier celle-ci
    //   String numberDays = String.valueOf(NumberOfLeaveDays);


    //set dans le context de la valeur du nbr de jour demandé
    Object numberDays = ReadRequest.get(String.valueOf("nbr_leave_days"));
    LOG.info("********** Nombre de jours demandés : " + numberDays);
    ctx.setVariable(ProcessConstantsRh.LEAVE_NUMBER_DAYS, String.valueOf(numberDays));


    //set dans le context de la valeur du balance user sur le type de congés choisi
    Object leave_balance = ReadRequest.get(String.valueOf("leave_balance"));
    LOG.info("********** Nombre de jours sur le solde du leave type : " + leave_balance);
    ctx.setVariable(ProcessConstantsRh.BALANCE, String.valueOf(leave_balance));


    //set dans le context de la liste de date dans l'intervalle de conges
    JSONArray list_date = ReadRequest.getJSONArray("available_days");
    LOG.info("liste de date : " + list_date);
    ctx.setVariable(ProcessConstantsRh.LIST_DATE_LR, String.valueOf(list_date));

    // String testO = (String) ctx.getVariable(ProcessConstants.LIST_DATE_LR);
    // LOG.info("&&&&&& " + testO);

    // JSONArray test0JA = new JSONArray(testO);

    // LOG.info("RECONVERSION : " + String.valueOf(test0JA));


    //check du solde avec le nombre de jours pour définir si la demande est possible ou non
    int leaveValueDays = (Integer) numberDays;
    int balance = (Integer) ReadRequest.get(String.valueOf("leave_balance"));
    if (balance >= leaveValueDays) {
      int final_balance = balance - leaveValueDays;
      LOG.info("********** final_balance : " + final_balance);
      String finalBalance=String.valueOf(final_balance);
      ctx.setVariable(ProcessConstantsRh.FINAL_BALANCE, finalBalance);

      boolean sufficientBalance = true;
      LOG.info("********** SUFFICIENT BALANCE ********* ");
      ctx.setVariable(ProcessConstantsRh.SUFFICIENT_BALANCE, sufficientBalance);
    } else {
      boolean sufficientBalance = false;
      ctx.setVariable(ProcessConstantsRh.SUFFICIENT_BALANCE, sufficientBalance);
      LOG.info("********** INSUFFICIENT BALANCE ********* ");
    }

    // } catch (Exception e) {
    //   e.printStackTrace();
    // }
  }
}
