package io.septlieues.workflow.adapterRh;

import com.google.gson.Gson;

import io.septlieues.workflow.model.RasaEvent;
import io.septlieues.workflow.services.RasaService;
import io.septlieues.workflow.utils.ProcessConstantsRh;
import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.camunda.bpm.engine.identity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Component
public class LrSetSlotsMngMultiType implements JavaDelegate {
    private static final Logger LOG = LoggerFactory.getLogger(LrSetSlotsMngMultiType.class);

    @Autowired
    private RasaService rasaService;

    @Autowired
    IdentityService identityService;

    @Autowired
    private Gson gson;

    @Value("${rasa.set_slot.action.name.multiple.type}")
    private String rasaActionName;

    @Value("${rasa.leave.balance.json}")
    private String rasaLeaveBalanceJson;


    // public List<String> remove_multiple_element(List<String> date_List, int start, int end) {
    
    //     for (int i = start; i <= end; i++) {
    //         date_List = date_List.remove(i);
    //     }
    
    //     return date_List;
    //   }

    @Override
    public void execute(DelegateExecution ctx) throws Exception {

        // On récup le trackerId du context cam, on fabrique le mail du user, on récup le mail du mng, le requestId
        String userTrackerID = (String) ctx.getVariable(ProcessConstantsRh.TRACKER_ID_LR);
        String userEmail = userTrackerID.split("::")[0];
        String managerEmail = (String) ctx.getVariable(ProcessConstantsRh.MANAGER_EMAIL);
        String requestID = (String) ctx.getVariable(ProcessConstantsRh.REQUEST_ID);
        String BU = (String) ProcessConstantsRh.BU;
        String managerTrackerId = managerEmail.concat("::").concat(requestID).concat("::").concat("1").concat("::").concat(BU);
        ctx.setVariable(ProcessConstantsRh.MANAGER_TRACKER_ID, managerTrackerId);
        
        LOG.info("********** Mail user : " + userEmail);
        LOG.info("********** Mail manager : " + managerEmail);
        LOG.info("********** Tracker ID manager : " + managerTrackerId);

        //récupération de la liste de congés valide pour la demande
        String conges = (String) ctx.getVariable(ProcessConstantsRh.CONGES);
        LOG.info("******** PASSAGE CHEZ CAMUNDA DU CONGES : " + conges);

        //récupération de la liste de solde valide pour la demande
        String solde = (String) ctx.getVariable(ProcessConstantsRh.SOLDE);
        LOG.info("******** PASSAGE CHEZ CAMUNDA DU SOLDE : " + solde);

        //remplir leave type avec les differents leave type et solde afin d'avoir une demande clair pour le mng
        String leaveType_temp = "";
        List<String> conges_list = Arrays.asList(conges.split(","));
        int conges_size = conges_list.size();
        List<String> solde_list = Arrays.asList(solde.split(","));

        for(int i = 0; i!= conges_size; i++) {
            leaveType_temp += solde_list.get(i) + " " + conges_list.get(i) + ", ";
        }
            
        int tarte = leaveType_temp.lastIndexOf(",");
        String leaveType = leaveType_temp.substring(0, tarte);
        LOG.info("**************** string composé : " + leaveType);        
        
        String processInstanceId = ctx.getProcessInstanceId();
        LOG.info("********** Process Instance ID : " + processInstanceId);

        // On récup du context Cam:
        String LeaveStart = (String) ctx.getVariable(ProcessConstantsRh.LEAVE_START);
        String LeaveEnd = (String) ctx.getVariable(ProcessConstantsRh.LEAVE_END);
        String LeaveNumberDays = (String) ctx.getVariable(ProcessConstantsRh.LEAVE_NUMBER_DAYS);

        User user = identityService.createUserQuery().userId(userEmail).singleResult();
        String userFullName=userEmail;
        if(user!=null) {
            userFullName = user.getFirstName() + " " + user.getLastName();
            LOG.info("********** Nom du demandeur : " + userFullName);
        }
        else {
            userFullName = userEmail;
        }

        try {

            // On imbrique les slots : ProcessInstanceId, LeaveType, LeaveStart, LeaveEnd, LeaveNumberDays, FinalBalance en format json qu'on stock dans jsonProduct
            HttpHeaders requestHeader = new HttpHeaders();
            requestHeader.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);

            Map<String, Object> inputMap = new HashMap<>();
            inputMap.put("processInstanceId", processInstanceId);
            inputMap.put("type", leaveType);
            inputMap.put("LeaveStart", LeaveStart);
            inputMap.put("LeaveEnd", LeaveEnd);
            inputMap.put("LeaveNumberDays",LeaveNumberDays);
            inputMap.put("applicant", userFullName);
            
            // convert map to JSON String
            String jsonProduct = gson.toJson(inputMap);

            LOG.info("********** JSON set slot côté manager : " + jsonProduct);

            // On set le slot de JsonProduct coté Rasa
            RasaEvent rasaEvent = new RasaEvent("slot", rasaLeaveBalanceJson, jsonProduct);
            HttpEntity<RasaEvent> requestEntitySetSlot = new HttpEntity<>(rasaEvent, requestHeader);

            //set les slots coté manager
            rasaService.setSlot(managerTrackerId, requestEntitySetSlot);
            rasaService.setSlot(managerTrackerId, requestEntitySetSlot);
            //set les slots coté user
            rasaService.setSlot(userTrackerID, requestEntitySetSlot);

        } catch (Exception e) {
            LOG.error("error when set slot rasa for leavetype  : " + leaveType + "and user "+userFullName , e);
        }

        try {

            //on demande à rasa de lancer l'action "action_parse_leave_balance_json" sur le managertrackerID
            rasaService.utter(rasaActionName, managerTrackerId);
            LOG.info("********* Manager tracker id :  =======  " + managerTrackerId);
            //on demande à rasa de lancer l'action "action_parse_leave_balance_json" sur le trackerID user
            rasaService.utter(rasaActionName, userTrackerID);
        } catch (Exception e) {
            LOG.error("error when set slot entities : " + managerTrackerId, e);
        }
    }
}
