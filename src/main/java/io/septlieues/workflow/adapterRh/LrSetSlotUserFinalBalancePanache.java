package io.septlieues.workflow.adapterRh;


import io.septlieues.workflow.model.RasaEvent;
import io.septlieues.workflow.services.BddService;
import io.septlieues.workflow.services.RasaService;
import io.septlieues.workflow.services.UmbrellaNotification;
import io.septlieues.workflow.utils.ProcessConstantsRh;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

/* une classe qui prépare le slot qui contient le solde final (aprés retrait ) et l'envoie à Rasa */

@Component
public class LrSetSlotUserFinalBalancePanache implements JavaDelegate {
  private static final Logger LOG = LoggerFactory.getLogger(LrSetSlotUserFinalBalancePanache.class);
  @Autowired
  private BddService bddService;
  @Autowired
  private RasaService rasaService;

  @Autowired
  private UmbrellaNotification umbrellaNotification;

  @Value("${rasa.user.final.balance}")
  private String rasaUserFinalBalance;

  @Override
  public void execute(DelegateExecution ctx) throws Exception {
    //récupération depuis le contexte de camunda
    String trackerId = (String) ctx.getVariable(ProcessConstantsRh.TRACKER_ID_LR);
    LOG.info("!!!!!!!!!!!!! trackerId = " + trackerId);
    String final_balance = (String) ctx.getVariable(ProcessConstantsRh.FINAL_BALANCE);
    LOG.info("!!!!!!!!!!!!! final balance = " + final_balance);
    String firstLeaveType=(String) ctx.getVariable(ProcessConstantsRh.FIRST_LEAVE_TYPE);
    LOG.info("!!!!!!!!!!!!! firstLeaveType = " + firstLeaveType);
    String secondLeaveType=(String) ctx.getVariable(ProcessConstantsRh.SECOND_LEAVE_TYPE);
    LOG.info("!!!!!!!!!!!!! secondLeaveType = " + secondLeaveType);

    String firstStock = (String) ctx.getVariable(ProcessConstantsRh.FIRST_BALANCE);
    int firstStockInt = Integer.parseInt(firstStock);
    String secondStock = (String) ctx.getVariable(ProcessConstantsRh.SECOND_BALANCE);
    int secondStockInt = Integer.parseInt(secondStock);
    String leaveDays = (String) ctx.getVariable(ProcessConstantsRh.LEAVE_NUMBER_DAYS);
    int leaveDaysInt = Integer.parseInt(leaveDays);

    String LeaveStart = (String) ctx.getVariable(ProcessConstantsRh.LEAVE_START);

    secondStockInt = secondStockInt - (leaveDaysInt - firstStockInt);

    int nbrDaysSecondCreate = leaveDaysInt - firstStockInt;
    LOG.info("second stock int : " + nbrDaysSecondCreate);

    LOG.info("!!!!!!!!!!!!! firstStock = " + firstStock);
    LOG.info("!!!!!!!!!!!!! secondStock = " + secondStock);
    LOG.info("!!!!!!!!!!!!! leaveDays = " + leaveDays);


    JSONObject CreateRequest = umbrellaNotification.createLeaveRequestNbrDays(trackerId, firstLeaveType, LeaveStart, firstStockInt, 1);
    LOG.info("Json create n°1 : " + CreateRequest);
    LeaveStart = (String) CreateRequest.get(String.valueOf("end_date_next_create"));
    LOG.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
    LOG.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
    
    JSONObject CreateRequest2 = umbrellaNotification.createLeaveRequestNbrDays(trackerId, secondLeaveType, LeaveStart, nbrDaysSecondCreate, 1);
    LOG.info("Json create n°2 : " + CreateRequest2);


  //   String user_id=(String) ctx.getVariable(ProcessConstants.USER_ID);
  //   int first_leave_type_id =(int) ctx.getVariable(ProcessConstants.FIRST_LEAVE_TYPE_ID);
  //   LOG.info("!!!!!!!!!!!!! first leave type id = " + String.valueOf(first_leave_type_id));
  //   int second_leave_type_id =(int) ctx.getVariable(ProcessConstants.SECOND_LEAVE_TYPE_ID);
  //   LOG.info("!!!!!!!!!!!!! second leave type id = " + String.valueOf(second_leave_type_id));
  //   LOG.info("!!!!!!!!!!!!! userId = " + user_id);

  // //  ajout de la valeur de solde final aprés la validation de demande de congé 
  //   bddService.UpdateRequestTwoCondition(ProcessConstants.SQL_TABLE_LEAVE_STOCK, ProcessConstants.SQL_SOLDE, "0", ProcessConstants.SQL_TABLE_LEAVE_TYPE_ID, String.valueOf(first_leave_type_id), ProcessConstants.SQL_USER_ID, user_id);
  //   bddService.UpdateRequestTwoCondition(ProcessConstants.SQL_TABLE_LEAVE_STOCK, ProcessConstants.SQL_SOLDE, String.valueOf(secondStockInt), ProcessConstants.SQL_TABLE_LEAVE_TYPE_ID, String.valueOf(second_leave_type_id), ProcessConstants.SQL_USER_ID, user_id);

    String second_balance = String.valueOf(secondStockInt);

    try {
      //création header
      HttpHeaders requestHeader = new HttpHeaders();
      requestHeader.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);

      //paramétrage demande rasa de remplir le slot "ent_lr_user_final_balance" avec le final balance
      RasaEvent rasaEvent = new RasaEvent("slot",rasaUserFinalBalance,second_balance);
      HttpEntity<RasaEvent> requestEntitySetSlot = new HttpEntity<>( rasaEvent,requestHeader);

      //déploiement de la requête
      rasaService.setSlot(trackerId, requestEntitySetSlot);

    } catch (Exception e) {
      LOG.error("error when set slot rasa for final balance  : " + second_balance,e );
    }
  }
}
