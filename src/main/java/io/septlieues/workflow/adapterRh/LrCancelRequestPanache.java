package io.septlieues.workflow.adapterRh;

import com.google.gson.Gson;

import io.septlieues.workflow.services.BddService;
import io.septlieues.workflow.services.RasaService;
import io.septlieues.workflow.services.UmbrellaNotification;
import io.septlieues.workflow.utils.ProcessConstantsRh;
import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public class LrCancelRequestPanache implements JavaDelegate {
    private static final Logger LOG = LoggerFactory.getLogger(LrCancelRequestPanache.class);

    @Autowired
    private RasaService rasaService;

    @Autowired
    IdentityService identityService;

    @Autowired
    private Environment env;

    @Autowired
    private Gson gson;

    @Autowired
    private BddService bddService;

    @Autowired
    private UmbrellaNotification umbrellaNotification;
    
    @Value("${rasa.deny.recap.solde}")
    private String rasaDenyRecapSoldes;


    @Override
    public void execute(DelegateExecution ctx) throws Exception {

        // On récup du context Cam le trackerId puis on fabrique le mail du user avec.
        String TrackerId = (String) ctx.getVariable(ProcessConstantsRh.TRACKER_ID_LR);
        String email = TrackerId.split(":")[0];
        String createRequestID = (String) ctx.getVariable(ProcessConstantsRh.LEAVE_REQUEST_ID);
        List<String> createRequestIdList = Arrays.asList(createRequestID.split(","));

        for (int i = 0; i < createRequestIdList.size(); i++) {
            JSONObject CancelRequest = umbrellaNotification.cancelLeaveRequest(Integer.parseInt(createRequestIdList.get(i)));
            LOG.info("********** affichage json cancel n°" + i + " : " + CancelRequest);
        }
        }

}