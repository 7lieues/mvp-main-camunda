package io.septlieues.workflow.adapterRh;

import com.google.gson.Gson;

import io.septlieues.workflow.model.RasaEvent;
import io.septlieues.workflow.services.RasaService;
import io.septlieues.workflow.utils.ProcessConstantsRh;
import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.camunda.bpm.engine.identity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

// une Classe qui prépare le slot à envoyer au Manager contenant les informations nécessaires
@Component
public class TlSetSlotMngValidation implements JavaDelegate {
  private static final Logger LOG = LoggerFactory.getLogger(TlSetSlotMngValidation.class);

  @Autowired
  private RasaService rasaService;
  @Autowired 
  IdentityService identityService;

  @Autowired
  private Gson gson;

  @Value("${rasa.travel.distance}")
  private String rasaTlDistance;
  @Value("${rasa.travel.type}")
  private String rasaTlType;
  @Value("${rasa.travel.class}")
  private String rasaTlClass;
  @Value("${rasa.travel.cost}")
  private String rasaTlCost;
  @Value("${rasa.travel.username}")
  private String rasaTlUserName;
  
  @Override
  public void execute(DelegateExecution ctx) throws Exception {

    // On recup les valeurs des variables suivantes -> TrackerId, TravelType, TravelDistance, TravelCost, TRavelClass 
    String managerTrackerID = (String) ctx.getVariable(ProcessConstantsRh.MANAGER_TRACKER_ID);
    String type = (String) ctx.getVariable(ProcessConstantsRh.TRAVEL_TYPE);
    String distance = String.valueOf((int) ctx.getVariable(ProcessConstantsRh.TRAVEL_DISTANCE));
    String cost = String.valueOf((int) ctx.getVariable(ProcessConstantsRh.TRAVEL_COST));
    String classTravel = (String) ctx.getVariable(ProcessConstantsRh.TRAVEL_CLASS);

    // On recup les valeurs bool des variables -> TYpeApproved, ClassApproved, NeedValidation
    boolean  typeApproved = (boolean) ctx.getVariable(ProcessConstantsRh.TYPE_APPROVED);
    boolean  classApproved = (boolean) ctx.getVariable(ProcessConstantsRh.CLASS_APPROVED);
    boolean  needValidation = (boolean) ctx.getVariable(ProcessConstantsRh.NEED_VALIDATION);
    LOG.info( "********** Cette classe set le slot de travel : type/distance/cost/class "  );
    LOG.info( "********** LE TYPE APPROVED aprés le passage par le dmn :  " +typeApproved  );
    LOG.info( "********** LE class Approved aprés le passage par le dmn :  " +classApproved  );
    LOG.info( "********** LE need Validation aprés le passage par le dmn :  " +needValidation  );
    
    // ON recup du context Cam le mail du user
    String userEmail = (String) ctx.getVariable(ProcessConstantsRh.USER_MAIL);
    User user = identityService.createUserQuery().userId(userEmail).singleResult();
    String applicant=userEmail;

    // On retrouve le nom complet du user à partir de son mail
    if(user!=null) { 
      applicant = user.getFirstName() + " " + user.getLastName();
      LOG.info("********** Nom du demandeur : " + applicant);
    }
    else {
      applicant = userEmail;
    }

    String a = ctx
    .getVariable(ProcessConstantsRh.TYPE_APPROVED)
    .getClass()
    .getName();
    LOG.info("********** Le type de type Approved est : " + a);
    Boolean b = (Boolean)ctx.getVariable(ProcessConstantsRh.TYPE_APPROVED);
    LOG.info("********** La valeur de type Approved est : " + b);
    
    
    // On va refaire les headers et les requettes pour pouvoir ensuite écrire le comm de refus dans le rasa user
    try {
      HttpHeaders requestHeader = new HttpHeaders();
      requestHeader.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);

      RasaEvent rasaEvent1 = new RasaEvent( "slot",rasaTlDistance,distance);
      HttpEntity<RasaEvent> requestEntitySetSlot1 = new HttpEntity<>(rasaEvent1,requestHeader );
      rasaService.setSlot(managerTrackerID, requestEntitySetSlot1);
      RasaEvent rasaEvent2 = new RasaEvent("slot", rasaTlType, type);
      HttpEntity<RasaEvent> requestEntitySetSlot2 = new HttpEntity<>( rasaEvent2, requestHeader);
      rasaService.setSlot(managerTrackerID, requestEntitySetSlot2);
      RasaEvent rasaEvent3 = new RasaEvent("slot", rasaTlCost, cost);
      HttpEntity<RasaEvent> requestEntitySetSlot3 = new HttpEntity<>( rasaEvent3, requestHeader);
      rasaService.setSlot(managerTrackerID, requestEntitySetSlot3);
      RasaEvent rasaEvent4 = new RasaEvent("slot", rasaTlClass, classTravel);
      HttpEntity<RasaEvent> requestEntitySetSlot4 = new HttpEntity<>( rasaEvent4, requestHeader);
      rasaService.setSlot(managerTrackerID, requestEntitySetSlot4);
      RasaEvent rasaEvent5 = new RasaEvent("slot", rasaTlUserName, applicant);
      HttpEntity<RasaEvent> requestEntitySetSlot5 = new HttpEntity<>( rasaEvent5, requestHeader);
      rasaService.setSlot(managerTrackerID, requestEntitySetSlot5);
    } catch (Exception e) {
      LOG.error("error when set slot rasa for user travel request : " , e );
    }
  }
}
