package io.septlieues.workflow.adapterRh;

import com.google.gson.Gson;

import io.septlieues.workflow.services.RasaService;
import io.septlieues.workflow.utils.ProcessConstantsRh;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

/*une class qui informe l'utilisateur du rejet de sa demande de congé suite à une insuffisance du solde de congé par mail */

@Component
public class lrNotifUserErrorLeaveType implements JavaDelegate {
  private static final Logger LOG = LoggerFactory.getLogger(lrNotifUserErrorLeaveType.class);

  @Autowired
  private RasaService rasaService;
  @Autowired
  private Environment env;

  @Autowired
  private Gson gson;

  @Value("${rasa.notify.user.error.leavetype}")
  private String rasaUtterName;

  @Override
  public void execute(DelegateExecution ctx) throws Exception {
    //récupération depuis le contexte camunda du tracker id et création du mail à partir de celui-ci
    String TrackerId = (String) ctx.getVariable(ProcessConstantsRh.TRACKER_ID_LR);
    String email = TrackerId.split(":")[0];

    try {
      //demande à rasa d'éxécuter l'action "utter_lr_user_unsufficient_balance" sur le tracker id du user
      rasaService.utter(rasaUtterName, TrackerId);
    } catch (Exception e) {
      LOG.error("error when notify user : " + TrackerId, e);
    }
  }
}
