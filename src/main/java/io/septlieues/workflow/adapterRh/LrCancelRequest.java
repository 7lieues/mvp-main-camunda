package io.septlieues.workflow.adapterRh;


import io.septlieues.workflow.services.BddService;
import io.septlieues.workflow.services.RasaService;
import io.septlieues.workflow.services.UmbrellaNotification;
import io.septlieues.workflow.utils.ProcessConstantsRh;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class LrCancelRequest implements JavaDelegate {
  private static final Logger LOG = LoggerFactory.getLogger(LrCancelRequest.class);
  @Autowired
  private BddService bddService;
  @Autowired
  private RasaService rasaService;
  
  @Autowired
  private UmbrellaNotification umbrellaNotification;


  @Override
  public void execute(DelegateExecution ctx) throws Exception {
    //récupération depuis le contexte de camunda
    String LeaveRequestID = (String) ctx.getVariable(ProcessConstantsRh.LEAVE_REQUEST_ID);
    
    //requête cancel pour delete les requetes pending
    JSONObject CancelRequest = umbrellaNotification.cancelLeaveRequest(Integer.parseInt(LeaveRequestID));
    LOG.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
    LOG.info("test affichage du Json du create: " + CancelRequest);
    LOG.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");

  }
}
