package io.septlieues.workflow.adapterRh;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;


@Component
public class SaveBooking implements JavaDelegate {
  private static final Logger LOG = LoggerFactory.getLogger( SaveBooking.class);
  @Override
  public void execute(DelegateExecution ctx) throws Exception {
    LOG.info("cette classe fait l'enregistrement dans ma base de données elle est pas encore implémentéé");
  }}