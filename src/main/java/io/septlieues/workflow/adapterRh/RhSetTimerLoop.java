package io.septlieues.workflow.adapterRh;

import io.septlieues.workflow.services.BddService;
import io.septlieues.workflow.services.UmbrellaNotification;
import io.septlieues.workflow.utils.ProcessConstantsRh;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class RhSetTimerLoop implements JavaDelegate {
  @Autowired
  private BddService bddService;

  @Autowired
  private UmbrellaNotification umbrellaNotification;

  private static final Logger LOG = LoggerFactory.getLogger(RhSetTimerLoop.class);

  @Value("${manager.timer.notification}")
  private String managerTimerNotification;
  
  @Override
  public void execute(DelegateExecution ctx) throws Exception {
    LOG.info("la durée pour que le manager repond est :"+managerTimerNotification );
    ctx.setVariable(ProcessConstantsRh.TIMER_LOOP_NOTIFICATION, managerTimerNotification);
  }
}
