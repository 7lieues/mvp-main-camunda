package io.septlieues.workflow.adapterRh;

import io.septlieues.workflow.services.RasaService;

import io.septlieues.workflow.utils.ProcessConstantsRh;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;


@Component
public class TlNotifUserSuccess implements JavaDelegate {
    private static final Logger LOG = LoggerFactory.getLogger(TlNotifUserSuccess.class);

    @Autowired
    private RasaService rasaService;
    @Autowired
    private Environment env;

    @Autowired
    private JavaMailSender javaMailSender;

    @Value("${rasa.notify.tl.user.success}")
    private String rasaTravelSucess;


    @Override
    public void execute(DelegateExecution ctx) throws Exception {

        // On recup le trackerId, puis on crée le mail à partir du trackerId
        String TrackerId = (String) ctx.getVariable(ProcessConstantsRh.TRACKER_ID_TL);
        String email = (String) ctx.getVariable(ProcessConstantsRh.USER_MAIL);

        // On demande à Cam de demandé à rasa d'exucuter le utter lié plus haut à rasaTravelSuccess
        try {
            rasaService.utter(rasaTravelSucess, TrackerId);
        } catch (Exception e) {
            LOG.error("error when notify manager : " + TrackerId, e);
        }

        // Cette partie envoie le mail de notif de validation 
        try {
            SimpleMailMessage mail = new SimpleMailMessage();
            LOG.info("********** Email user : "+ email);
            mail.setTo(email);
            mail.setFrom(env.getProperty("spring.mail.username"));
            mail.setSubject("Réservation de voyage validée.");
            mail.setText("Votre réservation est validée.");
            // javaMailSender.send(mail);
        } catch (Exception e) {
            LOG.warn("Could not send email to user", e);
        }

        }

}
