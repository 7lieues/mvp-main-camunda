package io.septlieues.workflow.adapterRh;


import io.septlieues.workflow.model.RasaEvent;
import io.septlieues.workflow.services.RasaService;
import io.septlieues.workflow.utils.ProcessConstantsRh;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;


@Component
public class TlSetSlotNotifRefus implements JavaDelegate {
    private static final Logger LOG = LoggerFactory.getLogger(TlSetSlotNotifRefus.class);

    @Autowired
    private RasaService rasaService;

    
    @Value("${rasa.deny.comment.tl.user}")
    private String rasaDenyCommentUser;

    @Override
    public void execute(DelegateExecution ctx) throws Exception {
        
        // On va chercher à partir du context Cam le comment de refus puis le tracker id
        String deny_comment = (String) ctx.getVariable(ProcessConstantsRh.COMMENT);
        String trackerId = (String) ctx.getVariable(ProcessConstantsRh.TRACKER_ID_TL);

        try {
            // On va refaire les headers et les requettes pour pouvoir ensuite écrire le comm de refus dans le rasa user
            HttpHeaders requestHeader = new HttpHeaders();
            requestHeader.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
            LOG.info("********** Commentaire de refus : " + deny_comment);
            RasaEvent rasaEvent = new RasaEvent("slot", rasaDenyCommentUser, deny_comment);
            HttpEntity<RasaEvent> requestEntitySetSlot = new HttpEntity<>(rasaEvent, requestHeader);
            rasaService.setSlot(trackerId, requestEntitySetSlot);

        } catch (Exception e) {
            LOG.error("error when set slot rasa for deny comment  : " + deny_comment, e);

        }

    }
}
