package io.septlieues.workflow.adapterRh;

import io.septlieues.workflow.services.BddService;
import io.septlieues.workflow.services.RasaService;
import io.septlieues.workflow.services.UmbrellaNotification;
import io.septlieues.workflow.utils.ProcessConstantsRh;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

/* une classe qui prépare le slot qui contient le solde final (aprés retrait ) et l'envoie à Rasa */

@Component
public class LrCreateRequestPendingPanache implements JavaDelegate {
  private static final Logger LOG = LoggerFactory.getLogger(LrCreateRequestPendingPanache.class);
  @Autowired
  private BddService bddService;
  @Autowired
  private RasaService rasaService;
  
  @Autowired
  private UmbrellaNotification umbrellaNotification;


  @Override
  public void execute(DelegateExecution ctx) throws Exception {
    //récupération depuis le contexte de camunda
    String trackerId = (String) ctx.getVariable(ProcessConstantsRh.TRACKER_ID_LR);
    String LeaveStart = (String) ctx.getVariable(ProcessConstantsRh.LEAVE_START);

    //récupération de la liste de congés valide pour la demande
    String conges = (String) ctx.getVariable(ProcessConstantsRh.CONGES);

    //récupération de la liste de solde valide pour la demande
    String solde = (String) ctx.getVariable(ProcessConstantsRh.SOLDE);

    //remplir leave type avec les differents leave type et solde afin d'avoir une demande clair pour le mng
    String leaveType_temp = "";
    List<String> conges_list = Arrays.asList(conges.split(","));
    LOG.info("******** liste de conges : " + conges_list);
    int conges_size = conges_list.size();
    List<String> solde_list = Arrays.asList(solde.split(","));
    LOG.info("******** liste de solde : " + solde_list);

    String createLeaveRequestID = "";

    for(int i = 0; i!= conges_size; i++) {
        JSONObject CreateRequest = umbrellaNotification.createLeaveRequestNbrDays(trackerId, conges_list.get(i), LeaveStart, Integer.parseInt(solde_list.get(i)), 0);
        LOG.info("Json create : " + CreateRequest);
        LOG.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        LOG.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        createLeaveRequestID += CreateRequest.get(String.valueOf("leave_request_id")) + ",";
        LeaveStart = (String) CreateRequest.get(String.valueOf("end_date_next_create"));
        LOG.info("Leave start : " + LeaveStart);
    }
    int tarte = createLeaveRequestID.lastIndexOf(",");
    createLeaveRequestID = createLeaveRequestID.substring(0, tarte);
    LOG.info("liste de request id : " + createLeaveRequestID);

    ctx.setVariable(ProcessConstantsRh.LEAVE_REQUEST_ID, createLeaveRequestID);

  }
}