package io.septlieues.workflow.adapterRh;

import com.google.gson.Gson;

import io.septlieues.workflow.model.RasaEvent;
import io.septlieues.workflow.services.RasaService;
import io.septlieues.workflow.utils.ProcessConstantsRh;
import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.camunda.bpm.engine.identity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;


@Component
public class LrSetSlotsMngPanache implements JavaDelegate {
    private static final Logger LOG = LoggerFactory.getLogger(LrSetSlotsMngPanache.class);

    @Autowired
    private RasaService rasaService;

    @Autowired
    IdentityService identityService;

    @Autowired
    private Gson gson;

    @Value("${rasa.set_slot.action.name.panache}")
    private String rasaActionName;

    @Value("${rasa.leave.balance.json.panache}")
    private String rasaLeaveBalanceJson;


    @Override
    public void execute(DelegateExecution ctx) throws Exception {

        // On récup le trackerId du context cam, on fabrique le mail du user, on récup le mail du mng, le requestId
        String userTrackerID = (String) ctx.getVariable(ProcessConstantsRh.TRACKER_ID_LR);
        String userEmail = userTrackerID.split("::")[0];
        String managerEmail = (String) ctx.getVariable(ProcessConstantsRh.MANAGER_EMAIL);
        String requestID = (String) ctx.getVariable(ProcessConstantsRh.REQUEST_ID);
        String BU = (String) ProcessConstantsRh.BU;
        String managerTrackerId = managerEmail.concat("::").concat(requestID).concat("::").concat("1").concat("::").concat(BU);
        ctx.setVariable(ProcessConstantsRh.MANAGER_TRACKER_ID, managerTrackerId);
        
        LOG.info("********** Mail user : " + userEmail);
        LOG.info("********** Mail manager : " + managerEmail);
        LOG.info("********** Tracker ID manager : " + managerTrackerId);
        
        
        String processInstanceId = ctx.getProcessInstanceId();
        LOG.info("********** Process Instance ID : " + processInstanceId);

        // On récup du context Cam: LeaveType, LeaveStart, LeaveEnd, Balance, FinalBalance, LeaveNumberDays
        String firstLeaveType = (String) ctx.getVariable(ProcessConstantsRh.FIRST_LEAVE_TYPE);
        String secondLeaveType = (String) ctx.getVariable(ProcessConstantsRh.SECOND_LEAVE_TYPE);
        String LeaveStart = (String) ctx.getVariable(ProcessConstantsRh.LEAVE_START);
        String LeaveEnd = (String) ctx.getVariable(ProcessConstantsRh.LEAVE_END);
        String additionBalance = (String) ctx.getVariable(ProcessConstantsRh.ADDITION_BALANCE);
        String finalBalance = (String) ctx.getVariable(ProcessConstantsRh.FINAL_BALANCE);
        String LeaveNumberDays = (String) ctx.getVariable(ProcessConstantsRh.LEAVE_NUMBER_DAYS);

        User user = identityService.createUserQuery().userId(userEmail).singleResult();
        String userFullName=userEmail;
        if(user!=null) {
            userFullName = user.getFirstName() + " " + user.getLastName();
            LOG.info("********** Nom du demandeur : " + userFullName);
        }
        else {
            userFullName = userEmail;
        }

        try {

            // On imbrique les slots : ProcessInstanceId, LeaveType, LeaveStart, LeaveEnd, LeaveNumberDays, FinalBalance en format json qu'on stock dans jsonProduct
            HttpHeaders requestHeader = new HttpHeaders();
            requestHeader.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);

            Map<String, Object> inputMap = new HashMap<>();
            inputMap.put("processInstanceId", processInstanceId);
            inputMap.put("type1", firstLeaveType);
            inputMap.put("type2", secondLeaveType);
            inputMap.put("LeaveStart", LeaveStart);
            inputMap.put("LeaveEnd", LeaveEnd);
            inputMap.put("LeaveNumberDays",LeaveNumberDays);
            inputMap.put("additionBalance", additionBalance);
            inputMap.put("final_balance", finalBalance);
            inputMap.put("applicant", userFullName);
            
            // convert map to JSON String
            String jsonProduct = gson.toJson(inputMap);

            LOG.info("********** JSON set slot côté manager : " + jsonProduct);
            LOG.info("!!!!!!!!!! checkpoint 1 !!!!!!!!!");

            // On set le slot de JsonProduct coté Rasa
            RasaEvent rasaEvent = new RasaEvent("slot", rasaLeaveBalanceJson, jsonProduct);
            HttpEntity<RasaEvent> requestEntitySetSlot = new HttpEntity<>(rasaEvent, requestHeader);
            LOG.info("!!!!!!!!!! checkpoint 2 !!!!!!!!!");

            //set les slots coté manager
            rasaService.setSlot(managerTrackerId, requestEntitySetSlot);
            rasaService.setSlot(managerTrackerId, requestEntitySetSlot);
            LOG.info("!!!!!!!!!! checkpoint 3 !!!!!!!!!");
            //set les slots coté user
            rasaService.setSlot(userTrackerID, requestEntitySetSlot);
            LOG.info("!!!!!!!!!! checkpoint 4 !!!!!!!!!");

        } catch (Exception e) {
            LOG.error("error when set slot rasa for leavetype  : " + "and user "+userFullName , e);
        }

        try {

            //on demande à rasa de lancer l'action "action_parse_leave_balance_json" sur le managertrackerID
            rasaService.utter(rasaActionName, managerTrackerId);
            LOG.info("!!!!!!!!!! checkpoint 5 !!!!!!!!!");
            //on demande à rasa de lancer l'action "action_parse_leave_balance_json" sur le trackerID user
            rasaService.utter(rasaActionName, userTrackerID);
            LOG.info("!!!!!!!!!! checkpoint 6 !!!!!!!!!");
        } catch (Exception e) {
            LOG.error("error when set slot entities : " + managerTrackerId, e);
        }
    }
}
