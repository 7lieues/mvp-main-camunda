package io.septlieues.workflow.adapterRh;

import com.google.gson.Gson;

import io.septlieues.workflow.doa.RepoDaysLeaveTypes;
import io.septlieues.workflow.doa.RepoUsers;
import io.septlieues.workflow.model.DayLeaveType;
import io.septlieues.workflow.utils.ProcessConstantsRh;
import io.septlieues.workflow.services.BddService;
import io.septlieues.workflow.services.RasaService;
import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Component
public class LrAddAnnualBalanceCp implements JavaDelegate {
    private static final Logger LOG = LoggerFactory.getLogger(LrAddAnnualBalanceCp.class);

    @Autowired
    private RasaService rasaService;

    @Autowired
    IdentityService identityService;

    @Autowired
    private Environment env;

    @Autowired
    private Gson gson;

    @Autowired
    private BddService bddService;

    @Autowired
    private RepoUsers repoUsers;

    @Autowired
    private RepoDaysLeaveTypes repoDay;

    @Override
    public void execute(DelegateExecution ctx) throws Exception {

        List<String> id_user = new ArrayList<>();
        String url = env.getProperty("spring.datasource-business.url");
        String username = env.getProperty("spring.datasource-business.username");
        String password = env.getProperty("spring.datasource-business.password");

        Connection conn = DriverManager.getConnection(url, username, password);
        Statement stmt = conn.createStatement();
        /*
        String sql = ("Select " + ProcessConstantsRh.SQL_PRIMARY_KEY_USER_ID + " from " + ProcessConstantsRh.SQL_USER); ///users

        
        ResultSet rs = stmt.executeQuery(sql);
        ResultSetMetaData resultMeta = rs.getMetaData();


        while (rs.next()) {
          String finalList = new String(rs.getString(ProcessConstantsRh.SQL_PRIMARY_KEY_USER_ID));
          id_user.add(finalList);
        }
        LOG.info("liste des user id : " + id_user);
        int id_user_size = id_user.size();
        String user_id_string = "";
        LOG.info("size = " + id_user_size);
        */
        List<String> id_users = repoUsers.retrieveAllUserId();

        //remplissage de ma string avec tous les éléments de la liste pour effectuer le update
        String delim = ",";

        String user_id_string = String.join(delim, id_users);

        LOG.info("string des users id : " + user_id_string);

        // //select de chaque solde puis soustraction avec les soldes à update pour écrire les nouvelles valeurs de solde
        String lr_type = ProcessConstantsRh.SQL_LR_TYPE_CP;
        // String leave_type_id = "";
        DayLeaveType dayLeaveType =repoDay.retrieveDayLeaveTypeBydescrip(lr_type);
        //leave_type_id = bddService.sqlRequestOneCondition(ProcessConstantsRh.SQL_LR_TYPE_ID, ProcessConstantsRh.SQL_TABLE_LEAVE_TYPE, ProcessConstantsRh.SQL_DESCRIPTION, lr_type);
        //LOG.info("******************** lr type id  : " + leave_type_id);

        //int annual_balance_string = Integer.parseInt(bddService.sqlRequestOneCondition(ProcessConstantsRh.SQL_ANNUAL_BALANCE, ProcessConstantsRh.SQL_TABLE_LEAVE_TYPE, ProcessConstantsRh.SQL_DESCRIPTION, lr_type));
        //LOG.info("******************** annual balance : " + annual_balance_string);

        String sql2 = ("Update " + ProcessConstantsRh.SQL_TABLE_LEAVE_STOCK + " set " + ProcessConstantsRh.SQL_SOLDE + " = " + ProcessConstantsRh.SQL_SOLDE + " + " + dayLeaveType.getAnnualBalance() + " where " + ProcessConstantsRh.SQL_TABLE_LEAVE_TYPE_ID + " = '" + dayLeaveType.getId() + "'" + " and " + ProcessConstantsRh.SQL_USER_ID + " IN " + "(" + user_id_string + ")");
        LOG.info("la querry : " + sql2);
        
        int update = stmt.executeUpdate(sql2);

        conn.close();

        }

}