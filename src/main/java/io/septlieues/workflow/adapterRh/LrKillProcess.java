package io.septlieues.workflow.adapterRh;

import com.google.gson.Gson;

import io.septlieues.workflow.services.BddService;
import io.septlieues.workflow.services.RasaService;
import io.septlieues.workflow.services.UmbrellaNotification;
import io.septlieues.workflow.utils.ProcessConstantsRh;
import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Iterator;

@Component
public class LrKillProcess implements JavaDelegate {
    private static final Logger LOG = LoggerFactory.getLogger(LrKillProcess.class);

    @Autowired
    private RasaService rasaService;

    @Autowired
    IdentityService identityService;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private UmbrellaNotification umbrellaNotification;

    @Autowired
    private Environment env;

    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    private BddService bddService;

    @Autowired
    private Gson gson;


    @Override
    public void execute(DelegateExecution ctx) throws Exception {

        String TrackerID = (String) ctx.getVariable(ProcessConstantsRh.TRACKER_ID_LR);

        LOG.info("on est dans le kill process début de la java là !");
        JSONObject test_readLeaveREquest = umbrellaNotification.stockLeaveRequest(TrackerID);
        LOG.info("retour dans le java de la sortie de la requete read : " + test_readLeaveREquest);
        for (Iterator iterator = test_readLeaveREquest.keys(); iterator.hasNext(); ) {
            Object cle = iterator.next();
            Object val = test_readLeaveREquest.get(String.valueOf(cle));
            LOG.info("cle=" + cle + ", valeur=" + val);
          }
        
    }
}