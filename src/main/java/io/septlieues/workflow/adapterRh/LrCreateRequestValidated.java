package io.septlieues.workflow.adapterRh;

import io.septlieues.workflow.services.BddService;
import io.septlieues.workflow.services.RasaService;
import io.septlieues.workflow.services.UmbrellaNotification;

import io.septlieues.workflow.utils.ProcessConstantsRh;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/* une classe qui prépare le slot qui contient le solde final (aprés retrait ) et l'envoie à Rasa */

@Component
public class LrCreateRequestValidated implements JavaDelegate {
  private static final Logger LOG = LoggerFactory.getLogger(LrCreateRequestValidated.class);
  @Autowired
  private BddService bddService;
  @Autowired
  private RasaService rasaService;
  
  @Autowired
  private UmbrellaNotification umbrellaNotification;


  @Override
  public void execute(DelegateExecution ctx) throws Exception {
    //récupération depuis le contexte de camunda
    String trackerId = (String) ctx.getVariable(ProcessConstantsRh.TRACKER_ID_LR);
    String leaveType=(String) ctx.getVariable(ProcessConstantsRh.LEAVE_TYPE);
    String LeaveStart = (String) ctx.getVariable(ProcessConstantsRh.LEAVE_START);
    String LeaveEnd = (String) ctx.getVariable(ProcessConstantsRh.LEAVE_END);
    
    //requête create pour créer la demande par défaut déjà validé
    JSONObject CreateRequest = umbrellaNotification.createLeaveRequest(trackerId, leaveType, LeaveStart, LeaveEnd, 1);
    LOG.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
    LOG.info("test affichage du Json du create: " + CreateRequest);
    LOG.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");

    Object leaveRequestId = CreateRequest.get(String.valueOf("leave_request_id"));
    ctx.setVariable(ProcessConstantsRh.LEAVE_REQUEST_ID, String.valueOf(leaveRequestId));

  }
}
