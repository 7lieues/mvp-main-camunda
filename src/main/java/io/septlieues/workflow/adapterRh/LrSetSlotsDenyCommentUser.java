package io.septlieues.workflow.adapterRh;

import com.google.gson.Gson;
import io.septlieues.workflow.model.RasaEvent;
import io.septlieues.workflow.services.RasaService;
import io.septlieues.workflow.utils.ProcessConstantsRh;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

@Component
public class LrSetSlotsDenyCommentUser implements JavaDelegate {
    private static final Logger LOG = LoggerFactory.getLogger(LrSetSlotsDenyCommentUser.class);

    @Autowired
    private RasaService rasaService;

    @Autowired
    private Gson gson;

    @Value("${rasa.deny.comment.user}")
    private String rasaDenyCommentUser;

    @Override
    public void execute(DelegateExecution ctx) throws Exception {

        // Récup dans le contexte Cam du comm de refus et le trackerId
        String deny_comment = (String) ctx.getVariable(ProcessConstantsRh.COMMENT);
        String trackerId = (String) ctx.getVariable(ProcessConstantsRh.TRACKER_ID_LR);

        // On crée le header, event et url de la requete pour set le slot.
        try {
            HttpHeaders requestHeader = new HttpHeaders();
            requestHeader.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
            RasaEvent rasaEvent = new RasaEvent("slot", rasaDenyCommentUser, deny_comment);
            HttpEntity<RasaEvent> requestEntitySetSlot = new HttpEntity<>(rasaEvent, requestHeader);
            LOG.info("********** Commentaire de refus : " + deny_comment);

            rasaService.setSlot(trackerId, requestEntitySetSlot);

        } catch (Exception e) {
            LOG.error("error when set slot rasa for deny comment  : " + deny_comment, e);

        }

    }
}
