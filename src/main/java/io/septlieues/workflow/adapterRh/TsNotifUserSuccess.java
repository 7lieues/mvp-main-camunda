package io.septlieues.workflow.adapterRh;

import com.google.gson.Gson;

import io.septlieues.workflow.services.RasaService;
import io.septlieues.workflow.utils.ProcessConstantsRh;
import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

// une classe qui informe le user que sont timeSheet est validé
@Component
public class TsNotifUserSuccess implements JavaDelegate {
  private static final Logger LOG = LoggerFactory.getLogger(
    TsNotifUserSuccess.class
  );

  @Autowired
  private RasaService rasaService;

  @Autowired
  IdentityService identityService;

  @Autowired
  private Environment env;

  @Autowired
  private JavaMailSender javaMailSender;

  @Autowired
  private Gson gson;

  @Value("${rasa.notify.user.ts.success}")
    private String rasaUtterName;

  @Override
  public void execute(DelegateExecution ctx) throws Exception {

    //on récupère le tracker id user qu'on avait écrit dans le context (cf TsGetUserType)
    String TrackerId = (String) ctx.getVariable(ProcessConstantsRh.TRACKER_ID_TS);

    //on en extrait le mail
    String email = TrackerId.split(":")[0];
    LOG.info("********** Email user : " + email);

       try {
            //on demande ici à rasa d'éxécuter une action (cf application.yml) avec le tracker id du user
            rasaService.utter(rasaUtterName, TrackerId);

        } catch (Exception e) {
            LOG.error("error when notify manager : " + TrackerId, e);
        }
    
    try {

      //construction et envoie du mail par camunda, ici aucun besoin de rasa
      SimpleMailMessage mail = new SimpleMailMessage();
      mail.setTo(email);
      mail.setFrom(env.getProperty("spring.mail.username"));
      mail.setSubject("TimeSheet accepté.");
      mail.setText(
        "Votre timeSheet est valide et accepté, il est stocké en base de données ."
      );
      // javaMailSender.send(mail);

    } catch (Exception e) {
      LOG.warn("Could not send email to user", e);
    }
  }
}
