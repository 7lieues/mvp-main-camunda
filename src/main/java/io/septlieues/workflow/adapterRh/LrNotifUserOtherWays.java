package io.septlieues.workflow.adapterRh;

import com.google.gson.Gson;

import io.septlieues.workflow.model.RasaEvent;
import io.septlieues.workflow.services.BddService;
import io.septlieues.workflow.services.RasaService;
import io.septlieues.workflow.services.UmbrellaNotification;
import io.septlieues.workflow.utils.ProcessConstantsRh;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import java.util.*;

/*une class qui informe l'utilisateur du rejet de sa demande de congé suite à une insuffisance du solde de congé par mail */

@Component
public class LrNotifUserOtherWays implements JavaDelegate {
  private static final Logger LOG = LoggerFactory.getLogger(LrNotifUserOtherWays.class);

  @Autowired
  private RasaService rasaService;
  @Autowired
  private Environment env;

  @Autowired
  private BddService bddService;

  @Autowired
  private Gson gson;

  @Autowired
  private UmbrellaNotification umbrellaNotification;

  @Value("${rasa.recap.conges.proposition}")
  private String rasaRecapCongesProposition;

  @Value("${rasa.notify.user.proposition}")
  private String rasaUtterName;

  @Value("${rasa.user.utter.final.proposition}")
  private String rasaSdkUtterName;

  @Value("${rasa.user.final.proposition}")
  private String rasaUtterFinalProposition;

  @Override
  public void execute(DelegateExecution ctx) throws Exception {

    String TrackerId = (String) ctx.getVariable(ProcessConstantsRh.TRACKER_ID_LR);
    String email = TrackerId.split(":")[0];
    LOG.info("********* mail du user : " + email);

    boolean nopropostion = false;
    ctx.setVariable(ProcessConstantsRh.NOPROPOSITION, nopropostion);

    boolean propositiononetype = false;
    ctx.setVariable(ProcessConstantsRh.PROPOSITIONONETYPE, propositiononetype);

    JSONObject obj = umbrellaNotification.stockLeaveRequest(TrackerId);
    String leaveEntered = (String) ctx.getVariable(ProcessConstantsRh.LEAVE_TYPE);;
    String nbDayAsk_string = (String) ctx.getVariable(ProcessConstantsRh.LEAVE_NUMBER_DAYS);
    int nbDayAsk = Integer.parseInt(nbDayAsk_string);
    String exceptionnel = "Exceptionnel";
    Map<String, Integer> array = new HashMap<String, Integer>();
    String leaveNames = "";
    String leaveValues = "";
    // Type Exceptional finds
    if (exceptionnel.equals(leaveEntered)) {
        LOG.info("Exceptionnel");
        //return 0;
    }
    
    Iterator<String> keys = obj.keys();
    while(keys.hasNext()) {
        String key = keys.next();
        array.put(key, Integer.valueOf(obj.get(key).toString()));
        if (nbDayAsk <= Integer.parseInt(obj.get(key).toString()) && leaveNames.isEmpty() && leaveValues.isEmpty() ) {
            leaveValues += key + ",";
            leaveNames += Integer.valueOf(obj.get(key).toString()) + ",";
        }
    }
    array.remove(exceptionnel);
    /* Add up all the user's leaves of absence and check that they are not less than the number requested */
    if (array.values().stream().reduce(0, Integer::sum) < nbDayAsk) {
        nopropostion = true;
        ctx.setVariable(ProcessConstantsRh.NOPROPOSITION, nopropostion);
        LOG.info("Malheuresement, Aucun autre type de congés ne correspond à votre demande ");
        return;
    }
    /* Retrieve the greater value of the leave and deduct it in nb_jour_demandé if it is greater */
    if (!leaveNames.isEmpty()  && !leaveValues.isEmpty()) {
        String names = leaveNames.substring(0, leaveNames.lastIndexOf(","));
        String numbers = leaveValues.substring(0, leaveValues.lastIndexOf(","));
        ctx.setVariable(ProcessConstantsRh.CONGES, numbers);
        ctx.setVariable(ProcessConstantsRh.LEAVE_TYPE, numbers);
        ctx.setVariable(ProcessConstantsRh.SOLDE, names);
        propositiononetype = true;
        ctx.setVariable(ProcessConstantsRh.PROPOSITIONONETYPE, propositiononetype);
        LOG.info(String.format("Mais les types de congés suivants possèdent un solde suffisant à votre demande : {%s} : {%s}", names, numbers));
        return;
    }
    /* Adding vacations to find a value greater than equal to the amount requested.
    If the sum of vacations is less than the requested amount, an error message will be displayed */
    leaveNames += leaveEntered;
    leaveValues += array.get(leaveEntered).toString();
    Integer sum  = array.get(leaveEntered);
    array.remove(leaveEntered);
    for (Map.Entry<String, Integer> pair : array.entrySet()) {
        leaveNames += "," + pair.getKey();
        sum += pair.getValue();
        if (sum >= nbDayAsk) {
            int value = (sum - nbDayAsk == 0) ? pair.getValue() : pair.getValue() - (sum - nbDayAsk);
            sum = sum - (sum - nbDayAsk);
            leaveValues += "," + value;
            break;
        }
        leaveValues += "," + pair.getValue();
    }
    String resultat = "";
    List<String> conges_list = Arrays.asList(leaveNames.split(","));
    List<String> solde_list = Arrays.asList(leaveValues.split(","));
    for (int i = 0; i != conges_list.size(); i++) {
    //   resultat += conges_list.get(i) + " : " + solde_list.get(i) + ",";
      resultat += solde_list.get(i) + " " + conges_list.get(i) + ", et ";
    }
    resultat = resultat.substring(0, resultat.lastIndexOf(','));
    LOG.info("String de résultat de la proposition panache : " + resultat);

    LOG.info("string résultat de conges : " + leaveNames);
    LOG.info("string résultat de soldes : " + leaveValues);
    ctx.setVariable(ProcessConstantsRh.CONGES, leaveNames);
    ctx.setVariable(ProcessConstantsRh.SOLDE, leaveValues);

    HttpHeaders requestHeader = new HttpHeaders();
    requestHeader.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);


    RasaEvent rasaEvent = new RasaEvent("slot", rasaUtterFinalProposition, resultat);
    HttpEntity<RasaEvent> requestEntitySetSlot = new HttpEntity<>(rasaEvent, requestHeader);

    rasaService.setSlot(TrackerId, requestEntitySetSlot);

    rasaService.utter(rasaSdkUtterName, TrackerId);
  }
}
