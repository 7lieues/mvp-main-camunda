package io.septlieues.workflow.adapterRh;

import com.google.gson.Gson;

import io.septlieues.workflow.services.RasaService;
import io.septlieues.workflow.utils.ProcessConstantsRh;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

/*une class qui informe l'utlisateur que les presta ne sont pas gérés */


@Component
public class TsNotifNotEmployee implements JavaDelegate {
  private static final Logger LOG = LoggerFactory.getLogger( TsNotifNotEmployee.class );

  @Autowired
  private RasaService rasaService;
  @Autowired
  private Environment env;

  @Autowired
  private JavaMailSender javaMailSender;

  @Autowired
  private Gson gson;

  @Value("${rasa.notify.user.not.managed.employee}")
  private String rasaUtterName; 

  @Override
  public void execute(DelegateExecution ctx) throws Exception {
    //récupération tracker id depuis le context camunda et fabrication email
    String TrackerId = (String) ctx.getVariable(ProcessConstantsRh.TRACKER_ID_TS);
    String email = TrackerId.split(":")[0];

    try {
      //demande à rasa de lancer l'acrtion "utter_ts_user_contractor"
      rasaService.utter(rasaUtterName, TrackerId);
      
    } catch (Exception e) {
      LOG.error("error when notify user : " + TrackerId, e);
    }

    try {

      //paramétrage et envoie du mail par camunda
      SimpleMailMessage mail = new SimpleMailMessage();
      LOG.info("********** Email : " + email);
      mail.setTo(email);
      mail.setFrom(env.getProperty("spring.mail.username"));
      mail.setSubject("Type employee non géré .");
      mail.setText( "Votre TimeSheet n'est pas accepté car votre type d'employéé n'est pas géré ");
      // javaMailSender.send(mail);

    } catch (Exception e) {
      LOG.warn("Could not send email to user", e);
    }
  }
}
