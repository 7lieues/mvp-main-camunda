package io.septlieues.workflow.adapterRh;


import io.septlieues.workflow.services.RasaService;
import io.septlieues.workflow.utils.ProcessConstantsRh;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

//une classe qui notif le user de rejet de sa réservation de la part de manager avec un motif normalement envoyé par le manager
@Component
public class TlNotifUserMngDeny implements JavaDelegate {
    private static final Logger LOG = LoggerFactory.getLogger(TlNotifUserMngDeny.class);

    @Autowired
    private RasaService rasaService;

    @Autowired
    private Environment env;

    @Autowired
    private JavaMailSender javaMailSender;

    @Value("${rasa.notify.user.deny.comment.tl}")
    private String rasaUtterName;


    @Override
    public void execute(DelegateExecution ctx) throws Exception {

        // On recup le tracker id du user, on crée son mail à partir du tracker id, puis on récupère du context Cam le comm de refus
        String TrackerId = (String) ctx.getVariable(ProcessConstantsRh.TRACKER_ID_TL);
        String email = TrackerId.split(":")[0];
        String deny_comment =(String) ctx.getVariable(ProcessConstantsRh.COMMENT);

        // On demande à Cam de demandé à rasa d'exucuter le utter lié plus haut à rasaUtterName
        try {
            rasaService.utter(rasaUtterName, TrackerId);
        } catch (Exception e) {
            LOG.error("error when notify user : " + TrackerId, e);
        }

        // Cette partie envoie le mail de notif de refus avec le comm de refus du mng 
        try {
            SimpleMailMessage mail = new SimpleMailMessage();
            LOG.info("********** Email : "+ email);
            mail.setTo(email);
            mail.setFrom(env.getProperty("spring.mail.username"));
            mail.setSubject("Réservation de voyage refusée.");
            mail.setText("Demande de reservation de voyage réfusée avec le motif suivant : " + deny_comment);
            // javaMailSender.send(mail);
        } catch (Exception e) {
            LOG.warn("Could not send email to user", e);
        }

        }

}
