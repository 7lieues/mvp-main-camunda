package io.septlieues.workflow.adapterRh;

import com.google.gson.Gson;


import io.septlieues.workflow.services.RasaService;
import io.septlieues.workflow.utils.ProcessConstantsRh;
import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;


@Component
public class LrNotifUserDenyWithComment implements JavaDelegate {
    private static final Logger LOG = LoggerFactory.getLogger(LrNotifUserDenyWithComment.class);

    @Autowired
    private RasaService rasaService;

    @Autowired
    IdentityService identityService;

    @Autowired
    private Environment env;

    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    private Gson gson;

    @Value("${rasa.notify.user.deny.comment}")
    private String rasaUtterName;


    @Override
    public void execute(DelegateExecution ctx) throws Exception {

        // On récup du context Cam le trackerId et le comm de refus puis on fabrique le mail du user avec.
        String TrackerId = (String) ctx.getVariable(ProcessConstantsRh.TRACKER_ID_LR);
        String email = TrackerId.split(":")[0];
        String deny_comment = (String) ctx.getVariable(ProcessConstantsRh.COMMENT);

        // On execute le utter lié à rasaUtterName plus haut dans @Value
        try {
            rasaService.utter(rasaUtterName, TrackerId);
        } catch (Exception e) {
            LOG.error("error when notify user : " + TrackerId, e);
        }

        // On envoie le mail de refus au mail fabriqué plus haut avec le comm de refus
        try {
            SimpleMailMessage mail = new SimpleMailMessage();
            LOG.info("********** Email : "+ email);
            mail.setTo(email);
            mail.setFrom(env.getProperty("spring.mail.username"));
            mail.setSubject("Demande de congé refusée.");
            mail.setText("Demande de congé resuée avec le motif suivant : " + deny_comment);
            // javaMailSender.send(mail);
        } catch (Exception e) {
            LOG.warn("Could not send email to user", e);
        }

        }

}
