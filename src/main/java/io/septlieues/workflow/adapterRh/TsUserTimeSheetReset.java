package io.septlieues.workflow.adapterRh;


import io.septlieues.workflow.model.RasaEvent;
import io.septlieues.workflow.services.RasaService;
import io.septlieues.workflow.utils.ProcessConstantsRh;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

@Component
public class TsUserTimeSheetReset implements JavaDelegate {
  private static final Logger LOG = LoggerFactory.getLogger(TsUserTimeSheetReset.class);

  @Value("${rasa.timesheet}")
  private String rasaUserTimeSheet;
  
  @Autowired
    private RasaService rasaService;


  @Override
  public void execute(DelegateExecution ctx) throws Exception {
    try{

    //set time à null
    String timeSheet = null;

    //on l'écris dans le context camunda
    ctx.setVariable(ProcessConstantsRh.TIME_SHEET, timeSheet);
    LOG.info("********** TimeSheet avant de l'inscrire dans le context: " + timeSheet);

    //deuxième check pour vérifier sa valeur dans le context camunda
    String ts = (String) ctx.getVariable(ProcessConstantsRh.TIME_SHEET);
    LOG.info("********** valeur de Timesheet inscrite dans le context de camunda: " + ts);

    //récupération tracker id rasa
    String userTrackID = (String) ctx.getVariable(ProcessConstantsRh.TRACKER_ID_TS);

    //création header
    HttpHeaders requestHeader = new HttpHeaders();

    //demande de set sur le slot "ent_slot_ts_timesheet" coté rasa avec la nouvelle valeur == null
    RasaEvent rasaEvent1 = new RasaEvent("slot", rasaUserTimeSheet, timeSheet);
    HttpEntity<RasaEvent> requestEntitySetSlot1 = new HttpEntity<>(rasaEvent1, requestHeader);

    //déploiement requête
    rasaService.setSlot(userTrackID,requestEntitySetSlot1);}

    catch (Exception e) {
        LOG.error("error when reseting the timeSheet : " +  e);
    }
  }
}
