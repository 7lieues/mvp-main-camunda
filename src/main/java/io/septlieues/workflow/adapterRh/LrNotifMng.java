package io.septlieues.workflow.adapterRh;

import com.google.gson.Gson;

import io.septlieues.workflow.services.RasaService;
import io.septlieues.workflow.utils.ProcessConstantsRh;
import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


@Component
public class LrNotifMng implements JavaDelegate {
    private static final Logger LOG = LoggerFactory.getLogger(LrNotifMng.class);

    @Autowired
    private RasaService rasaService;

    @Autowired
    IdentityService identityService;

    @Autowired
    private Gson gson;

    /*the config of utter to send to notify Rasa parsed from yml file*/
    @Value("${rasa.notify_lr_request.utter.name}")
    private String rasaUtterName;

    @Value("${rasa.action.wait.utter.name.lr}")
    private String rasaActionWaitingLr;

    @Override
    public void execute(DelegateExecution ctx) throws Exception {

        //encadrement compteur pour le rappel mng
        ctx.setVariable(ProcessConstantsRh.COMPTEUR_PING_MNG, "1");
        ctx.setVariable(ProcessConstantsRh.MESSAGE_LOOP, rasaUtterName);
        //encadrement compteur pour le rappel mng

        //on récupère le trackerID du user
        String usertrackerID = (String) ctx.getVariable(ProcessConstantsRh.TRACKER_ID_LR);
        //récupération trackerid manager et mail manager depuis le context de camunda
        String managerEmail = (String) ctx.getVariable(ProcessConstantsRh.MANAGER_EMAIL);
        String managerTrackerId = (String) ctx.getVariable(ProcessConstantsRh.MANAGER_TRACKER_ID);

        try {
            //demande à rasa de lancer l'action "utter_lr_waiting_response" sur le tracker id user
            rasaService.utter(rasaActionWaitingLr, usertrackerID);
            
            //demande à rasa de lancer l'action "utter_lr_mng_notify_lr_request" sur le trackerid du manager
            rasaService.utter(rasaUtterName, managerTrackerId);
        } catch (Exception e) {
            LOG.error("error when notify manager : " + managerTrackerId, e);
        }

    }
}
