package io.septlieues.workflow.adapterRh;

import com.google.gson.Gson;

import io.septlieues.workflow.model.RasaEvent;
import io.septlieues.workflow.services.RasaService;
import io.septlieues.workflow.utils.ProcessConstantsRh;
import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.camunda.bpm.engine.identity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;


@Component
public class LrSetSlotRecapUserPanache implements JavaDelegate {
    private static final Logger LOG = LoggerFactory.getLogger(LrSetSlotRecapUserPanache.class);

    @Autowired
    private RasaService rasaService;

    @Autowired
    IdentityService identityService;

    @Autowired
    private Gson gson;

    @Value("${rasa.set_slot.action.name.panache}")
    private String rasaActionName;

    @Value("${rasa.leave.balance.json.panache}")
    private String rasaLeaveBalanceJson;

    @Override
    public void execute(DelegateExecution ctx) throws Exception {

        // On récup le trackerId du context cam, on fabrique le mail du user, on récup le mail du mng, le requestId
        String userTrackerID = (String) ctx.getVariable(ProcessConstantsRh.TRACKER_ID_LR);
        String userEmail = userTrackerID.split("::")[0];
        
        String processInstanceId = ctx.getProcessInstanceId();
        LOG.info("********** Process Instance ID : " + processInstanceId);

        // On récup du context Cam: LeaveType, LeaveStart, LeaveEnd, Balance, FinalBalance, LeaveNumberDays

        String firstLeaveType = (String) ctx.getVariable(ProcessConstantsRh.FIRST_LEAVE_TYPE);
        String secondLeaveType = (String) ctx.getVariable(ProcessConstantsRh.SECOND_LEAVE_TYPE);
        String additionBalance = (String) ctx.getVariable(ProcessConstantsRh.ADDITION_BALANCE);
        String LeaveStart = (String) ctx.getVariable(ProcessConstantsRh.LEAVE_START);
        String LeaveEnd = (String) ctx.getVariable(ProcessConstantsRh.LEAVE_END);
        String finalBalance = (String) ctx.getVariable(ProcessConstantsRh.FINAL_BALANCE);
        String LeaveNumberDays = (String) ctx.getVariable(ProcessConstantsRh.LEAVE_NUMBER_DAYS);
        
        LOG.info("********** leave types : " + firstLeaveType + " " + secondLeaveType);
        LOG.info("********** addition balance : " + additionBalance);
        LOG.info("********** Final_balance : " + finalBalance);

        User user = identityService.createUserQuery().userId(userEmail).singleResult();
        String userFullName=userEmail;
        if(user!=null) {
            userFullName = user.getFirstName() + " " + user.getLastName();
            LOG.info("********** Nom du demandeur : " + userFullName);
        }
        else {
            userFullName = userEmail;
        }

        try {

            // On imbrique les slots : ProcessInstanceId, LeaveType, LeaveStart, LeaveEnd, LeaveNumberDays, FinalBalance en format json qu'on stock dans jsonProduct
            HttpHeaders requestHeader = new HttpHeaders();
            requestHeader.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);

            Map<String, Object> inputMap = new HashMap<>();
            inputMap.put("processInstanceId", processInstanceId);
            inputMap.put("type1", firstLeaveType);
            inputMap.put("type2", secondLeaveType);
            inputMap.put("LeaveStart", LeaveStart);
            inputMap.put("LeaveEnd", LeaveEnd);
            inputMap.put("LeaveNumberDays",LeaveNumberDays);
            inputMap.put("additionBalance", additionBalance);
            inputMap.put("final_balance", finalBalance);
            inputMap.put("applicant", userFullName);
            
            // convert map to JSON String
            String jsonProduct = gson.toJson(inputMap);

            LOG.info("********** JSON set slot côté manager : " + jsonProduct);

            // On set le slot de JsonProduct coté Rasa
            RasaEvent rasaEvent = new RasaEvent("slot", rasaLeaveBalanceJson, jsonProduct);
            HttpEntity<RasaEvent> requestEntitySetSlot = new HttpEntity<>(rasaEvent, requestHeader);

            //set les slots coté user
            rasaService.setSlot(userTrackerID, requestEntitySetSlot);

        } catch (Exception e) {
            LOG.error("error when set slot rasa for leavetype  : " + "and user "+userFullName , e);
        }

        try {
            //on demande à rasa de lancer l'action "action_parse_leave_balance_json" sur le trackerID user
            rasaService.utter(rasaActionName, userTrackerID);

        } catch (Exception e) {
            LOG.error("error when set slot entities : " + userTrackerID, e);
        }
    }
}
