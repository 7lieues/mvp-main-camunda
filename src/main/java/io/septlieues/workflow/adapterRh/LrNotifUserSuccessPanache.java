package io.septlieues.workflow.adapterRh;

import com.google.gson.Gson;

import io.septlieues.workflow.services.RasaService;
import io.septlieues.workflow.utils.ProcessConstantsRh;
import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;


@Component
public class LrNotifUserSuccessPanache implements JavaDelegate {
    private static final Logger LOG = LoggerFactory.getLogger(LrNotifUserSuccessPanache.class);

    @Autowired
    private RasaService rasaService;

    @Autowired
    IdentityService identityService;

    @Autowired
    private Environment env;

    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    private Gson gson;

    @Value("${rasa.notify.user.success.panache}")
    private String rasaUtterName;


    @Override
    public void execute(DelegateExecution ctx) throws Exception {
        //récupération du tracker id depuis le context de camunda et création du mail à partir de celui-ci
        String TrackerId = (String) ctx.getVariable(ProcessConstantsRh.TRACKER_ID_LR);
        String email = TrackerId.split(":")[0];

        try {
            //demande à rasa d'éxecuter l'action "utter_lr_user_notify_success" sur le tracker id du user
            rasaService.utter(rasaUtterName, TrackerId);
        } catch (Exception e) {
            LOG.error("error when notify manager : " + TrackerId, e);
        }


        try {
            //création et envoie du mail de succés au user
            SimpleMailMessage mail = new SimpleMailMessage();
            LOG.info("********** Email user : "+ email);
            mail.setTo(email);
            mail.setFrom(env.getProperty("spring.mail.username"));
            mail.setSubject("Demande de congé validée.");
            mail.setText("Demande de congé validée.");
            // javaMailSender.send(mail);

        } catch (Exception e) {
            LOG.warn("Could not send email to user", e);
        }

        }

}
