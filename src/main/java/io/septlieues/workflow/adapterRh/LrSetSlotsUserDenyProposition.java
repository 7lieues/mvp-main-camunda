package io.septlieues.workflow.adapterRh;

import com.google.gson.Gson;
import io.septlieues.workflow.model.RasaEvent;
import io.septlieues.workflow.services.BddService;
import io.septlieues.workflow.services.RasaService;

import io.septlieues.workflow.services.UmbrellaNotification;
import io.septlieues.workflow.utils.ProcessConstantsRh;
import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import java.util.Iterator;

@Component
public class LrSetSlotsUserDenyProposition implements JavaDelegate {
    private static final Logger LOG = LoggerFactory.getLogger(LrSetSlotsUserDenyProposition .class);

    @Autowired
    private RasaService rasaService;

    @Autowired
    IdentityService identityService;

    @Autowired
    private Environment env;

    @Autowired
    private Gson gson;

    @Autowired
    private BddService bddService;

    @Autowired
    private UmbrellaNotification umbrellaNotification;
    
    @Value("${rasa.deny.recap.solde}")
    private String rasaDenyRecapSoldes;


    @Override
    public void execute(DelegateExecution ctx) throws Exception {

        // On récup du context Cam le trackerId puis on fabrique le mail du user avec.
        String TrackerId = (String) ctx.getVariable(ProcessConstantsRh.TRACKER_ID_LR);
        String email = TrackerId.split(":")[0];
        String result_final = "";
        
        JSONObject StockRequest = umbrellaNotification.stockLeaveRequest(TrackerId);
        for (Iterator iterator = StockRequest.keys(); iterator.hasNext(); ) {
            Object cle = iterator.next();
            Object val = StockRequest.get(String.valueOf(cle));
            result_final += cle + " : " + val + ", ";
          }
        int tarte = result_final.lastIndexOf(",");
        result_final = result_final.substring(0, tarte);
        result_final += ".";
        LOG.info("la string de conges : " + result_final);

        // on set le slot
        try {
            HttpHeaders requestHeader = new HttpHeaders();
            requestHeader.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
            RasaEvent rasaEvent = new RasaEvent("slot", rasaDenyRecapSoldes, result_final);
            HttpEntity<RasaEvent> requestEntitySetSlot = new HttpEntity<>(rasaEvent, requestHeader);

            rasaService.setSlot(TrackerId, requestEntitySetSlot);

        } catch (Exception e) {
            LOG.error("fail to set les slots deny proposition");

        }

        }

}