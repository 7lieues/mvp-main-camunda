package io.septlieues.workflow.adapterRh;

import com.google.gson.Gson;

import io.septlieues.workflow.services.RasaService;
import io.septlieues.workflow.utils.ProcessConstantsRh;
import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;


@Component
public class LrNotifRappelMng implements JavaDelegate {
    private static final Logger LOG = LoggerFactory.getLogger(LrNotifRappelMng.class);

    @Autowired
    private RasaService rasaService;

    @Autowired
    IdentityService identityService;

    @Autowired
    private Environment env;

    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    private Gson gson;

    @Value("${rasa.notify.manager.respond.request}")
    private String rasaUtterNamePingMng;

    // @Value("${rasa.notify_lr_request.utter.name}")
    // private String rasaUtterNameResendMsg;

    @Override
    public void execute(DelegateExecution ctx) throws Exception {
        //récupération du tracker id depuis le context de camunda et création du mail à partir de celui-ci
        String managerEmail = (String) ctx.getVariable(ProcessConstantsRh.MANAGER_EMAIL);
        String collaborateur = (String) ctx.getVariable(ProcessConstantsRh.APPLICANT);
        String managerTrackerID = (String) ctx.getVariable(ProcessConstantsRh.MANAGER_TRACKER_ID);
        String rasaUtterNameResendMsg = (String) ctx.getVariable(ProcessConstantsRh.MESSAGE_LOOP);

        try {
            //création et envoie du mail de succés au user
            SimpleMailMessage mail = new SimpleMailMessage();
            LOG.info("********** Email user : "+ managerEmail);
            mail.setTo(managerEmail);
            mail.setFrom(env.getProperty("spring.mail.username"));
            mail.setSubject("Demande en attente de validation");
            mail.setText("Bonjour, votre collaborateur : " + collaborateur + " attend une réponse de votre part sur sa demande de congés.");
            // javaMailSender.send(mail);

        } catch (Exception e) {
            LOG.warn("Could not send email to manager", e);
        }

        // String compteur = (String) ctx.getVariable(ProcessConstants.COMPTEUR_PING_MNG);
        // LOG.info("on est sur un rappel de type rappel là, rappel n°" + compteur);
        // int compteur_int = Integer.parseInt(compteur) + 1;
        // ctx.setVariable(ProcessConstants.COMPTEUR_PING_MNG, String.valueOf(compteur_int));
        rasaService.utter(rasaUtterNamePingMng, managerTrackerID);
        rasaService.utter(rasaUtterNameResendMsg, managerTrackerID);
        }

}
