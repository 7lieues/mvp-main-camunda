package io.septlieues.workflow.adapterRh;

import io.septlieues.workflow.services.KeycloakUserService;
import io.septlieues.workflow.utils.ProcessConstantsRh;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.camunda.bpm.extension.keycloak.json.JSONArray;
import org.camunda.bpm.extension.keycloak.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class TsGetUserType implements JavaDelegate {
  private static final Logger LOG = LoggerFactory.getLogger(
    TsGetUserType.class
  );

  @Autowired
  KeycloakUserService keycloakUserService;

  @Value("${manager.timer.notification}")
  private String managerTimerNotification;


  @Override
  public void execute(DelegateExecution ctx) throws Exception {

    ctx.setVariable(ProcessConstantsRh.TIMER_LOOP_NOTIFICATION, managerTimerNotification);
    //on récupère ce qui nous interesse dans le tracker id
    String trackerId = (String) ctx.getVariable(ProcessConstantsRh.TRACKER_ID_TS);
    String addressMail = trackerId.split("::")[0];
    String trackerTemp = trackerId.split("::")[1];
    String requestID = trackerTemp.split("::")[0];

    //on écrit ce qu'on veut garder dans le context de camunda
    ctx.setVariable(ProcessConstantsRh.REQUEST_ID, requestID);
    ctx.setVariable(ProcessConstantsRh.USER_MAIL, addressMail);
    String userType = (String) ctx.getVariable(ProcessConstantsRh.USER_TYPE);
    
    //on verifie qu'on a bien les bonnes valeurs
    LOG.info("********** Tracker ID d'employee : " + trackerId);
    LOG.info("********** Mail d'employee : " + addressMail);
    LOG.info("********** Bout du tracker ID : " + trackerTemp);
    LOG.info("********** Request ID : " + requestID);
    LOG.info("********** User Type : " + userType);

    try {
      //récupération mail manager
      String manager = ( (JSONArray) new JSONObject( keycloakUserService.getKeycloakUserAttributesByUserEmail(addressMail))
          .get("manager")
        ).getString(0);
      String managerEmail = keycloakUserService.searchUserByCustomAttribute("LDAP_ENTRY_DN", manager);

      //vérification de sa valeur
      LOG.info("********** Mail du manager : " + managerEmail);

      //on l'écrit dans le context
      ctx.setVariable(ProcessConstantsRh.MANAGER_EMAIL, managerEmail);
    } catch (Exception e) {
      LOG.error(" can not find the user manager ", e);
    }
  }
}
