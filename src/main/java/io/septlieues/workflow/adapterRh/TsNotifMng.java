package io.septlieues.workflow.adapterRh;

import com.google.gson.Gson;

import io.septlieues.workflow.services.RasaService;
import io.septlieues.workflow.utils.ProcessConstantsRh;
import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
public class TsNotifMng implements JavaDelegate {
    private static final Logger LOG = LoggerFactory.getLogger(TsNotifMng.class);

    @Autowired
    private RasaService rasaService;

    @Autowired
    IdentityService identityService;

    @Autowired
    private Gson gson;
    //cette partie doit etre éliminée aprés 
    @Autowired
    private Environment env;

    @Value("${rasa.notify_ts.utter.name}")
    private String rasaUtterName;

    @Value("${rasa.action.wait.utter.name.ts}")
    private String rasaActionWaiting;

    @Override
    public void execute(DelegateExecution ctx) throws Exception {

        //on récupère le trackerID du user
        String usertrackerID = (String) ctx.getVariable(ProcessConstantsRh.TRACKER_ID_LR);
        //récupération email manager et tracker id dans le context camunda
        String managerEmail = (String) ctx.getVariable(ProcessConstantsRh.MANAGER_EMAIL);
        String managerTrackerId = (String) ctx.getVariable(ProcessConstantsRh.MANAGER_TRACKER_ID);
        ctx.setVariable(ProcessConstantsRh.MESSAGE_LOOP, rasaUtterName);

       try {
            //requête vers rasa avec action à éxecuter(utter_ts_notif_mng) sur le tracker id manager
            rasaService.utter(rasaUtterName, managerTrackerId);

            //demande à rasa de lancer l'action "utter_ts_waiting_response"
            rasaService.utter(rasaActionWaiting, usertrackerID);

        } catch (Exception e) {
            LOG.error("error when notify manager : " + managerTrackerId, e);
        }
        
    }
}
