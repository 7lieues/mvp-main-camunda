package io.septlieues.workflow.adapterRh;

import com.google.gson.Gson;

import io.septlieues.workflow.services.BddService;
import io.septlieues.workflow.services.RasaService;
import io.septlieues.workflow.services.UmbrellaNotification;
import io.septlieues.workflow.utils.ProcessConstantsRh;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class LrSetSlotReorientation implements JavaDelegate {
  private static final Logger LOG = LoggerFactory.getLogger(LrSetSlotReorientation.class);

  @Autowired
  private BddService bddService;

  @Autowired
  private UmbrellaNotification umbrellaNotification;

  @Autowired
  private RasaService rasaService;

  @Autowired
  private Gson gson;

  @Override
  public void execute(DelegateExecution ctx) throws Exception {

    //récupération de trackerid, leavetype et leave numberdays depuis le contexte camunda et création mail user à partir du tracker id
    String firstleaveType = (String) ctx.getVariable(ProcessConstantsRh.FIRST_LEAVE_TYPE);

    ctx.setVariable(ProcessConstantsRh.LEAVE_TYPE, firstleaveType);



  }
}
