package io.septlieues.workflow.adapterRh;

import com.google.gson.Gson;

import io.septlieues.workflow.model.RasaEvent;
import io.septlieues.workflow.services.BddService;
import io.septlieues.workflow.services.RasaService;
import io.septlieues.workflow.services.UmbrellaNotification;
import io.septlieues.workflow.utils.ProcessConstantsRh;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import java.util.Iterator;

@Component
public class LrBalanceCheckPanache implements JavaDelegate {
  private static final Logger LOG = LoggerFactory.getLogger(LrBalanceCheckPanache.class);

  @Autowired
  private BddService bddService;

  @Autowired
  private UmbrellaNotification umbrellaNotification;

  @Autowired
  private RasaService rasaService;

  @Autowired
  private Gson gson;

  @Value("${rasa.panache.redirection.ask}")
  private String rasaPanacheRedirectionAsk;

  @Value("${rasa.slot.leave.type}")
  private String rasaLeaveTypeSlot;

  @Override
  public void execute(DelegateExecution ctx) throws Exception {

    //récupération de trackerid, leavetype et leave numberdays depuis le contexte camunda et création mail user à partir du tracker id
    String trackerId = (String) ctx.getVariable(ProcessConstantsRh.TRACKER_ID_LR);
    String addressMail = trackerId.split("::")[0];
    String firstleaveType = (String) ctx.getVariable(ProcessConstantsRh.FIRST_LEAVE_TYPE);
    String secondleaveType = (String) ctx.getVariable(ProcessConstantsRh.SECOND_LEAVE_TYPE);
    String LeaveStart = (String) ctx.getVariable(ProcessConstantsRh.LEAVE_START);
    String LeaveEnd = (String) ctx.getVariable(ProcessConstantsRh.LEAVE_END);
    
    //convertion de format et initialisation des variables tampon
    // int user_id = 0;
    int first_balance_bd = 0;
    int second_balance_bd = 0;
    int final_balance = 0;
    
    String first_balance = "";
    String second_balance = "";


    JSONObject ReadRequest = umbrellaNotification.readLeaveRequest(trackerId, firstleaveType, LeaveStart, LeaveEnd);
    LOG.info("test affichage du Json du read : " + ReadRequest);

    //set dans le context de la valeur du nbr de jour demandé
    Object numberDays = ReadRequest.get(String.valueOf("nbr_leave_days"));
    LOG.info("********** Nombre de jours demandés : " + numberDays);
    ctx.setVariable(ProcessConstantsRh.LEAVE_NUMBER_DAYS, String.valueOf(numberDays));
    
    int leaveValueDays = (Integer) numberDays;

    JSONObject StockRequest = umbrellaNotification.stockLeaveRequest(trackerId);
    for (Iterator iterator = StockRequest.keys(); iterator.hasNext(); ) {
        Object cle = iterator.next();
        Object val = StockRequest.get(String.valueOf(cle));
        if (cle.equals(firstleaveType)) {
          first_balance += val;
        }
        if (cle.equals(secondleaveType)) {
          second_balance += val;
        }
      }


      LOG.info("********** first leaveType : " + firstleaveType + "   Balance :" + first_balance);
      ctx.setVariable(ProcessConstantsRh.FIRST_BALANCE, first_balance);

      
      LOG.info("********** second leaveType : " + secondleaveType + "   Balance :" + second_balance);
      ctx.setVariable(ProcessConstantsRh.SECOND_BALANCE, second_balance);

      int additional_balance_bd = Integer.parseInt(first_balance) + Integer.parseInt(second_balance);
      String additional_balance = String.valueOf(additional_balance_bd);
      ctx.setVariable(ProcessConstantsRh.ADDITION_BALANCE, additional_balance);
      LOG.info("addition des deux soldes : " + additional_balance_bd);
      

    try {

      // //récupération user id 
      // user_id = Integer.parseInt(bddService.sqlRequestOneCondition(ProcessConstants.SQL_PRIMARY_KEY_USER_ID, ProcessConstants.SQL_USER, ProcessConstants.SQL_EMAIL, addressMail));
      // LOG.info("********** getting user_id from data base via son email " + user_id);

      // //on stock ce user_id dans le contexte de camunda
      // String userId=String.valueOf(user_id);
      // ctx.setVariable(ProcessConstants.USER_ID, userId);

      // //on récupère l'id du leave type
      // int first_leave_type_id = Integer.parseInt(bddService.sqlRequestOneCondition(ProcessConstants.SQL_LR_TYPE_ID, ProcessConstants.SQL_TABLE_LEAVE_TYPE, ProcessConstants.SQL_DESCRIPTION, firstleaveType));
      // ctx.setVariable(ProcessConstants.FIRST_LEAVE_TYPE_ID, first_leave_type_id);
      // int second_leave_type_id = Integer.parseInt(bddService.sqlRequestOneCondition(ProcessConstants.SQL_LR_TYPE_ID, ProcessConstants.SQL_TABLE_LEAVE_TYPE, ProcessConstants.SQL_DESCRIPTION, secondleaveType));
      // LOG.info("lr type id du deuxième :: " + second_leave_type_id);
      // ctx.setVariable(ProcessConstants.SECOND_LEAVE_TYPE_ID, second_leave_type_id);

      // //on récupère ensuite le balance avec l'id du leave type que l'on vient de récupérer
      // first_balance_bd = Integer.parseInt(bddService.sqlRequestTwoCondition(ProcessConstants.SQL_SOLDE, ProcessConstants.SQL_TABLE_LEAVE_STOCK, ProcessConstants.SQL_TABLE_LEAVE_TYPE_ID, String.valueOf(first_leave_type_id), ProcessConstants.SQL_USER_ID, String.valueOf(user_id)));
      // String first_balance=String.valueOf(first_balance_bd);
      // second_balance_bd = Integer.parseInt(bddService.sqlRequestTwoCondition(ProcessConstants.SQL_SOLDE, ProcessConstants.SQL_TABLE_LEAVE_STOCK, ProcessConstants.SQL_TABLE_LEAVE_TYPE_ID, String.valueOf(second_leave_type_id), ProcessConstants.SQL_USER_ID, String.valueOf(user_id)));
      // String second_balance=String.valueOf(second_balance_bd);
      // int additional_balance_bd = first_balance_bd + second_balance_bd;
      // String additional_balance=String.valueOf(additional_balance_bd);
      //on l'écrit dans le contexte de camunda

      boolean redirection = false;
      ctx.setVariable(ProcessConstantsRh.REDIRECTION, redirection);

      first_balance_bd = Integer.parseInt(first_balance);
      if (first_balance_bd >= leaveValueDays) {
        redirection = true;
        LOG.info("********** Premier type de conge suffisant *********");
        ctx.setVariable(ProcessConstantsRh.REDIRECTION, redirection);

        HttpHeaders requestHeader = new HttpHeaders();
        requestHeader.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        RasaEvent rasaEvent = new RasaEvent("slot", rasaLeaveTypeSlot, firstleaveType);
        HttpEntity<RasaEvent> requestEntitySetSlot = new HttpEntity<>(rasaEvent, requestHeader);
        rasaService.setSlot(trackerId, requestEntitySetSlot);
        
        rasaService.utter(rasaPanacheRedirectionAsk, trackerId);
      }

      //check si le solde est suffisant par rapport au nombre de jours demandés
      if (additional_balance_bd >= leaveValueDays) {
        final_balance = additional_balance_bd - leaveValueDays;
        LOG.info("********** final_balance : " + final_balance);
        String finalBalance=String.valueOf(final_balance);
        ctx.setVariable(ProcessConstantsRh.FINAL_BALANCE, finalBalance);

        boolean sufficientBalance = true;
        LOG.info("********** SUFFICIENT BALANCE ********* ");
        ctx.setVariable(ProcessConstantsRh.SUFFICIENT_BALANCE, sufficientBalance);
      } else {
        boolean sufficientBalance = false;
        ctx.setVariable(ProcessConstantsRh.SUFFICIENT_BALANCE, sufficientBalance);
        LOG.info("********** INSUFFICIENT BALANCE ********* ");
      }
    } catch (Exception e) {
      LOG.error(" Erreur with balance value block ", e);
    }
  }
}
