package io.septlieues.workflow.adapterRh;


import io.septlieues.workflow.services.RasaService;
import io.septlieues.workflow.utils.ProcessConstantsRh;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class TsAskTimeSheet implements JavaDelegate {

    private static final Logger LOG = LoggerFactory.getLogger(TsAskTimeSheet.class);

    @Autowired
    private RasaService rasaService;

    @Value("${rasa.utter.ask.timesheet}")
    private String rasaNotifUser;


    @Override
    public void execute(DelegateExecution ctx) throws Exception {
        //on récupère le tracker id du user
        String trackerId = (String) ctx.getVariable(ProcessConstantsRh.TRACKER_ID_TS);
        String userType = (String) ctx.getVariable(ProcessConstantsRh.USER_TYPE);
        LOG.info("!!!!!!!!!!!!!!!!! CHECKPOINT !!!!!!!!!!!!!!!");
        LOG.info("******** userType: " + userType);
        try {
            //on demande à rasa de lancer l'action "utter_tl_validated_resp" sur le tracker id du user
            rasaService.utter(rasaNotifUser, trackerId);
        } catch (Exception e) {
            LOG.info("!!!!!!!!!!!!!!!!! ERROR !!!!!!!!!!!!!!!");
        }

    }
}