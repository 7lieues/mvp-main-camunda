package io.septlieues.workflow.adapterRh;

import com.google.gson.Gson;

import io.septlieues.workflow.services.RasaService;
import io.septlieues.workflow.utils.ProcessConstantsRh;
import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

// une classe qui informe le user que sont timeSheet est non validé
@Component
public class TsNotifUserDeny implements JavaDelegate {
  private static final Logger LOG = LoggerFactory.getLogger(
    TsNotifUserDeny.class
  );

  @Autowired
  private RasaService rasaService;

  @Autowired
  IdentityService identityService;

  @Autowired
  private Environment env;

  @Autowired
  private JavaMailSender javaMailSender;

  @Autowired
  private Gson gson;

  @Value("${rasa.deny.ts.user}")
    private String rasaUtterName;

  @Override
  public void execute(DelegateExecution ctx) throws Exception {

    //récuperation tracker id dans le context camunda
    String TrackerId = (String) ctx.getVariable(ProcessConstantsRh.TRACKER_ID_TS);
    
    //fabrication mail
    String email = TrackerId.split(":")[0];

       try {
            //demande à rasa d'éxécuter l'action "utter_ts_user_notify_deny"
            rasaService.utter(rasaUtterName, TrackerId);

        } catch (Exception e) {
            LOG.error("error when notify manager : " + TrackerId, e);
        }
    

    try {
      
      //paramétrage et envoie du mail par camunda
      SimpleMailMessage mail = new SimpleMailMessage();
      LOG.info("********** Email user : " + email);
      mail.setTo(email);
      mail.setFrom(env.getProperty("spring.mail.username"));
      mail.setSubject("TimeSheet réfusé.");
      mail.setText("Votre timeSheet n'est pas valide , veuillez resseiller.");
      // javaMailSender.send(mail);
      LOG.warn("The mail validation is sent to user email" + email);

    } catch (Exception e) {
      LOG.warn("Could not send email to user", e);
    }
  }
}
