package io.septlieues.workflow.adapterRh;


import io.septlieues.workflow.services.KeycloakUserService;
import io.septlieues.workflow.utils.ProcessConstantsRh;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
//une Classe pour avoir le type de user

@Component
public class TlGetUserType implements JavaDelegate {
    
    @Autowired
    KeycloakUserService keycloakUserService;
    
    private static final Logger LOG = LoggerFactory.getLogger(TlGetUserType.class);

    @Value("${manager.timer.notification}")
    private String managerTimerNotification;


    @Override
    public void execute(DelegateExecution ctx) throws Exception {

        ctx.setVariable(ProcessConstantsRh.TIMER_LOOP_NOTIFICATION, managerTimerNotification);
        // On recup le trackerId puis à partir de ce dernier on fabrique le mail du user puis on l'écrit dans le context Cam
        String trackerId = (String) ctx.getVariable(ProcessConstantsRh.TRACKER_ID_TL);
        String addressMail = trackerId.split("::")[0];
        LOG.info("********** Track ID  : "+ trackerId);
        ctx.setVariable(ProcessConstantsRh.USER_MAIL,addressMail);

        // Ici on récupère les valeurs des variables suivantes à partir du context Camunda : userType, distance, cost, travelType, travelClass 
        String userType = (String) ctx.getVariable(ProcessConstantsRh.USER_TYPE);
        String distance = (String) ctx.getVariable(ProcessConstantsRh.TRAVEL_DISTANCE);
        String cost = (String) ctx.getVariable(ProcessConstantsRh.TRAVEL_COST);
        String travelType = (String) ctx.getVariable(ProcessConstantsRh.TRAVEL_TYPE);
        String travelClass = (String) ctx.getVariable(ProcessConstantsRh.TRAVEL_CLASS);
        LOG.info("********** UserType  : "+ userType);
        LOG.info("********** distance  : "+ distance);
        LOG.info("********** cost  : "+ cost);
        LOG.info("********** travel Type  : "+ travelType);
        LOG.info("********** travelClass  : "+ travelClass);

        // Ici on va stocker le contenu des variables distance et cost dans le context Camunda pour la suite du process
        ctx.setVariable(ProcessConstantsRh.TRAVEL_DISTANCE,Integer.parseInt(distance));
        ctx.setVariable(ProcessConstantsRh.TRAVEL_COST,Integer.parseInt(cost));

        // On récupère la valeur de la variable de TRAVEL_COST à partir du context Cam
        String a = ctx.getVariable(ProcessConstantsRh.TRAVEL_COST).getClass().getName();
        LOG.info("Le type de cost est : "+ a);

    }
}