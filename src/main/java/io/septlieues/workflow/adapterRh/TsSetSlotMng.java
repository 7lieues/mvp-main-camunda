package io.septlieues.workflow.adapterRh;

import com.google.gson.Gson;

import io.septlieues.workflow.model.RasaEvent;
import io.septlieues.workflow.services.RasaService;
import io.septlieues.workflow.utils.ProcessConstantsRh;
import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.camunda.bpm.engine.identity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;


@Component
public class TsSetSlotMng implements JavaDelegate {
    private static final Logger LOG = LoggerFactory.getLogger(TsSetSlotMng.class);

    @Autowired
    private RasaService rasaService;

    @Autowired
    IdentityService identityService;

    @Autowired
    private Gson gson;

    @Value("${rasa.timesheet}")
    private String rasaTimeSheet;
    @Value("${rasa.username}")
    private String rasaApplicant;


    @Override
    public void execute(DelegateExecution ctx) throws Exception {
        
        //récuération de valeurs dans le context camunda et fabrication mail user
        String userTrackerID = (String) ctx.getVariable(ProcessConstantsRh.TRACKER_ID_TS);
        String managerEmail = (String) ctx.getVariable(ProcessConstantsRh.MANAGER_EMAIL);
        String userEmail = userTrackerID.split("::")[0];
        String requestID = (String) ctx.getVariable(ProcessConstantsRh.REQUEST_ID);
        String timeSheet = (String) ctx.getVariable(ProcessConstantsRh.TIME_SHEET);

        //BU = RH
        String BU = (String) ProcessConstantsRh.BU;

        //fabrication tracker id de manager
        String managerTrackerId = managerEmail.concat("::").concat(requestID).concat("::").concat("1").concat("::").concat(BU);

        //on l'écrit dans le context camunda
        ctx.setVariable(ProcessConstantsRh.MANAGER_TRACKER_ID, managerTrackerId);

        //vérification des valeurs
        LOG.info("********** Mail manager : " + managerEmail);
        LOG.info("********** Mail user : " + userEmail);
        LOG.info("********** Tracker ID manager : " + managerTrackerId);

        User user = identityService.createUserQuery().userId(userEmail).singleResult();
        String userFullName=userEmail;
        if(user!=null) {
            userFullName = user.getFirstName() + " " + user.getLastName();
            LOG.info("********** Nom de l'employee : " + userFullName);
        }
        else {
            userFullName = userEmail;
        }

        try {
            //création header
            HttpHeaders requestHeader = new HttpHeaders();
            requestHeader.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);


            //paramétrage requete avant envoie à rasa (cf demande de remplir le slot "ent_slot_ts_user_name" avec userfullname)
            RasaEvent rasaEvent1 = new RasaEvent("slot", rasaApplicant, userFullName);
            HttpEntity<RasaEvent> requestEntitySetSlot1 = new HttpEntity<>(rasaEvent1, requestHeader);
            //envoie
            rasaService.setSlot(managerTrackerId, requestEntitySetSlot1);
            // rasaService.setSlot(managerTrackerId, requestEntitySetSlot1);

            //paramétrage requete avant envoie à rasa (cf demande de remplir le slot "ent_slot_ts_timesheet" avec timeshseet)
            RasaEvent rasaEvent = new RasaEvent("slot", rasaTimeSheet, timeSheet);
            HttpEntity<RasaEvent> requestEntitySetSlot = new HttpEntity<>(rasaEvent, requestHeader);
            //envoie
            rasaService.setSlot(managerTrackerId, requestEntitySetSlot);

        } catch (Exception e) {
            LOG.error("error when set slot rasa for timeSheet : " + timeSheet + "and user "+userFullName , e);
        }

    }
}
