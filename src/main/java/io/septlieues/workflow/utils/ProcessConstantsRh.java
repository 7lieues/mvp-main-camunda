package io.septlieues.workflow.utils;

public class ProcessConstantsRh {
    
    // [OLIV] REMARQUE : 
    // public static final String TRACKER_ID_LR = "trackerID"; et tous les autres pointent sur le même trackerID
    // Ca ne semble pas être une bonne pratique, dans la mesure où c'est une variable dans laquelle on écrit
    
    // ---------------------- General + Exception ---------------------- //
    
        public static final String BU = "rh";
        public static final String TASK_ID ="task_id";
        public static final String MANAGER_EMAIL = "manager_email";
        public static final String MANAGER_TRACKER_ID = "manager_tracker_id";
        public static final String COMMENT = "rejection_comment";
        public static final String REQUEST_ID = "request_id";
        public static final String APPLICANT = "demandeur";
        public static final String APPROVED ="approved";
        public static final String USER_ID= "userId";
        public static final String USER_MAIL ="userMail";
        public static final String NEED_VALIDATION= "needValidation"; 
        public static final String MNG_APPROVED= "mngApproved";
        
        public static final String USER_TYPE ="userType";// used in travels and timeSheet
        public static final String SUPPORTED ="supported";

        public static final String COMPTEUR_PING_MNG ="compteur";
        public static final String PROPOSITION ="proposition";
        public static final String MESSAGE_LOOP = "messageLoop";
        public static final String TIMER_LOOP_NOTIFICATION = "timerLoopNotification";

        // Permet d'activer un envoi "générique" de message à l'utilisateur
        public static final String ERROR_MESSAGE_USER_FLAG = "errorMessageUserFlag";
        public static final String ERROR_MESSAGE_USER_CONTENT = "errorMessageUserContent";

    // ----------------------------------------------------------------- //
    
    // ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| //
    // ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| //
    // ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| //

    // ------------------------- LeaveRequest -------------------------- //
        
        public static final String TRACKER_ID_LR = "trackerID";
        public static final String LEAVE_TYPE = "LeaveType";
        public static final String LEAVE_START = "LeaveStart";
        public static final String LEAVE_END = "LeaveEnd";
        public static final String LEAVE_NUMBER_DAYS = "LeaveNumberDays";
        public static final String BALANCE= "balance";
        public static final String SUFFICIENT_BALANCE= "sufficientBalance";
        public static final String FINAL_BALANCE= "finalBalance";
        public static final String LEAVE_TYPE_ID ="leave_type_id";
        public static final String LIST_DATE_LR ="lr_list_date";
        public static final String CONGES ="CONGES";
        public static final String SOLDE ="SOLDE";

        public static final String PANACHE_BOOL = "panacheBool";
        public static final String PANACHE_ERROR = "panacheError";
        public static final String FIRST_LEAVE_TYPE = "FirstLeaveType";
        public static final String SECOND_LEAVE_TYPE = "SecondLeaveType";
        public static final String FIRST_LEAVE_TYPE_ID = "FirstLeaveTypeId";
        public static final String SECOND_LEAVE_TYPE_ID = "SecondLeaveTypeId";
        public static final String FIRST_BALANCE = "FirstBalance";
        public static final String SECOND_BALANCE = "SecondBalance";
        public static final String ADDITION_BALANCE = "AdditionBalance";
        public static final String NEED_FIRST_VALIDATION= "needFirstValidation";
        public static final String NEED_SECOND_VALIDATION= "needSecondValidation";

        public static final String LEAVE_REQUEST_ID= "leave_request_id";
        public static final String REDIRECTION= "redirection";
        public static final String NOPROPOSITION= "noproposition";
        public static final String PROPOSITIONONETYPE = "propositiononetype";
        public static final String REORIENTATIONONETYPE= "reorientationonetype";
        

    // ----------------------------------------------------------------- //

    // ------------------- Variabilisation lr sql --------------------- //
        
        public static final String SQL_TABLE_LEAVE_TYPE = "days_leave_types";
        public static final String SQL_TABLE_LEAVE_STOCK = "days_leave_stocks";
        public static final String SQL_TABLE_LEAVE_TYPE_ID = "days_leave_type_id";
        public static final String SQL_DESCRIPTION = "description";
        public static final String SQL_LR_TYPE_ID = "id";
        public static final String SQL_PRIMARY_KEY_USER_ID = "id";
        public static final String SQL_SOLDE = "balance";
        public static final String SQL_USER_ID = "user_id";
        public static final String SQL_USER = "users";
        public static final String SQL_EMAIL = "email";
        public static final String SQL_ANNUAL_BALANCE = "annual_balance";
        public static final String SQL_LR_TYPE_RTT = "RTT";
        public static final String SQL_LR_TYPE_CP = "CP";
        public static final String SQL_LISTE_TOUT_CONGES = "RTT,CP,Exceptionnel,Magique";
    // ----------------------------------------------------------------- //

    // ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| //
    // ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| //
    // ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| //

    // --------------------------- Travels ----------------------------- //
        
        public static final String TRACKER_ID_TL = "trackerID";
        public static final String TRAVEL_TYPE= "travelType";
        public static final String TRAVEL_CLASS= "travelClass";
        public static final String TRAVEL_COST= "travelCost";
        public static final String TRAVEL_DISTANCE= "travelDistance";
        public static final String TYPE_APPROVED= "typeApproved"; 
        public static final String CLASS_APPROVED= "classApproved";

    // ----------------------------------------------------------------- //

    // ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| //
    // ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| //
    // ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| //

    // -------------------------- TimeSheet ---------------------------- //
        
        public static final String TRACKER_ID_TS = "trackerID";
        public static final String TIME_SHEET ="timeSheet";

    // ----------------------------------------------------------------- //




}
