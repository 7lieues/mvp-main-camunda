package io.septlieues.workflow.utils;

public class ProcessConstantsAchat {

    public static final String TRACKER_ID = "tracker_id";
    public static final String QUANTITY = "quantity";
    public static final String PRODUCT_PRICE = "price";
    public static final String DELIVERY_DATE = "deliveryDate";
    public static final String PRODUCT_CATALOG = "json_catalogue";
    public static final String APPLICANT_ROLE = "roleDemandeur";
    public static final String APPLICANT = "demandeur";
    public static final String IS_APPLICANT_MANAGER = "isManager";
    public static final String MANAGER = "manager";
    public static final String MANAGER_TRACKER_ID = "manager_tracker_id";
    public static final String ACHETEUR_TRACKER_ID = "acheteur_tracker_id";
    public static final String MANAGER_EMAIL = "manager_email";
    public static final String ACHETEUR_EMAIL = "acheteur_email";
    public static final String PRODUCT_TYPE ="productType";
    public static final String MANAGER_COMMENT ="manager_comment";
    public static final String REJECTION_COMMENT ="rejection_comment";
    public static final String TASK_ID ="task_id";
    public static final String APPROVED ="approved";
    public static final String PRODUCT_SCALE = "gammeProduitAutorisee";
    public static final String STOCK = "stock";
    public static final String USER_MAIL = "user_mail";
    public static final String PRODUCT_REF = "productRef";
    public static final String REQUEST_ID = "request_id";
    public static final String BU = "achat";
    public static final String AVAILABLE = "available";
    public static final String ERROR_MESSAGE_USER_CONTENT = "error_msg";


    // ------------------- Variabilisation achat catalogue sql --------------------- //
        
    public static final String SQL_PRODUCTS = "products";
    public static final String SQL_QUANTITY = "quantity";
    public static final String SQL_REF = "ref";
    public static final String SQL_SCALE = "scale";
    public static final String SQL_PRICE = "price";
    public static final String SQL_NAME = "name";
    public static final String SQL_TYPE = "type";
    public static final String SQL_IMAGE_URL = "image_url";
    public static final String SQL_DESCRIPTION = "description";
    // ----------------------------------------------------------------- //



    // ------------------- Variabilisation achat contrat --------------------- //
    
    public static final String CONTRACT_REF = "contractRef";
    public static final String TABLE_CONTRACTS = "contracts";
    public static final String TABLE_SUPPLIERS = "suppliers";
    public static final String TABLE_ENGAGEMENTS = "financial_engagements";
    public static final String SQL_CONTRACT_NAME = "technical_name";
    public static final String SQL_CONTRACT_ID = "id";
    public static final String SQL_TABLE_CONTRACT_COLUMN_SUPPLIER_ID = "supplier_id";
    public static final String SQL_SUPPLIER_NAME = "name";
    public static final String SUPPLIER_NAME = "supplier_name";
    public static final String SQL_SUPPLIER_STATUS = "status";
    public static final String SUPPLIER_STATUS = "supplier_status";
    public static final String SQL_SUPPLIER_ID = "id";
    public static final String SQL_CONTRACT_VALIDITY_START = "validity_start";
    public static final String SQL_CONTRACT_VALIDITY_END = "validity_end";
    public static final String CONTRACT_VALIDITY_START = "validity_start";
    public static final String CONTRACT_VALIDITY_END = "validity_end";
    public static final String SQL_CONTRACT_MAX_AMOUNT = "max_amount";
    public static final String SQL_ENGAGEMENT_AMOUNTS_ENGAGED = "amount_engaged";
    public static final String SQL_ENGAGEMENT_CONTRACT_ID = "contract_id";
    
    public static final String CHECK_CONTRACT = "check_contract";
    public static final String AMOUNT_ASKED = "engagement_amount";
    public static final String CHECK_AMOUNT = "check_amount";
    public static final String USER_ROLE = "user_role";
    public static final String VALIDATION_TYPE = "validation_type";


    // ------------------- Variabilisation achat famille --------------------- //
    
    public static final String CATEGORY_NAME = "categoryName";
    public static final String CHECK_CATEGORY = "check_category";

    
    
}
