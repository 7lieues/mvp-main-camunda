package io.septlieues.workflow.adapterAchat;


import io.septlieues.workflow.services.RasaService;
import io.septlieues.workflow.utils.ProcessConstantsAchat;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

@Component
public class PurCommunicatePONoStock implements JavaDelegate {
    private static final Logger LOG = LoggerFactory.getLogger(PurCommunicatePONoStock.class);
    @Autowired
    private Environment env;
    @Value("${rasa.notify_user.form.name}")
    private String rasaFormName;
    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    private RasaService rasaService;

    @Override
    public void execute(DelegateExecution ctx) throws Exception {

        // On recup le tracker Id, le manager, la ref du produit (+ on construit l'email du user avec son trackerId)
        String trackerId = (String) ctx.getVariable(ProcessConstantsAchat.TRACKER_ID);
        String adressMail = trackerId.split(":")[0];
        String manager = (String) ctx.getVariable(ProcessConstantsAchat.MANAGER);
        String productRef = (String) ctx.getVariable(ProcessConstantsAchat.PRODUCT_REF);

        try {

            // Envoie d'un mail de notif au user
            SimpleMailMessage mail = new SimpleMailMessage();
            LOG.info("Start sending mail for PO rupture de stock ..", " to :" + adressMail);
            mail.setTo(adressMail);
            mail.setFrom(env.getProperty("spring.mail.username"));
            mail.setSubject("Demande Achat");
            mail.setText("Votre demande d'achat du produit " + productRef + " est validée par votre manager " + manager + " mais celui-ci est en rupture de stock. Nous vous proposons de choisir un autre modèle via Wilson.");
            // javaMailSender.send(mail);
            LOG.info("mail send with success");

            // execution du utter de notif
            try {
                rasaService.utter(rasaFormName, trackerId);
            } catch (Exception e) {
                LOG.error("cannot Notify Rasa " + e);
            }

        } catch (Exception e) {
            LOG.error("Could not send email to assignee", e);
        }
    }
}
