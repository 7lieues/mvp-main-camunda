package io.septlieues.workflow.adapterAchat;

import com.google.gson.Gson;

import io.septlieues.workflow.services.RasaService;
import io.septlieues.workflow.utils.ProcessConstantsAchat;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

@Component
public class PurDispCatalog implements JavaDelegate {

    @Autowired
    private Gson gson;

    private static final Logger LOG = LoggerFactory.getLogger(PurDispCatalog.class);

    /*the config of utter to send to notify Rasa parsed from yml file*/
    @Value("${rasa.action.catalog.name}")
    private String rasaUtterName;

    @Autowired
    private RasaService rasaService;

    @Override
    public void execute(DelegateExecution ctx) throws Exception {
        String trackerId = (String) ctx.getVariable(ProcessConstantsAchat.TRACKER_ID);
        //add json forma to header
        HttpHeaders requestHeader = new HttpHeaders();
         try {
            LOG.info("******************* lancement action rasa");
            rasaService.utter(rasaUtterName, trackerId);//uter & action
         } catch (Exception e) {
             LOG.error("error in RasaService ", e);
         }
    }

}
