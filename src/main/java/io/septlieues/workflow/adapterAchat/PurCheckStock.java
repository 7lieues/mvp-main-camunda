package io.septlieues.workflow.adapterAchat;

import com.google.gson.Gson;

import io.septlieues.workflow.doa.RepoProducts;
import io.septlieues.workflow.model.Product;
import io.septlieues.workflow.services.RasaService;
import io.septlieues.workflow.utils.ProcessConstantsAchat;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.sql.*;

@Component
public class PurCheckStock implements JavaDelegate {


    @Autowired
    private Gson gson;

    private static final Logger LOG = LoggerFactory.getLogger(PurCheckStock.class);

    @Autowired
    private Environment env;

    @Autowired
    private RasaService rasaService;

    @Autowired
    private RepoProducts repoProducts;


    @Override
    public void execute(DelegateExecution ctx) throws Exception {

        String ref_produit = (String) ctx.getVariable(ProcessConstantsAchat.PRODUCT_REF);// de pref on recoit le id et non pas le ref
        LOG.info("****** ref: " + ref_produit);
        Product product =  repoProducts.retrieveProductRef(ref_produit);
        LOG.info("****** product by ref  to String  : " + product.toString());
        ctx.setVariable(ProcessConstantsAchat.STOCK, product.getQuantity());

    /*
      //paramètre connexion à la base  de donnée
      String url = env.getProperty("spring.datasource-business.url");
      String user = env.getProperty("spring.datasource-business.username");
      String passwd = env.getProperty("spring.datasource-business.password");
      LOG.info("******************* check stock *******************");
      LOG.info("****** url : " + url);
      LOG.info("****** password : " + passwd);
      LOG.info("****** user : " + user);

      String ref_produit = "";
      ref_produit = (String) ctx.getVariable(ProcessConstantsAchat.PRODUCT_REF);
      LOG.info("****** ref: " + ref_produit);
      
      //connexion bdd
      Connection conn = DriverManager.getConnection(url, user, passwd);
      Statement state = conn.createStatement();

      //querry
      String sql = ("SELECT " + ProcessConstantsAchat.SQL_QUANTITY + " from " + ProcessConstantsAchat.SQL_PRODUCTS + " where " + ProcessConstantsAchat.SQL_REF + " = '" + ref_produit + "'");
      LOG.info("***requête sql   " + sql);
      ResultSet result = state.executeQuery(sql);
      ResultSetMetaData resultMeta = result.getMetaData();
      
      //On affiche le nom des produits et on le stocke dans une string
      Integer stock_produit = 0;
      while (result.next()) {
        stock_produit = Integer.parseInt(result.getString(ProcessConstantsAchat.SQL_QUANTITY));
      }
      //on inscrit le stock du produit choisie dans le context de camunda
      ctx.setVariable(ProcessConstantsAchat.STOCK, stock_produit);
      LOG.info("stock du produit demandé : " + stock_produit);
      LOG.info("stock du produit demandé  par methode JPQL: " + product.getQuantity());
      result.close();
      state.close(); */

    //return;
  }
}
