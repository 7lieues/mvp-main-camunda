package io.septlieues.workflow.adapterAchat;

import com.google.gson.Gson;

import io.septlieues.workflow.doa.RepoProducts;
import io.septlieues.workflow.model.Product;
import io.septlieues.workflow.services.RasaService;
import io.septlieues.workflow.utils.ProcessConstantsAchat;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.sql.*;

@Component
public class PurUpdateStock implements JavaDelegate {


    @Autowired
    private RepoProducts repoProducts;

    private static final Logger LOG = LoggerFactory.getLogger(PurUpdateStock.class);

    @Autowired
    private Environment env;

    @Autowired
    private RasaService rasaService;

    @Override
    public void execute(DelegateExecution ctx) throws Exception {

      LOG.info("******************* update stock *******************");
    /*
      //paramètre connexion à la base de donnée
      String url = env.getProperty("spring.datasource-business.url");
      String user = env.getProperty("spring.datasource-business.username");
      String passwd = env.getProperty("spring.datasource-business.password");
      LOG.info("****** url : " + url);
      LOG.info("****** password : " + passwd);
      LOG.info("****** user : " + user);

      //récupération stock et ref produit depuis le context de camunda
      Integer stock = (Integer) ctx.getVariable(ProcessConstantsAchat.STOCK);
      int nouveau_stock = stock -1;
      String ref_produit = "";
      ref_produit = (String) ctx.getVariable(ProcessConstantsAchat.PRODUCT_REF);
      LOG.info("****** ref: " + ref_produit);

      //connexion bdd
      Connection conn = DriverManager.getConnection(url, user, passwd);
      Statement state = conn.createStatement();
      
      //querry
      String sql = ("Update " + ProcessConstantsAchat.SQL_PRODUCTS + " set " + ProcessConstantsAchat.SQL_QUANTITY + " = '" + nouveau_stock + "'" + " where " + ProcessConstantsAchat.SQL_REF + " = '" + ref_produit + "'");
      LOG.info("Requête sql : " + sql);
      int update = state.executeUpdate(sql);

      state.close();

      LOG.info("******************* verif stock *******************");
      Statement state1 = conn.createStatement();

      String sql1 = ("select " + ProcessConstantsAchat.SQL_QUANTITY + " from " + ProcessConstantsAchat.SQL_PRODUCTS + " where " + ProcessConstantsAchat.SQL_REF +  " = '" + ref_produit + "'");
      LOG.info("Deuxième requête sql : " + sql1);
      ResultSet result1 = state1.executeQuery(sql1);

      Integer stock_produit = 0;
      while (result1.next()) {
        stock_produit = Integer.parseInt(result1.getString(ProcessConstantsAchat.SQL_QUANTITY));
      }
      LOG.info("le nouveau stock dans la base de donnée pour le produit ref = " + ref_produit + " est de : " + stock_produit);
      result1.close();
      state1.close();

        */
        Integer stock = (Integer) ctx.getVariable(ProcessConstantsAchat.STOCK);
        LOG.info("le stock dans la base de donnée pour le produit est de : " + stock);
      String ref_produit = (String) ctx.getVariable(ProcessConstantsAchat.PRODUCT_REF);
      Product product= repoProducts.retrieveProductRef(ref_produit);
      product.setQuantity(product.getQuantity()-1);
      repoProducts.save(product);
      Product productVerif = repoProducts.retrieveProductRef(ref_produit);
      LOG.info("le nouveau stock dans la base de donnée pour le produit ref = " + productVerif.getRef() + " est de : " + productVerif.getQuantity());




    }

}
