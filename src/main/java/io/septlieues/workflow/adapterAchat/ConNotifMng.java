package io.septlieues.workflow.adapterAchat;// package io.septlieues.camunda.ha.adapter;

// import com.google.gson.Gson;
// import io.septlieues.camunda.ha.model.RasaEvent;
// import io.septlieues.camunda.ha.services.RasaService;
// import io.septlieues.camunda.ha.utils.ProcessConstants;
// import org.camunda.bpm.engine.IdentityService;
// import org.camunda.bpm.engine.delegate.DelegateExecution;
// import org.camunda.bpm.engine.delegate.JavaDelegate;
// import org.camunda.bpm.engine.identity.User;
// import org.slf4j.Logger;
// import org.slf4j.LoggerFactory;
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.beans.factory.annotation.Value;
// import org.springframework.stereotype.Component;


// @Component
// public class ConNotifMng implements JavaDelegate {
//     private static final Logger LOG = LoggerFactory.getLogger(ConNotifMng.class);

//     @Autowired
//     private RasaService rasaService;

//     @Autowired
//     IdentityService identityService;

//     @Autowired
//     private Gson gson;

//     /*the config of utter to send to notify Rasa parsed from yml file*/
//     // ACTIONS RASA À CRÉER !
//     @Value("${rasa.notify_request.utter.name}")
//     private String rasaUtterName;

//     @Value("${rasa.action.wait.utter.name.lr}")
//     private String rasaActionWaitingLr;

//     @Override
//     public void execute(DelegateExecution ctx) throws Exception {

//         //on récupère le trackerID du user
//         String usertrackerID = (String) ctx.getVariable(ProcessConstants.TRACKER_ID);
//         //récupération trackerid manager et mail manager depuis le context de camunda
//         String managerEmail = (String) ctx.getVariable(ProcessConstants.MANAGER_EMAIL);
//         String managerTrackerId = (String) ctx.getVariable(ProcessConstants.MANAGER_TRACKER_ID);

//         try {
//             //demande à rasa de lancer l'action "utter_lr_waiting_response" sur le tracker id user
//             rasaService.utter(rasaActionWaitingLr, usertrackerID);
            
//             //demande à rasa de lancer l'action "utter_lr_mng_notify_request" sur le trackerid du manager
//             rasaService.utter(rasaUtterName, managerTrackerId);
//         } catch (Exception e) {
//             LOG.error("error when notify manager : " + managerTrackerId, e);
//         }

//     }
// }
