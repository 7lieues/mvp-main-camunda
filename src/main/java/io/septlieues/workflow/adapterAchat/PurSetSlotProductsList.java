package io.septlieues.workflow.adapterAchat;

import com.google.gson.Gson;

import io.septlieues.workflow.model.Product;
import io.septlieues.workflow.services.RasaService;
import io.septlieues.workflow.utils.ProcessConstantsAchat;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

import static org.camunda.spin.Spin.JSON;

@Component
public class PurSetSlotProductsList implements JavaDelegate {

    @Autowired
    private Gson gson;

    private static final Logger LOG = LoggerFactory.getLogger(PurSetSlotProductsList.class);

    /* the odrer slot name*/
    @Value("${rasa.event.slot.catalog.name}")
    private String rasaCatalogSlot;


    @Autowired
    private RasaService rasaService;

    @Override
    public void execute(DelegateExecution ctx) throws Exception {

        String product_catalog= ctx.getVariable(ProcessConstantsAchat.PRODUCT_CATALOG).toString();
        LOG.info("********* Product Catalog : " + product_catalog);

        //get The trackerId from the process variable
        String trackerId = (String) ctx.getVariable(ProcessConstantsAchat.TRACKER_ID);

        //add json forma to header
        HttpHeaders requestHeader = new HttpHeaders();
        // next 4 lines are made to be used with carrousel
        requestHeader.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        List<Product> product = JSON(product_catalog).mapTo(ArrayList.class);
        LOG.info("the order is +" + product.toString());
        String orderAsJson = gson.toJson(product);

        // creation de la requete du setslot
        String adressMail = trackerId.split(":")[0];
        ctx.setVariable(ProcessConstantsAchat.USER_MAIL, adressMail);
        LOG.info("!!!!!!!!!!!!!!!!! adresse mail user: " + adressMail);

        // execution de la requete du set slot
         try {
            LOG.info("******************* lancement remplissage slot");
            rasaService.rasaSetSlot(rasaCatalogSlot, product_catalog, trackerId);

         } catch (Exception e) {
             LOG.error("error in RasaService ", e);
         }
    }

}
