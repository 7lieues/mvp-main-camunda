package io.septlieues.workflow.adapterAchat;


import io.septlieues.workflow.services.KeycloakUserService;
import io.septlieues.workflow.services.RasaService;
import io.septlieues.workflow.utils.ProcessConstantsAchat;
import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.camunda.bpm.extension.keycloak.json.JSONArray;
import org.camunda.bpm.extension.keycloak.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
public class ConGetMngInformations implements JavaDelegate {

    // Récupération et set coté rasa des slots pour notifier le Mng de la damande collaborateur

    private static final Logger LOG = LoggerFactory.getLogger(ConGetMngInformations.class);

    @Autowired
    private Environment env;

    @Autowired
    private RasaService rasaService;

    @Autowired
    KeycloakUserService keycloakUserService;

    @Autowired
    IdentityService identityService;

    @Override
    public void execute(DelegateExecution ctx) throws Exception {

        // On recup le tracker Id, le manager, la ref du produit (+ on construit l'email du user avec son trackerId)
        String trackerId = (String) ctx.getVariable(ProcessConstantsAchat.TRACKER_ID);
        String userMail = trackerId.split("::")[0];
        String trackerTemp = trackerId.split("::")[1];
        String requestID = trackerTemp.split("::")[0];

        try {
            //récupération mail manager depuis keycloak
            String manager = ((JSONArray) new JSONObject(keycloakUserService.getKeycloakUserAttributesByUserEmail(userMail)).get("manager")).getString(0);
            String managerEmail= keycloakUserService.searchUserByCustomAttribute("LDAP_ENTRY_DN",manager);
            LOG.info("********** Mail du manager : " + managerEmail);
            
            //inscription du mail manager et du request id dans le context de camunda
            ctx.setVariable(ProcessConstantsAchat.MANAGER_EMAIL, managerEmail);
            ctx.setVariable(ProcessConstantsAchat.REQUEST_ID, requestID);
        } catch (Exception e) {
            LOG.error("cannot Notify Rasa " + e);
        }

    }
}
