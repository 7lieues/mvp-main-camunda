package io.septlieues.workflow.adapterAchat;

import com.google.gson.Gson;

import io.septlieues.workflow.doa.RepoProducts;
import io.septlieues.workflow.model.Product;
import io.septlieues.workflow.services.RasaService;
import io.septlieues.workflow.utils.ProcessConstantsAchat;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class PurProductGetCatalog implements JavaDelegate {


    @Autowired
    private Gson gson;

    private static final Logger LOG = LoggerFactory.getLogger(PurProductGetCatalog.class);

    @Autowired
    private Environment env;

    @Autowired
    RepoProducts repoProducts;
    @Autowired
    private RasaService rasaService;

    @Override
    public void execute(DelegateExecution ctx) throws Exception {
   // try {
      //paramètre connexion à la base de donnée
     /* String url = env.getProperty("spring.datasource-business.url");
      String user = env.getProperty("spring.datasource-business.username");
      String passwd = env.getProperty("spring.datasource-business.password");
      LOG.info("****** url : " + url);
      LOG.info("****** password : " + passwd);
      LOG.info("****** user : " + user);
        */
      //récupération productType et gamme produit depuis le context de camunda
      String productType = (String) ctx.getVariable(ProcessConstantsAchat.PRODUCT_TYPE);
      String gammeProduitAutorisee = (String) ctx.getVariable(ProcessConstantsAchat.PRODUCT_SCALE);

      LOG.info("****** productType : " + productType);
      LOG.info("****** gammeProduitAutorisee : " + gammeProduitAutorisee);

        int index = gammeProduitAutorisee.indexOf(",") ;
        String products_list = "[";
        // si la personne a deux gammes
        if (index != 0 ){
        String split[] = gammeProduitAutorisee.split(",", 0);
            String gammePro =split[0];
            LOG.info("****** first gamme  : " + gammePro);
            String gammeProduit =split[1];
            LOG.info("****** second gamme : " + gammeProduit);
            //// rrchercher selon deux gammes
            List<Product> ListProducts = repoProducts.retrieveProductScale(gammeProduit,gammePro);
            List<Product> Products =ListProducts.stream().filter(e -> e.getType().equals(productType) && e.getQuantity() > 0).collect(Collectors.toList());;
            boolean available = true;
            ctx.setVariable(ProcessConstantsAchat.AVAILABLE, available);
            if (Products.isEmpty()) {
                available = false;
                String errorMsg = "";
                ctx.setVariable(ProcessConstantsAchat.AVAILABLE, available);
                errorMsg = "Il n'y a plus de produit en stock pour votre sélection. Merci de contacter votre service approvisionnement pour plus d'information.";
                ctx.setVariable(ProcessConstantsAchat.ERROR_MESSAGE_USER_CONTENT, errorMsg);
            }else{

                for(Product elem:Products) {
                    products_list = products_list + "{\"name\": \"" + elem.getName() +
                            "\", \"price\": \"" + elem.getPrice() +
                            "\", \"scale\": \"" + elem.getScale() +
                            "\", \"ref\": \"" + elem.getRef()+
                            "\", \"image_url\": \"" + elem.getImage_url()+
                            "\", \"description\": \"" + elem.getDescription()+
                            "\"},";
                }
                products_list = products_list.substring(0, products_list.length() - 1) + "]";
                LOG.info("****** Product Json Format : " + products_list);
                LOG.info("****** Product tostring : " + Products.toString());
            ctx.setVariable(ProcessConstantsAchat.PRODUCT_CATALOG, products_list);}
        }else {
            ///// pas encortee testé recherche a un seul gamme
        List<Product> Products = repoProducts.retrieveProductbyTelOneGam(ProcessConstantsAchat.SQL_TYPE,gammeProduitAutorisee);

            for(Product elem:Products) {
                products_list = products_list + "{\"name\": \"" + elem.getName() +
                        "\", \"price\": \"" + elem.getPrice() +
                        "\", \"scale\": \"" + elem.getScale() +
                        "\", \"ref\": \"" + elem.getRef()+
                        "\", \"image_url\": \"" + elem.getImage_url()+
                        "\", \"description\": \"" + elem.getDescription()+
                        "\"},";
            }
            products_list = products_list.substring(0, products_list.length() - 1) + "]";
        ctx.setVariable(ProcessConstantsAchat.PRODUCT_CATALOG, products_list); }

        /*

        //création liste contenant les gammes autorisé
      List<String> scale_list = Arrays.asList(gammeProduitAutorisee.split(","));
      int test = scale_list.size();

      //création de la fin de la querry (on remplace "," par "or")
      String end = "";
      for (int i = 0; i != test; i++)
        end = end + "'" + scale_list.get(i) + "'" + " or";

      //on supprime le "or" en trop dans notre String
      int tarte = end.lastIndexOf("or");
      String newend = end.substring(0, tarte);

      //dans cette nouvelle String on remplace "or" par "or scale ="
        String ynewend = newend.replace("or", "or p.scale = :");
      newend = newend.replace("or", "or scale =");
      LOG.info("string de fin: " + newend);

      //connexion bdd
      Connection conn = DriverManager.getConnection(url, user, passwd);
      Statement state = conn.createStatement();

      //querry dynamique qui prend le bon nombre de "or" en fonction de la gamme produit autorisé
      String sql = ("SELECT " + ProcessConstantsAchat.SQL_NAME + ", " + ProcessConstantsAchat.SQL_PRICE + ", " + ProcessConstantsAchat.SQL_SCALE + ", " + ProcessConstantsAchat.SQL_REF + ", " + ProcessConstantsAchat.SQL_IMAGE_URL + ", " + ProcessConstantsAchat.SQL_DESCRIPTION + " FROM " + ProcessConstantsAchat.SQL_PRODUCTS + " Where " + ProcessConstantsAchat.SQL_QUANTITY + " > 0 and " + ProcessConstantsAchat.SQL_TYPE + " = '" + productType + "'" + " and (scale =" + newend + ")");
      LOG.info("***requête sql   " + sql);
      ResultSet result = state.executeQuery(sql);
      ResultSetMetaData resultMeta = result.getMetaData();
      boolean available = true;
      String errorMsg = "";
      ctx.setVariable(ProcessConstantsAchat.AVAILABLE, available);

      //On affiche le nom des produits et on le stocke dans une string
      String products_list = "[";
      while (result.next()) {
        products_list = products_list + "{\"name\": \"" + result.getString(ProcessConstantsAchat.SQL_NAME) +
        "\", \"price\": \"" + result.getString(ProcessConstantsAchat.SQL_PRICE) +
        "\", \"scale\": \"" + result.getString(ProcessConstantsAchat.SQL_SCALE) +
        "\", \"ref\": \"" + result.getString(ProcessConstantsAchat.SQL_REF) +
        "\", \"image_url\": \"" + result.getString(ProcessConstantsAchat.SQL_IMAGE_URL) +
        "\", \"description\": \"" + result.getString(ProcessConstantsAchat.SQL_DESCRIPTION) +
        "\"},";
      }
      products_list = products_list.substring(0, products_list.length() - 1) + "]";
      LOG.info("liste des produits du catalogue" + products_list);
      if (products_list.length() == 1) {
        available = false;
        ctx.setVariable(ProcessConstantsAchat.AVAILABLE, available);
        errorMsg = "Il n'y a plus de produit en stock pour votre sélection. Merci de contacter votre service approvisionnement pour plus d'information.";
        ctx.setVariable(ProcessConstantsAchat.ERROR_MESSAGE_USER_CONTENT, errorMsg);
      }

      //on inscrit la liste des produits dans le context de camunda
      ctx.setVariable(ProcessConstantsAchat.PRODUCT_CATALOG, products_list);
      result.close();
      state.close();
    } catch (SQLException e) {
      LOG.error(" can not find the user", e);
    }
        */
  }
}
