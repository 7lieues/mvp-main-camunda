package io.septlieues.workflow.adapterAchat;


import io.septlieues.workflow.services.KeycloakUserService;
import io.septlieues.workflow.utils.ProcessConstantsAchat;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.camunda.bpm.extension.keycloak.json.JSONArray;
import org.camunda.bpm.extension.keycloak.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PurGetUserManager implements JavaDelegate {
    private static final Logger LOG = LoggerFactory.getLogger(PurGetUserManager.class);

   @Autowired
   KeycloakUserService keycloakUserService;

    @Override
    public void execute(DelegateExecution ctx) throws Exception {

        String trackerId = (String) ctx.getVariable(ProcessConstantsAchat.TRACKER_ID);
        LOG.info("****************** tracker id *********************************"+ trackerId);

        String addressMail = trackerId.split("::")[0];
        LOG.info("****************** ADRESS MAIL de demandeur *********************************"+ addressMail);
        
        String trackerTemp = trackerId.split("::")[1];
        String requestId = trackerTemp.split("::")[0];
        LOG.info ("****************** request id *********************************"+ requestId);

        ctx.setVariable(ProcessConstantsAchat.REQUEST_ID, requestId);
        try {

            String manager = ((JSONArray) new JSONObject(keycloakUserService.getKeycloakUserAttributesByUserEmail(addressMail)).get("manager")).getString(0);
            String managerEmail= keycloakUserService.searchUserByCustomAttribute("LDAP_ENTRY_DN",manager);
            LOG.info("!!!!!!!!!!!!!!! the manager of the user :  "+ managerEmail);
            ctx.setVariable(ProcessConstantsAchat.MANAGER, managerEmail);

        } catch (Exception e) {
            LOG.error(" can not find user ", e);
        }


    }
}
