package io.septlieues.workflow.adapterAchat;


import io.septlieues.workflow.services.BddService;
import io.septlieues.workflow.utils.ProcessConstantsAchat;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


// Classe qui récupère la famille, vérifie l'existence de contrats rattachés à cette famille, et inscrit le tout dans le contexte.


@Component
public class CatCheckContracts implements JavaDelegate {
  private static final Logger LOG = LoggerFactory.getLogger(CatCheckContracts.class);


  @Override
  public void execute(DelegateExecution ctx) throws Exception {
    // Récupération du tracker ID et de la référence du contrat
    String trackerId = (String) ctx.getVariable(ProcessConstantsAchat.TRACKER_ID);
    String categoryName = (String) ctx.getVariable(ProcessConstantsAchat.CATEGORY_NAME);

    try {
      // Récupération des contrats sur la famille d'achat citée
      // Exemple de requête lorsque la BDD sera carrée
      // select day_name from tds_roc_week_slots where id in (select tds_roc_week_slot_id from tds_roc_week_slots_slots where tds_roc_slot_id in (select id from tds_roc_slots where name = '20-21'));



    } catch (Exception e) {
      LOG.error(" Erreur with balance value block ", e);
    }
  }
}
