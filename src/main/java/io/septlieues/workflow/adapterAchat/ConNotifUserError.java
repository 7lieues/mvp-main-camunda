package io.septlieues.workflow.adapterAchat;



import io.septlieues.workflow.services.RasaService;
import io.septlieues.workflow.utils.ProcessConstantsAchat;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


@Component
public class ConNotifUserError implements JavaDelegate {
        
    private static final Logger LOG = LoggerFactory.getLogger(ConNotifUserError.class);

    @Autowired
    private RasaService rasaService;

    @Value("${rasa.notify.user.failure_utter}")
    private String rasaNotifyUserFailureUtter;

    @Value("${rasa.notify.user.failure_slot}")
    private String rasaNotifyUserFailureSlot;

    @Override
    public void execute(DelegateExecution ctx) throws Exception {

        LOG.info("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        LOG.info("!!!! ERREUR FONCTIONNELLE A TRAITER !!!!!");
        String errorMessageUserContent = (String) ctx.getVariable(ProcessConstantsAchat.ERROR_MESSAGE_USER_CONTENT);
        LOG.info("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! Contenu de l'erreur : " + errorMessageUserContent);
        
        String userTrackerID = (String) ctx.getVariable(ProcessConstantsAchat.TRACKER_ID);
        LOG.info("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! tracker ID concerné : " + userTrackerID);
        try {
            rasaService.rasaSetSlot(rasaNotifyUserFailureSlot, errorMessageUserContent, userTrackerID);
            rasaService.utter(rasaNotifyUserFailureUtter, userTrackerID);
            
        } catch (Exception e) {
            LOG.error("!!!!!!!!! [Class ConNotifUserError] Error when pushing ERROR to RASA : " + userTrackerID, e);
        }

    }
}

