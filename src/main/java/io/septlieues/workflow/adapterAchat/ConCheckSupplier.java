package io.septlieues.workflow.adapterAchat;


import io.septlieues.workflow.doa.RepoContracts;
import io.septlieues.workflow.doa.RepoSuppliers;
import io.septlieues.workflow.model.Contract;

import io.septlieues.workflow.model.Supplier;
import io.septlieues.workflow.services.BddService;
import io.septlieues.workflow.services.RasaService;
import io.septlieues.workflow.utils.ProcessConstantsAchat;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.math.BigInteger;
import java.util.Optional;


// Classe qui récupère le supplier lié au contrat, vérifie son existence en base, et récupère son status.
// Le tout est inscrit dans le contexte camunda.


@Component
public class ConCheckSupplier implements JavaDelegate {
  private static final Logger LOG = LoggerFactory.getLogger(ConCheckSupplier.class);

  @Autowired
  RepoContracts repoContracts;

  @Autowired
  RepoSuppliers repoSuppliers;

  @Autowired
  private BddService bddService;

  @Autowired
  private RasaService rasaService;

  @Value("${rasa.con.supplier.name}")
  private String supplierName;

  @Override
  public void execute(DelegateExecution ctx) throws Exception {
    LOG.error(" je suis dans la classe ConCheckSupplier");
    // Récupération du tracker ID et de la référence du contrat
    String trackerId = (String) ctx.getVariable(ProcessConstantsAchat.TRACKER_ID);
    String contractRef = (String) ctx.getVariable(ProcessConstantsAchat.CONTRACT_REF);
    BigInteger contractsId = (BigInteger) ctx.getVariable("contracts_id"); ///// get le id du contracts
    LOG.info("***contractsId ****"+contractsId);

    ////////////////////// new
    Optional<Contract> contract = repoContracts.findById(contractsId);
    if (contract.isPresent()){
     Optional<Supplier> suppliers = repoSuppliers.findById(contract.get().getSupplierId()); ////recherche le supp par id du table contracts
         if (suppliers.isPresent()){
           ctx.setVariable(ProcessConstantsAchat.SUPPLIER_STATUS, suppliers.get().getStatus());
           LOG.info("***suppliers get status() ****"+suppliers.get().getStatus());
           ctx.setVariable(ProcessConstantsAchat.SUPPLIER_NAME, suppliers.get().getName());
           LOG.info("***suppliers. Name  ****"+suppliers.get().getName());
           String errorMsg = "Le fournisseur " + suppliers.get().getName() + " du contrat demandé n'est pas/plus valide. Veuillez vous rapprocher de votre administration.";
           ctx.setVariable(ProcessConstantsAchat.ERROR_MESSAGE_USER_CONTENT, errorMsg);
           // Set slot nom du fournisseur
           rasaService.rasaSetSlot(supplierName, suppliers.get().getName(), trackerId);
         }
    }





  /*

    ///////////////////old
    try {
      // Vérification de la présence du supplier en base et inscription dans le contexte camunda
      List<String> request = bddService.sqlRequestOneConditionList(ProcessConstantsAchat.SQL_TABLE_CONTRACT_COLUMN_SUPPLIER_ID, ProcessConstantsAchat.TABLE_CONTRACTS, ProcessConstantsAchat.SQL_CONTRACT_NAME, contractRef);
      String supplier_id = request.get(0);
      request.stream()
              .forEach(e -> System.out.println("yasss   request  ssser"+e+"yasssssssser"));
      System.out.println("supplier_id :"+supplier_id+"yasssssss"); ////si il exist return 1

      // Récupération du status du supplier et inscription dans le contexte camunda
      String supplier_status = bddService.sqlRequestOneCondition(ProcessConstantsAchat.SQL_SUPPLIER_STATUS, ProcessConstantsAchat.TABLE_SUPPLIERS, ProcessConstantsAchat.SQL_SUPPLIER_ID , supplier_id);
        System.out.println("supplier_status"+supplier_status); /////si il exist return Actif
      ctx.setVariable(ProcessConstantsAchat.SUPPLIER_STATUS, supplier_status);
      
      LOG.error(" Supplier ID : " + supplier_id);
      LOG.error(" Supplier status : " + supplier_status);

      // Récupération du nom du supplier et inscription dans le contexte camunda
      String supplier_name = bddService.sqlRequestOneCondition(ProcessConstantsAchat.SQL_SUPPLIER_NAME, ProcessConstantsAchat.TABLE_SUPPLIERS, ProcessConstantsAchat.SQL_SUPPLIER_ID, supplier_id);
      ctx.setVariable(ProcessConstantsAchat.SUPPLIER_NAME, supplier_name);
      LOG.info("NOM DU FOURNISSEUR : " + supplier_name);

      // Message d'erreur contrat
      String errorMsg = "Le fournisseur " + supplier_name + " du contrat demandé n'est pas/plus valide. Veuillez vous rapprocher de votre administration.";
      ctx.setVariable(ProcessConstantsAchat.ERROR_MESSAGE_USER_CONTENT, errorMsg);

      // Set slot nom du fournisseur
      rasaService.rasaSetSlot(supplierName, supplier_name, trackerId);

    } catch (Exception e) {
      LOG.error(" Erreur with balance value block ", e);
    } */
  }
}
