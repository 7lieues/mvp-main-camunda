package io.septlieues.workflow.adapterAchat;


import io.septlieues.workflow.doa.RepoContracts;
import io.septlieues.workflow.doa.RepoFinancialEngagements;

import io.septlieues.workflow.model.Contract;
import io.septlieues.workflow.model.FinancialEngagement;
import io.septlieues.workflow.services.BddService;

import io.septlieues.workflow.services.RasaService;
import io.septlieues.workflow.utils.ProcessConstantsAchat;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;


@Component
public class ConCheckEngagementAmount implements JavaDelegate {

    // Récupération du montant de la commande ouverte et vérification avec le montant max du contrat

    private static final Logger LOG = LoggerFactory.getLogger(ConCheckEngagementAmount.class);

    @Autowired
    private Environment env;

    @Autowired
    private BddService bddService;

    @Autowired
    private RasaService rasaService;

    @Autowired
    RepoContracts repoContracts;

    @Autowired
    RepoFinancialEngagements repoFinancialEngagements;

    @Value("${rasa.con.slot.engagement.amount}")
    private String amountSlot;

    @Override
    public void execute(DelegateExecution ctx) throws Exception {

        String trackerId = (String) ctx.getVariable(ProcessConstantsAchat.TRACKER_ID);

        // Récupération du montant demandé et du montant max sur le contrat
        String amountAskedString = (String) ctx.getVariable(ProcessConstantsAchat.AMOUNT_ASKED);
        amountAskedString = amountAskedString.replaceAll("\\s+","");
        LOG.info(" Montant demandé (string) : " + amountAskedString);
        int amountAsked = Integer.parseInt(amountAskedString);
        LOG.info(" Montant demandé (int) : " + amountAsked);

        String contractRef = (String) ctx.getVariable(ProcessConstantsAchat.CONTRACT_REF);
        LOG.info(" Réf du contrat : " + contractRef);

        // c'est pour enlever la requete
        BigInteger contractID =(BigInteger)  ctx.getVariable("contracts_id");
        LOG.info(" ID idContract  ********* : " +contractID.toString());
        Optional<Contract> contracts =repoContracts.findById(contractID);
        if(contracts.isPresent()){
            BigInteger maxAmountBigInteger =  contracts.get().getMaxAmount();
            LOG.info(" Montant max (BigInteger) yasser : " + maxAmountBigInteger);
            List<FinancialEngagement> listFinancial =   repoFinancialEngagements.findAll();
            int engagementsAmount =listFinancial.stream().filter(e->e.getContractId() == contracts.get().getId())
                                                        .mapToInt(e -> e.getAmountEngaged().intValue())
                                                        .sum();
            LOG.info(" Montant demandé (engagementsAmountY) yasser : " + engagementsAmount);

      //  String contractID = bddService.sqlRequestOneCondition(ProcessConstantsAchat.SQL_CONTRACT_ID, ProcessConstantsAchat.TABLE_CONTRACTS, ProcessConstantsAchat.SQL_CONTRACT_NAME, contractRef);
      //  LOG.info(" ID contrat compare ****: " + contractID);

      //  String maxAmountString = bddService.sqlRequestOneCondition(ProcessConstantsAchat.SQL_CONTRACT_MAX_AMOUNT, ProcessConstantsAchat.TABLE_CONTRACTS, ProcessConstantsAchat.SQL_CONTRACT_NAME, contractRef);
       // LOG.info(" Montant max (string) : " + maxAmountString);
      //  int maxAmount = Integer.parseInt(maxAmountString);
       // LOG.info(" Montant max (int) : " + maxAmountString);

        // Récupération des montants des commandes ouvertes sur ce contrat
       /* String url = env.getProperty("spring.datasource-business.url");
        String username = env.getProperty("spring.datasource-business.username");
        String password = env.getProperty("spring.datasource-business.password");
        Connection conn = DriverManager.getConnection(url, username, password);
        Statement stmt = conn.createStatement();

        String sql = ("SELECT COALESCE(SUM(" + ProcessConstantsAchat.SQL_ENGAGEMENT_AMOUNTS_ENGAGED + "),0) FROM " + ProcessConstantsAchat.TABLE_ENGAGEMENTS + " WHERE " + ProcessConstantsAchat.SQL_ENGAGEMENT_CONTRACT_ID + " = " + "'" + contractID.toString() + "'");
        LOG.info(" Requête SQL somme des commandes ouvertes : " + sql);

        ResultSet rs = stmt.executeQuery(sql);
        LOG.info(" RS : " + rs);
        
        String engagementsAmountString = "";
        while (rs.next()) {
            engagementsAmountString = rs.getString("coalesce");
        }
        int engagementsAmount = Integer.parseInt(engagementsAmountString);
        
        LOG.info(" Montant max : " + maxAmountBigInteger.intValue());
        LOG.info(" Commandes ouvertes faites : " + engagementsAmount);
            */
        int amountRemaining = maxAmountBigInteger.intValue() - engagementsAmount;

        LOG.info(" Restant : " + amountRemaining);

        try {
            // Check du montant demandé vs montant restant
            if (amountAsked > amountRemaining) {
                ctx.setVariable(ProcessConstantsAchat.CHECK_AMOUNT, "KO");
                LOG.info(" Flag check amount : " + ctx.getVariable(ProcessConstantsAchat.CHECK_AMOUNT));
                String errorMsg = "Le montant maximum autorisé sur ce contrat est atteint, nous ne pouvons pas donner suite à votre demande. Merci de vous rapprocher du service achat.";
                ctx.setVariable(ProcessConstantsAchat.ERROR_MESSAGE_USER_CONTENT, errorMsg);
            } else {
                ctx.setVariable(ProcessConstantsAchat.CHECK_AMOUNT, "OK");
                LOG.info(" Flag check amount : " + ctx.getVariable(ProcessConstantsAchat.CHECK_AMOUNT));
                rasaService.rasaSetSlot(amountSlot, amountAskedString, trackerId);
                String errorMsg = "Votre profil utilisateur ne vous autorise pas à passer des commandes avec le montant demandé. Merci de vous rapprocher du service achat.";
                ctx.setVariable(ProcessConstantsAchat.ERROR_MESSAGE_USER_CONTENT, errorMsg);
            };
        } catch (Exception e) {
            LOG.error("cannot Notify Rasa " + e);
        }

    }}
}
