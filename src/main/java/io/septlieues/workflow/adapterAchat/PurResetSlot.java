package io.septlieues.workflow.adapterAchat;

import com.google.gson.Gson;

import io.septlieues.workflow.services.RasaService;
import io.septlieues.workflow.utils.ProcessConstantsAchat;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class PurResetSlot implements JavaDelegate {

    @Autowired
    private Gson gson;

    private static final Logger LOG = LoggerFactory.getLogger(PurResetSlot.class);

    /* the odrer slot name*/
    @Value("${rasa.event.slot.product.chosen}")
    private String rasaProductChosen;


    @Autowired
    private RasaService rasaService;

    @Override
    public void execute(DelegateExecution ctx) throws Exception {

        String product_chosen = null; /// ?
        LOG.info("******************* Reset Product chosen");

        // On recup le trackerId du process constant
        String trackerId = (String) ctx.getVariable(ProcessConstantsAchat.TRACKER_ID);

        // On execute le reset slot
         try {
            rasaService.rasaSetSlot(rasaProductChosen, product_chosen, trackerId);
         } catch (Exception e) {
             LOG.error("error in RasaService ", e);
         }
    }

}
