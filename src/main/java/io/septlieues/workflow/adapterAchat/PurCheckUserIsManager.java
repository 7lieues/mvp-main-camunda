package io.septlieues.workflow.adapterAchat;



import io.septlieues.workflow.utils.ProcessConstantsAchat;
import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PurCheckUserIsManager implements JavaDelegate {

    @Autowired
    IdentityService identityService;

        @Override
        public void execute(DelegateExecution ctx) throws Exception {
            String trackerId = (String) ctx.getVariable(ProcessConstantsAchat.TRACKER_ID);
            String adressMail = trackerId.split(":")[0];
            boolean isInManagerGroup = identityService.createGroupQuery().groupMember(adressMail).list().stream()
                    .filter(group -> "managers".equals(group.getName())).findFirst().orElse(null) == null ? false : true;

            if (isInManagerGroup) {
            ctx.setVariable(ProcessConstantsAchat.IS_APPLICANT_MANAGER, true);
            ctx.setVariable(ProcessConstantsAchat.APPLICANT_ROLE, "manager");

        } else {
            ctx.setVariable(ProcessConstantsAchat.IS_APPLICANT_MANAGER, false);
            ctx.setVariable(ProcessConstantsAchat.APPLICANT_ROLE, "employee");
        }
    }
}
