package io.septlieues.workflow.adapterAchat;


import io.septlieues.workflow.services.RasaService;
import io.septlieues.workflow.utils.ProcessConstantsAchat;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

@Component
public class ConNotifEngagementAmount implements JavaDelegate {

    // Affichage côté user pour récupérer le montant de la commande ouverte

    private static final Logger LOG = LoggerFactory.getLogger(ConNotifEngagementAmount.class);

    @Autowired
    private Environment env;
    @Value("${rasa.notify.engagement.amount}")
    private String rasaFormName;
    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    private RasaService rasaService;

    @Override
    public void execute(DelegateExecution ctx) throws Exception {

        // Récupération du tracker Id
        String trackerId = (String) ctx.getVariable(ProcessConstantsAchat.TRACKER_ID);

        // Envoi de la requête pour lancer le message
        try {
            rasaService.utter(rasaFormName, trackerId);
        } catch (Exception e) {
            LOG.error("cannot Notify Rasa " + e);
        }

    }
}
