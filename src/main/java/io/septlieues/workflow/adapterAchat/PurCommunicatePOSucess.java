package io.septlieues.workflow.adapterAchat;


import io.septlieues.workflow.services.RasaService;
import io.septlieues.workflow.utils.ProcessConstantsAchat;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class PurCommunicatePOSucess implements JavaDelegate {

    private static final Logger LOG = LoggerFactory.getLogger(PurCommunicatePOSucess.class);

    @Autowired
    private RasaService rasaService;


    /*the config of utter to send to notify Rasa parsed from yml file*/
    @Value("${rasa.sucess.utter.name}")
    private String rasaSucessUtterName;


    @Override
    public void execute(DelegateExecution ctx) throws Exception {
        String trackerId = (String) ctx.getVariable(ProcessConstantsAchat.TRACKER_ID);
        
        // On execute le utter lié au rasaSucessUtterName plus haut
        try {
            rasaService.utter(rasaSucessUtterName, trackerId);
        } catch (Exception e) {
            LOG.error("error in communicate_PurchaseOrder ", e);
        }


    }


}
