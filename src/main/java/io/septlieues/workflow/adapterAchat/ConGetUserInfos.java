package io.septlieues.workflow.adapterAchat;


import io.septlieues.workflow.services.RasaService;
import io.septlieues.workflow.utils.ProcessConstantsAchat;
import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
public class ConGetUserInfos implements JavaDelegate {

    // Récupération et calcul des infos users (tracker ID, mail, manager, manager tracker ID, etc.)

    private static final Logger LOG = LoggerFactory.getLogger(ConGetUserInfos.class);

    @Autowired
    private Environment env;

    @Autowired
    private RasaService rasaService;

    @Autowired
    IdentityService identityService;

    @Override
    public void execute(DelegateExecution ctx) throws Exception {
        LOG.info("je suis dnas la classe ConGetUserInfos  ");
        // On recup le tracker Id, le manager, la ref du produit (+ on construit l'email du user avec son trackerId)
        String trackerId = (String) ctx.getVariable(ProcessConstantsAchat.TRACKER_ID);
        String userMail = (String) trackerId.split("::")[0];
        // List<String> groupIds = new ArrayList<>();
        // identityService.createGroupQuery().groupMember("1").list().forEach( g -> groupIds.add(g.getId()));
        // LOG.info("LISTE DES GROUPS POUR OLIVIER : " + groupIds);
        String role = "acheteur";
        ctx.setVariable(ProcessConstantsAchat.USER_ROLE, role);

        try {
            // Inscription en base des infos
            ctx.setVariable(ProcessConstantsAchat.USER_MAIL, userMail);
        } catch (Exception e) {
            LOG.error("cannot Notify Rasa " + e);
        }

    }
}
