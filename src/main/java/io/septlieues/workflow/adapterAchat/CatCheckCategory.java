package io.septlieues.workflow.adapterAchat;


import io.septlieues.workflow.services.BddService;
import io.septlieues.workflow.utils.ProcessConstantsAchat;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


// Classe qui récupère la famille, vérifie son existence en base.
// Le tout est inscrit dans le contexte camunda.


@Component
public class CatCheckCategory implements JavaDelegate {
  private static final Logger LOG = LoggerFactory.getLogger(CatCheckCategory.class);



  @Override
  public void execute(DelegateExecution ctx) throws Exception {
    // Récupération du tracker ID et de la référence du contrat
    String trackerId = (String) ctx.getVariable(ProcessConstantsAchat.TRACKER_ID);
    String categoryName = (String) ctx.getVariable(ProcessConstantsAchat.CATEGORY_NAME);
    String check = "";

    try {
      // Vérification de la présence du contrat en base et inscription dans le contexte camunda
      // List<String> check_request = bddService.sqlRequestOneConditionList(ProcessConstants.SUPPLIER_ID, ProcessConstants.TABLE_CONTRACTS, ProcessConstants.CONTRACT_NAME, contractRef);
      // String check = check_request.get(0);


    if (categoryName.equals("imprimante") || categoryName.equals("mobilier")) {
      check = "OK";
    } else {
      check = "KO";
    }

      ctx.setVariable(ProcessConstantsAchat.CHECK_CATEGORY, check);

      LOG.error("Nom famille achat : '" + categoryName + "' ");
      LOG.error("Check famille achat : " + check);

    } catch (Exception e) {
      LOG.error(" Erreur with balance value block ", e);
    }
  }
}
