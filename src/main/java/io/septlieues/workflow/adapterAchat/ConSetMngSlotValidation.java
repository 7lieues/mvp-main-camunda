package io.septlieues.workflow.adapterAchat;// package io.septlieues.camunda.ha.adapter;

// import io.septlieues.camunda.ha.services.RasaService;
// import io.septlieues.camunda.ha.utils.ProcessConstants;

// import java.security.acl.Group;
// import java.util.List;

// import org.camunda.bpm.engine.delegate.DelegateExecution;
// import org.camunda.bpm.engine.delegate.JavaDelegate;
// import org.slf4j.Logger;
// import org.slf4j.LoggerFactory;
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.beans.factory.annotation.Value;
// import org.springframework.core.env.Environment;
// import org.springframework.mail.SimpleMailMessage;
// import org.springframework.mail.javamail.JavaMailSender;
// import org.springframework.stereotype.Component;
// import org.camunda.bpm.engine.IdentityService;
// import java.util.ArrayList;
// import org.camunda.bpm.engine.identity.User;
// import org.springframework.http.HttpEntity;
// import org.springframework.http.HttpHeaders;
// import org.springframework.http.MediaType;
// import io.septlieues.camunda.ha.model.RasaEvent;

// @Component
// public class ConSetMngSlotValidation implements JavaDelegate {

//     // Récupération et set coté rasa des slots pour notifier le Mng de la damande collaborateur

//     private static final Logger LOG = LoggerFactory.getLogger(ConSetMngSlotValidation.class);

//     @Autowired
//     private Environment env;

//     @Autowired
//     private RasaService rasaService;

//     @Autowired
//     IdentityService identityService;
    
//     @Value("${rasa.slot.contract.name}")
//     private String rasaSlotContractName;
    
//     @Value("${rasa.slot.amount.contract}")
//     private String rasaSlotAmountContract;

//     @Value("${rasa.slot.contract.name.user}")
//     private String rasaSlotContractNameUser;

//     @Override
//     public void execute(DelegateExecution ctx) throws Exception {

//         // On récup le trackerId du context cam, on fabrique le mail du user, on récup le mail du mng, le requestId
//         String userTrackerID = (String) ctx.getVariable(ProcessConstants.TRACKER_ID);
//         String userEmail = userTrackerID.split("::")[0];
//         String managerEmail = (String) ctx.getVariable(ProcessConstants.MANAGER_EMAIL);
//         String requestID = (String) ctx.getVariable(ProcessConstants.REQUEST_ID);
//         String BU = (String) ProcessConstants.BU;
//         String managerTrackerId = managerEmail.concat("::").concat(requestID).concat("::").concat("1").concat("::").concat(BU);
//         ctx.setVariable(ProcessConstants.MANAGER_TRACKER_ID, managerTrackerId);
        
//         LOG.info("********** Mail user : " + userEmail);
//         LOG.info("********** Mail manager : " + managerEmail);
//         LOG.info("********** Tracker ID manager : " + managerTrackerId);

//         // On récupère du contexte le montant et et le nom duu contrat
//         String montant = (String) ctx.getVariable(ProcessConstants.AMOUNT_ASKED);
//         String nom_Contrat = (String) ctx.getVariable(ProcessConstants.CONTRACT_NAME);
//         LOG.info("*********** nom du contrat : " + nom_Contrat);
//         LOG.info("*********** montant : " + montant);

//         // Récupération du nom de l'utilisateur
//         User user = identityService.createUserQuery().userId(userEmail).singleResult();
//         String userFullName=userEmail;
//         if(user!=null) {
//             userFullName = user.getFirstName() + " " + user.getLastName();
//             LOG.info("********** Nom du demandeur : " + userFullName);
//         }
//         else {
//             userFullName = userEmail;
//         }
//         LOG.info("Demande de : " + userFullName);

//         try {
//             HttpHeaders requestHeader = new HttpHeaders();
//             requestHeader.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);

//             // Set du slot nom de contrat chez le Mng
//             RasaEvent rasaEvent = new RasaEvent("slot", rasaSlotContractName, nom_Contrat);
//             HttpEntity<RasaEvent> requestEntitySetSlot = new HttpEntity<>(rasaEvent, requestHeader);

//             rasaService.setSlot(managerTrackerId, requestEntitySetSlot);

//             // Set du slot montant demandé par l'utilisateur chez le Mng
//             RasaEvent rasaEvent2 = new RasaEvent("slot", rasaSlotAmountContract, montant);
//             HttpEntity<RasaEvent> requestEntitySetSlot2 = new HttpEntity<>(rasaEvent2, requestHeader);

//             rasaService.setSlot(managerTrackerId, requestEntitySetSlot2);

//             // Set du slot nom de l'utilisateur chez le Mng
//             RasaEvent rasaEvent3 = new RasaEvent("slot", rasaSlotContractNameUser, montant);
//             HttpEntity<RasaEvent> requestEntitySetSlot3 = new HttpEntity<>(rasaEvent3, requestHeader);

//             rasaService.setSlot(managerTrackerId, requestEntitySetSlot3);
            
//         } catch (Exception e) {
//             LOG.error("cannot Notify Rasa " + e);
//         }

//     }
// }
