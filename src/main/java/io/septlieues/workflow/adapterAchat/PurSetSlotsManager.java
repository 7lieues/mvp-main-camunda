package io.septlieues.workflow.adapterAchat;

import com.google.gson.Gson;

import io.septlieues.workflow.services.RasaService;
import io.septlieues.workflow.utils.ProcessConstantsAchat;
import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.camunda.bpm.engine.identity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class PurSetSlotsManager implements JavaDelegate {
    private static final Logger LOG = LoggerFactory.getLogger(PurSetSlotsManager.class);

    @Autowired
    private RasaService rasaService;

    @Autowired
    IdentityService identityService;

    @Autowired
    private Gson gson;


    /*the config of utter to send to notify Rasa parsed from yml file*/
    @Value("${rasa.notify_pur_request.utter.name}")
    private String rasaUtterName;
    @Value("${rasa.notify_request.action.name}")
    private String rasaActionName;
    /* the odrer slot name*/
    @Value("${rasa.event.slot.commande.name}")
    private String rasaProductSlot;


    @Override
    public void execute(DelegateExecution ctx) throws Exception {
        String processInstanceId = ctx.getProcessInstanceId();
        String productRef = (String) ctx.getVariable(ProcessConstantsAchat.PRODUCT_REF);
        String productType = (String) ctx.getVariable(ProcessConstantsAchat.PRODUCT_TYPE);
        String tracker_id = (String) ctx.getVariable(ProcessConstantsAchat.TRACKER_ID);

        LOG.info("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");

        String managerMail = (String) ctx.getVariable(ProcessConstantsAchat.MANAGER);
        LOG.info("manager mail : " + managerMail);

        String requestID = (String) ctx.getVariable(ProcessConstantsAchat.REQUEST_ID);
        LOG.info("requestID : " + requestID);

        String BU = "achat";
        LOG.info("BU : " + BU);

        String managerTrackerId = managerMail.concat("::").concat(requestID).concat("::").concat("1").concat("::").concat(BU);
        LOG.info("managerTrackerID : " + managerTrackerId);

        ctx.setVariable(ProcessConstantsAchat.MANAGER_TRACKER_ID, managerTrackerId);
        LOG.info("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        LOG.info("manager tracker id:" + managerTrackerId);
        LOG.info("process instance id:" + processInstanceId);
        LOG.info("product ref:" + productRef);
        LOG.info("product type:" + productType);
        LOG.info("tracker id:" + tracker_id);
        LOG.info("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");



        //JSONObject catalogue = (JSONObject) ctx.getVariable(ProcessConstants.Product_CATALOG);
        String price = null;
        String userMail = tracker_id.split(":")[0];
        User user = identityService.createUserQuery().userId(userMail).singleResult();
        String userFullName = null;
        String name = (user.getFirstName() + " " + user.getLastName());
        if (user != null) {
            userFullName = user.getFirstName() + " " + user.getLastName();
        } else {
            userFullName = tracker_id;}

            try {
                HttpHeaders requestHeader = new HttpHeaders();
                requestHeader.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);


                /*try {
                    if (catalogue.get("ref").equals(productRef)){
                        Object product = catalogue.get("ref");
                        Product productChosen = JSON(product).mapTo(Product.class);
                        price = productChosen.getPrice();
                         }
                } catch (Exception e){
                    LOG.error("cannot extact price "+ e);
                }*/
                
                Map<String, String> inputMap = new HashMap<>();
                inputMap.put("processInstanceId", processInstanceId);
                inputMap.put("ref", productRef);
                inputMap.put("productType", productType);
                inputMap.put("applicant", userFullName);
                inputMap.put("price", price);
                inputMap.put("userMail", userMail);
                inputMap.put("name", name);
                // convert map to JSON String & set slot
                String jsonProduct = gson.toJson(inputMap);
                rasaService.rasaSetSlot(rasaProductSlot, jsonProduct, managerTrackerId); // utter test  && action listen
                rasaService.rasaSetSlot(rasaProductSlot, jsonProduct, managerTrackerId); /// tracker manager a voir

                
            } catch (Exception e) {
                LOG.error("error when set slot rasa for product  : " + productRef + "and user " + userFullName, e);

            }
        try {
            LOG.info("##################### trackerId de manager ######################### "+managerTrackerId );
            rasaService.utter(rasaActionName, managerTrackerId);
        } catch (Exception e) {
            LOG.error("error when set slot entities : " + managerTrackerId, e);
        }

        try {
            rasaService.utter(rasaUtterName, managerTrackerId);
            LOG.info("!!!!!!!!!!!!!!!! validation lancé !!!!!!!!!!!!!!!");
        } catch (Exception e) {
            LOG.error("error when notify manager : " + managerTrackerId, e);
        }

    }
}
