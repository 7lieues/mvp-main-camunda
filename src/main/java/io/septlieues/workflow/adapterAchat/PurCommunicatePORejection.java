package io.septlieues.workflow.adapterAchat;


import io.septlieues.workflow.services.RasaService;
import io.septlieues.workflow.utils.ProcessConstantsAchat;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

@Component
public class PurCommunicatePORejection implements JavaDelegate {
    private static final Logger LOG = LoggerFactory.getLogger(PurCommunicatePORejection.class);
    @Autowired
    private Environment env;

    @Autowired
    private JavaMailSender javaMailSender;
    @Autowired
    private RasaService rasaService;


    /*the config of utter to send to notify Rasa parsed from yml file*/
    @Value("${rasa.rejection.utter.name}")
    private String rasaRejectionUtterName;

    @Override
    public void execute(DelegateExecution ctx) throws Exception {
        String rejectionComment = null;
        String trackerId = (String) ctx.getVariable(ProcessConstantsAchat.TRACKER_ID);
        String adressMail = trackerId.split(":")[0];
        String manager = (String) ctx.getVariable(ProcessConstantsAchat.MANAGER);
        Boolean managerComment = (Boolean) ctx.getVariable(ProcessConstantsAchat.MANAGER_COMMENT);
        try {
            SimpleMailMessage mail = new SimpleMailMessage();
            LOG.info("Start sending mail for PO rejection ..", " to :" + adressMail);
            mail.setTo(adressMail);
            mail.setFrom(env.getProperty("spring.mail.username"));
            mail.setSubject("Demande Achat");
            mail.setText("Votre demande a été refusée par votre manager " + manager);

            // javaMailSender.send(mail);
            try {
                rasaService.utter(rasaRejectionUtterName, trackerId);
            } catch (Exception e) {
                LOG.error("error in notify user rejection ", e);
            }
            LOG.info("mail send with success");
        } catch (Exception e) {
            LOG.warn("Could not send email to assignee", e);

        }

    }
}
