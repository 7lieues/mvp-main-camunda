package io.septlieues.workflow.adapterAchat;


import io.septlieues.workflow.doa.RepoContracts;
import io.septlieues.workflow.model.Contract;

import io.septlieues.workflow.services.BddService;
import io.septlieues.workflow.services.RasaService;
import io.septlieues.workflow.utils.ProcessConstantsAchat;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.Optional;


// Classe qui récupère la réf contrat, vérifie son existence en base, et récupère ses dates de validité.
// Le tout est inscrit dans le contexte camunda.


@Component
public class ConCheckContract implements JavaDelegate {
  private static final Logger LOG = LoggerFactory.getLogger(ConCheckContract.class);

  @Autowired
  RepoContracts repoContracts;


  @Autowired
  private RasaService rasaService;

  @Override
  public void execute(DelegateExecution ctx) throws Exception {
    LOG.info("je suis dans la classe  ConCheckContract !!");
    // Récupération du tracker ID et de la référence du contrat
    String trackerId = (String) ctx.getVariable(ProcessConstantsAchat.TRACKER_ID);
    String contractRef = (String) ctx.getVariable(ProcessConstantsAchat.CONTRACT_REF);

    LOG.info("je suis dans CONTRACT_REF  " +contractRef);
    String errorMsg = "Le contrat " + contractRef + " n'existe pas, veuillez vérifier le nom du contrat et vous rapprocher de votre administration.";

    // JPA find all qui herite de JpaRepository
    List<Contract> listContrats  =  repoContracts.findAll();
    Contract Contart  =  repoContracts.retrieveContractsByTechName(contractRef); //2eme methode avec JPQL
    LOG.info("je chercher dans Contart methode JPQL " +Contart);
    // stream + filtre
    Optional<Contract> x = listContrats.stream()
            .filter(e -> e.getTechnicalName().equals(contractRef))
            .findFirst();
    if(x.isPresent()){
      ctx.setVariable("contracts_id", x.get().getId()); ///// injecter le id avec le type BigInterger
      ctx.setVariable(ProcessConstantsAchat.CHECK_CONTRACT, Boolean.TRUE);
      System.out.println("*************"+x.get().getValidityStart()+"***************");
      ctx.setVariable(ProcessConstantsAchat.CONTRACT_VALIDITY_START, x.get().getValidityStart());
      ctx.setVariable(ProcessConstantsAchat.CONTRACT_VALIDITY_END, x.get().getValidityEnd());
      System.out.println("*************"+x.get().getValidityEnd()+"***************");
    }else{
      ctx.setVariable(ProcessConstantsAchat.ERROR_MESSAGE_USER_CONTENT,errorMsg);
      String check="impossible";
      ctx.setVariable(ProcessConstantsAchat.CHECK_CONTRACT,check);
    }



   /* old
    try {
      // Vérification de la présence du contrat en base et inscription dans le contexte camunda
      List<String> check_request = bddService.sqlRequestOneConditionList(ProcessConstantsAchat.SQL_TABLE_CONTRACT_COLUMN_SUPPLIER_ID, ProcessConstantsAchat.TABLE_CONTRACTS, ProcessConstantsAchat.SQL_CONTRACT_NAME, contractRef);
      String check = check_request.get(0);////il return 1 ou impossible
      LOG.info("********votre check est " );
      ctx.setVariable(ProcessConstantsAchat.CHECK_CONTRACT, check);

  //// affichage list Contracts


      // Message d'erreur contrat

      if (check.equals("impossible")) {
        ctx.setVariable(ProcessConstantsAchat.ERROR_MESSAGE_USER_CONTENT, errorMsg);
      }

      // Récupération des dates de validité/expiration du contrat et inscription dans le contexte camunda
      List<String> validity_start = bddService.sqlRequestOneConditionList(ProcessConstantsAchat.SQL_CONTRACT_VALIDITY_START, ProcessConstantsAchat.TABLE_CONTRACTS, ProcessConstantsAchat.SQL_CONTRACT_NAME, contractRef);
      List<String> validity_end = bddService.sqlRequestOneConditionList(ProcessConstantsAchat.SQL_CONTRACT_VALIDITY_END, ProcessConstantsAchat.TABLE_CONTRACTS, ProcessConstantsAchat.SQL_CONTRACT_NAME, contractRef);

      String debut = validity_start.get(0);
      String fin = validity_end.get(0);

      ctx.setVariable(ProcessConstantsAchat.CONTRACT_VALIDITY_START, debut);
      ctx.setVariable(ProcessConstantsAchat.CONTRACT_VALIDITY_START, fin);

      LOG.error(" Réf du contrat : " + contractRef);
      LOG.error(" Date de début du contrat : " + debut);
      LOG.error(" Date de début du contrat : " + fin);

    } catch (Exception e) {
      LOG.error(" Erreur with balance value block ", e);
    } */
  }
}
