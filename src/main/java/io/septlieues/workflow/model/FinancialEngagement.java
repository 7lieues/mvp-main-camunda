package io.septlieues.workflow.model;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity @Data @NoArgsConstructor @AllArgsConstructor @ToString
@Table(name = "Financial_engagements")
public class FinancialEngagement implements Serializable {
    @Id
    private BigInteger id ;
    @Column(name="amount_engaged")
    private BigInteger amountEngaged;
    @Column(name="contract_id")
    private BigInteger contractId;

}
