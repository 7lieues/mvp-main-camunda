package io.septlieues.workflow.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

@Data @NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table(name = "Suppliers")
public class Supplier implements Serializable {
    @Id
    private BigInteger id ;
    private String name ;
    @Temporal(TemporalType.TIMESTAMP)
    private Date inserted_at;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="updated_at")
    private Date updatedAt;
    private String adresse ;
    @Column(name="order_id")
    private BigInteger orderId ;
    private String status ;
    private BigInteger siren ;
    private BigInteger vat ;


}
