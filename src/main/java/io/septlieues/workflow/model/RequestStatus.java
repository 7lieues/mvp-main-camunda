package io.septlieues.workflow.model;

public enum RequestStatus {
    started("1"),  before_validation("1"),on_validation("2"), finished("2");
    private String status;

    RequestStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}
