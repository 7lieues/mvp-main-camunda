package io.septlieues.workflow.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigInteger;

@Data
@NoArgsConstructor

@Entity
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
@Table(name = "Products")
public class Product implements Serializable {
    @Id
    private BigInteger id ;
    private String ref;
    private String name;
    private String description;
    private int quantity;
    private String image_url;
    private String scale;
    private long price;
    private String type;
    @Column(name="contract_id")
    private BigInteger contractId ;


    public Product(String ref, String name, String description, int quantity, String image_url, String scale, long price) {
        this.ref = ref;
        this.name = name;
        this.description = description;
        this.quantity = quantity;
        this.image_url = image_url;
        this.scale = scale;
        this.price = price;
    }

    public Product(BigInteger id, String ref, String name, String description, int quantity, String image_url, String scale, long price, String type, BigInteger contractId) {
        this.id = id;
        this.ref = ref;
        this.name = name;
        this.description = description;
        this.quantity = quantity;
        this.image_url = image_url;
        this.scale = scale;
        this.price = price;
        this.type = type;
        this.contractId = contractId;
    }
}
