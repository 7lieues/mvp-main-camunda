package io.septlieues.workflow.model;

public class UmbrellaRequest {
    String request_id;
    Boolean approved;
    String status;
    String process_ext_id;
    String process_definition_id;
    String manager_decision;
    String rank;
    String total_steps;
    String step_name;
    String applicant;

    public UmbrellaRequest() {
    }

    public UmbrellaRequest(String request_id, String status, String process_ext_id, String process_definition_id, String manager_decision, String rank, String total_steps, String step_name, String applicant) {
        this.request_id = request_id;
        this.status = status;
        this.process_ext_id = process_ext_id;
        this.process_definition_id = process_definition_id;
        this.manager_decision = manager_decision;
        this.rank = rank;
        this.total_steps = total_steps;
        this.step_name = step_name;
        this.applicant = applicant;
    }

    public UmbrellaRequest(String request_id, String status, String process_ext_id, String process_definition_id, String rank, String total_steps, String step_name, String applicant) {
        this.request_id = request_id;
        this.status = status;
        this.process_ext_id = process_ext_id;
        this.process_definition_id = process_definition_id;
        this.rank = rank;
        this.total_steps = total_steps;
        this.step_name = step_name;
        this.applicant = applicant;
    }

    public UmbrellaRequest(String request_id, Boolean approved, String status, String process_ext_id, String process_definition_id, String manager_decision, String rank, String total_steps, String step_name, String applicant) {
        this.request_id = request_id;
        this.approved = approved;
        this.status = status;
        this.process_ext_id = process_ext_id;
        this.process_definition_id = process_definition_id;
        this.manager_decision = manager_decision;
        this.rank = rank;
        this.total_steps = total_steps;
        this.step_name = step_name;
        this.applicant = applicant;
    }

    public Boolean getApproved() {
        return approved;
    }

    public void setApproved(Boolean approved) {
        this.approved = approved;
    }

    public String getRequest_id() {
        return request_id;
    }

    public String getStatus() {
        return status;
    }

    public String getProcess_ext_id() {
        return process_ext_id;
    }

    public void setRequest_id(String request_id) {
        this.request_id = request_id;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setProcess_ext_id(String process_ext_id) {
        this.process_ext_id = process_ext_id;
    }

    public String getProcess_definition_id() {
        return process_definition_id;
    }

    public void setProcess_definition_id(String process_definition_id) {
        this.process_definition_id = process_definition_id;
    }

    public void setManager_decision(String manager_decision) {
        this.manager_decision = manager_decision;
    }

    public String getManager_decision() {
        return manager_decision;
    }

    public String getRank() {
        return rank;
    }

    public String getTotal_steps() {
        return total_steps;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public void setTotal_steps(String total_steps) {
        this.total_steps = total_steps;
    }

    public String getStep_name() {
        return step_name;
    }

    public void setStep_name(String step_name) {
        this.step_name = step_name;
    }

    public String getApplicant() {
        return applicant;
    }

    public void setApplicant(String applicant) {
        this.applicant = applicant;
    }

    @Override
    public String toString() {
        return "UmbrellaRequest{" +
                "request_id='" + request_id + '\'' +
                ", status='" + status + '\'' +
                ", process_ext_id='" + process_ext_id + '\'' +
                ", process_definition_id='" + process_definition_id + '\'' +
                ", manager_decision='" + manager_decision + '\'' +
                ", rank='" + rank + '\'' +
                ", total_steps='" + total_steps + '\'' +
                ", step_name='" + step_name + '\'' +
                ", applicant='" + applicant + '\'' +
                '}';
    }
}
