package io.septlieues.workflow.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigInteger;

@Entity @Data @NoArgsConstructor @AllArgsConstructor @ToString
@Table(name = "Users")
public class User implements Serializable {

    @Id
    private BigInteger id ;
    private String email ;
    private String username ;
    @Column(name="manager_id")
    private int managerId ;


}
