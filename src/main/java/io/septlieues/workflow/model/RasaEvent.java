package io.septlieues.workflow.model;

public class RasaEvent {

  private String event;

  private String name;

  private String value;

  public RasaEvent(String event, String name, String value) {
    this.name = name;
    this.event = event;
    this.value = value;
  }

  public String getEvent() {
    return event;
  }

  public void setEvent(String event) {
    this.event = event;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }
}
