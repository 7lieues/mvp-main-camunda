package io.septlieues.workflow.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigInteger;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Table(name = "days_leave_types")
public class DayLeaveType implements Serializable
{
    @Id
    private BigInteger id ;
    private String description ;
    @Column(name="annual_balance")
    private long annualBalance ;
    @Column(name="leave_type")
    private long leaveType ;
}
