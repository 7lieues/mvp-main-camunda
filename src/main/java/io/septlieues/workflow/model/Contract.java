package io.septlieues.workflow.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;


import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.*;
import java.util.Date;

@Entity @Data @NoArgsConstructor @AllArgsConstructor @ToString
@Table(name = "Contracts")
public class Contract implements Serializable {
    @Id
    private BigInteger id ;
    private String name ;
    private String link ;
    @Column(name="supplier_id")
    private BigInteger supplierId;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="inserted_at")
    private Date insertedAt;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="updated_at")
    private Date updatedAt;
    @Temporal(TemporalType.DATE)
    @Column(name="validity_start")
    private Date validityStart;
    @Temporal(TemporalType.DATE)
    @Column(name="validity_end")
    private Date validityEnd;
    @Column(name="max_amount")
    private BigInteger maxAmount;
    @Column(name="technical_name")
    private String technicalName;

}
