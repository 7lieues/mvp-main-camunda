package io.septlieues.workflow.model;

import java.util.Date;

public class Order {

    private String ref;
    private Double price;
    Date deliveryDate;
    private int quantity;

    public Order() {
    }

    public Order(String ref, Double price, Date deliveryDate, int quantity) {
        this.ref = ref;
        this.price = price;
        this.deliveryDate = deliveryDate;
        this.quantity = quantity;
    }

    public Order(Double price, Date deliveryDate) {
        this.price = price;
        this.deliveryDate = deliveryDate;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "Order{" +
                "ref='" + ref + '\'' +
                ", price=" + price +
                ", deliveryDate=" + deliveryDate +
                ", quantity=" + quantity +
                '}';
    }
}
