package io.septlieues.workflow.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigInteger;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Table(name = "Days_leave_stocks")
public class DayLeaveStock implements Serializable {
    @Id
    private BigInteger id ;
    private long balance ;
    @Column(name="days_leave_type_id")
    private BigInteger daysLeaveTypeId ;
    @Column(name="user_id")
    private BigInteger userId ;
}
