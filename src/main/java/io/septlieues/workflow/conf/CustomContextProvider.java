package io.septlieues.workflow.conf;

import org.camunda.bpm.extension.keycloak.KeycloakConfiguration;
import org.camunda.bpm.extension.keycloak.KeycloakContextProvider;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class CustomContextProvider extends KeycloakContextProvider {
    /**
     * Creates a new Keycloak context provider
     *
     * @param keycloakConfiguration the Keycloak configuration
     * @param restTemplate          REST template
     */
    public CustomContextProvider(KeycloakConfiguration keycloakConfiguration, RestTemplate restTemplate) {
        super(keycloakConfiguration, restTemplate);
    }
}
