package io.septlieues.workflow.conf;

import org.camunda.bpm.extension.keycloak.KeycloakConfiguration;
import org.camunda.bpm.extension.keycloak.KeycloakContextProvider;
import org.camunda.bpm.extension.keycloak.KeycloakIdentityProviderSession;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class KeycloakIdentityProviderSessionPlugin extends KeycloakIdentityProviderSession {
    /**
     * Creates a new session.
     *
     * @param keycloakConfiguration   the Keycloak configuration
     * @param restTemplate            REST template
     * @param keycloakContextProvider Keycloak context provider
     */
    public KeycloakIdentityProviderSessionPlugin(KeycloakConfiguration keycloakConfiguration, RestTemplate restTemplate, KeycloakContextProvider keycloakContextProvider) {
        super(keycloakConfiguration, restTemplate, keycloakContextProvider);
    }
}
