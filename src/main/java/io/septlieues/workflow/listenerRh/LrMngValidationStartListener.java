package io.septlieues.workflow.listenerRh;


import io.septlieues.workflow.model.RequestStatus;
import io.septlieues.workflow.services.UmbrellaNotification;
import io.septlieues.workflow.utils.ProcessConstantsRh;
import org.camunda.bpm.engine.delegate.DelegateTask;
import org.camunda.bpm.engine.delegate.TaskListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class LrMngValidationStartListener implements TaskListener {
    private static final Logger LOG = LoggerFactory.getLogger(LrMngValidationStartListener.class);

    @Autowired
    RestTemplate restTemplate;

    @Value("${umbrella.host}")
    private String umbrellaHost;


    @Autowired
    private UmbrellaNotification umbrellaNotification;


    @Override
    public void notify(DelegateTask delegateTask) {

        // On recup de Cam les valeurs du trackerId et du Applicant et on s'en sert pour fabriquer le requestId
        String process_definition_id = (String) delegateTask.getProcessDefinitionId();
        String trackerId = (String) delegateTask.getExecution().getVariable(ProcessConstantsRh.TRACKER_ID_LR);
        String step_name = "Attente validation manager";
        String applicant = (String) delegateTask.getVariable(ProcessConstantsRh.APPLICANT);
        String request_id = trackerId.split("::")[1];
        String process_ext_id = delegateTask.getProcessInstanceId();

        // On met à jours le status de la requête Umbrella
        try {
            umbrellaNotification.umbrellaStartRequest(request_id, RequestStatus.before_validation.getStatus(),
                                                        process_ext_id, process_definition_id,"2","2",step_name,applicant);
        } catch (Exception e) {
            LOG.error("ERROR TO SEND REQUEST UMBRELLA START USER TASK "+e);
        }
    }
}
