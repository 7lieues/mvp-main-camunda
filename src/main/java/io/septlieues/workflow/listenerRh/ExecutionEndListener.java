package io.septlieues.workflow.listenerRh;


import io.septlieues.workflow.model.RequestStatus;
import io.septlieues.workflow.services.UmbrellaNotification;
import io.septlieues.workflow.utils.ProcessConstantsRh;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.ExecutionListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class ExecutionEndListener implements ExecutionListener {
    private static final Logger LOG = LoggerFactory.getLogger(ExecutionEndListener.class);
    @Autowired
    RestTemplate restTemplate;

    @Value("${umbrella.host}")
    private String umbrellaHost;
    @Autowired
    private UmbrellaNotification umbrellaNotification;

    @Override
    public void notify(DelegateExecution delegateExecution) throws Exception {
        String process_definition_id = (String) delegateExecution.getProcessDefinitionId();
        String trackerId = (String) delegateExecution.getVariable(ProcessConstantsRh.TRACKER_ID_LR);
        String applicant = (String) delegateExecution.getVariable(ProcessConstantsRh.APPLICANT);
        String step_name = "Fin du workflow";
        String request_id = trackerId.split("::")[1];
        // String request_id = null;
        // if (trackerId.contains("::")) {
        //     request_id = trackerId.split("::")[1];
        // } else request_id = trackerId;
        String process_ext_id = delegateExecution.getProcessInstanceId();
        // HistoryService historyService = ProcessEngines.getDefaultProcessEngine().getHistoryService();
        // int historicalStepCount = historyService.createHistoricTaskInstanceQuery()
        //         .processInstanceId(process_ext_id).list().size();
        // LOG.info("================================HistoricalStepCount================================ "+historicalStepCount);
        try {
            umbrellaNotification.umbrellaStartRequest(request_id, RequestStatus.finished.getStatus(), process_ext_id, process_definition_id,"4","4",step_name,applicant);
        } catch (Exception e) {
            LOG.error("ERROR TO SEND REQUEST UMBRELLA FINISH USER TASK "+e);
        }
    }
}
