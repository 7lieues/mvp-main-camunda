package io.septlieues.workflow.listenerRh;


import io.septlieues.workflow.model.RasaEvent;
import io.septlieues.workflow.services.RasaService;
import io.septlieues.workflow.utils.ProcessConstantsRh;
import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.delegate.DelegateTask;
import org.camunda.bpm.engine.delegate.TaskListener;
import org.camunda.bpm.engine.identity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;


@Component
public class TsUserTimesheetListener implements TaskListener {

    private static final Logger LOG = LoggerFactory.getLogger(TsUserTimesheetListener.class);

    @Autowired
    IdentityService identityService;

    @Autowired
    private RasaService rasaService;

    @Autowired
    private Environment env;

    @Autowired
    private JavaMailSender javaMailSender;

    @Value("${rasa.event.slot.ts.usertask_id}")
    private String rasaUserTaskIdSlot;
    
    @Value("${rasa.reset.slot.ts}")
    private String rasaActionUserResetTimeSheet;
    
    @Override
    public void notify(DelegateTask delegateTask) {
        
        
        String assignee = delegateTask.getAssignee();
        String taskId = (String) delegateTask.getId();
        delegateTask.getExecution().setVariable(ProcessConstantsRh.TASK_ID,taskId);
        LOG.info("********** Task ID : " + taskId);
        String userTrackID = (String) delegateTask.getExecution().getVariable(ProcessConstantsRh.TRACKER_ID_TS);
        if (assignee != null) {
            User user = identityService.createUserQuery().userId(assignee).singleResult();
            if (user != null) {
                try {
                    //création header
                    HttpHeaders requestHeader = new HttpHeaders();

                    //paramétrage slot "ent_ts_user_usertask_id" à remplir avec le task id rasa
                    RasaEvent rasaEvent = new RasaEvent("slot", rasaUserTaskIdSlot, taskId);
                    HttpEntity<RasaEvent> requestEntitySetSlot = new HttpEntity<>(rasaEvent, requestHeader);

                    //déploiement requête
                    rasaService.setSlot(userTrackID,requestEntitySetSlot);

                    } catch (Exception e) {
                    LOG.warn("Could not send the slots to rasa", e);
                }
                try {
                    //demande à rasa de lancer l'action "form_ts_user_re_send" au user
                    rasaService.utter(rasaActionUserResetTimeSheet, userTrackID);
                    
                } catch (Exception e) {
                    LOG.error("error when asking to regenerate the formulaire : " + userTrackID, e);
                }

            } else {
                    LOG.info("user is null");
            }
        }
       
    }
}