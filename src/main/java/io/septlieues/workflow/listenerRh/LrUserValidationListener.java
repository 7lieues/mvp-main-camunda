package io.septlieues.workflow.listenerRh;


import io.septlieues.workflow.model.RasaEvent;
import io.septlieues.workflow.services.RasaService;
import io.septlieues.workflow.utils.ProcessConstantsRh;
import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.delegate.DelegateTask;
import org.camunda.bpm.engine.delegate.TaskListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

@Component
public class LrUserValidationListener implements TaskListener{
    private static final Logger LOG = LoggerFactory.getLogger(LrUserValidationListener.class);
    @Autowired
    IdentityService identityService;
    @Autowired
    private RasaService rasaService;
    @Autowired
    private Environment env;
    @Autowired
    private JavaMailSender javaMailSender;
    @Value("${rasa.event.slot.task_id.name_user}")
    private String rasaTaskIdSlot;
    @Override
    public void notify(DelegateTask delegateTask) {
        // On recup le taskId et on l'écrit dans le context Cam
        String taskId = delegateTask.getId();
        delegateTask.getExecution().setVariable(ProcessConstantsRh.TASK_ID,taskId);
        LOG.info("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! checkpoint!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        LOG.info("******* taskId: " + taskId);
        try {
            // On crée la requete set slot du taskId
            RasaEvent rasaEvent = new RasaEvent("slot", rasaTaskIdSlot, taskId);
            HttpHeaders requestHeader = new HttpHeaders();
            HttpEntity<RasaEvent> requestEntitySetSlot = new HttpEntity<>(rasaEvent, requestHeader);
            String TrackerId = (String) delegateTask.getExecution().getVariable(ProcessConstantsRh.TRACKER_ID_LR);
            LOG.info("!!!!!!!!!!" + TrackerId);
            LOG.info("!!!!!!!!!!" + taskId);
            // On execute le set slot du taskId
            rasaService.setSlot(TrackerId,requestEntitySetSlot);
        } catch (Exception e) {
            LOG.warn("Could not send email to assignee", e);
        }
    }
}