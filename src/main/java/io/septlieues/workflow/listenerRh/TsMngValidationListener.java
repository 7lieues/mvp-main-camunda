package io.septlieues.workflow.listenerRh;

import io.septlieues.workflow.model.RasaEvent;
import io.septlieues.workflow.services.RasaService;

import io.septlieues.workflow.utils.ProcessConstantsRh;
import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.delegate.DelegateTask;
import org.camunda.bpm.engine.delegate.TaskListener;
import org.camunda.bpm.engine.identity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;


@Component
public class TsMngValidationListener implements TaskListener {

    private static final Logger LOG = LoggerFactory.getLogger(TsMngValidationListener.class);

    @Autowired
    IdentityService identityService;

    @Autowired
    private RasaService rasaService;

    @Autowired
    private Environment env;

    @Autowired
    private JavaMailSender javaMailSender;

    @Value("${rasa.event.slot.ts.task_id.name}")
    private String rasaTaskIdSlot;

    @Override
    public void notify(DelegateTask delegateTask) {

        String assignee = delegateTask.getAssignee();
        String taskId = delegateTask.getId();
        delegateTask.getExecution().setVariable(ProcessConstantsRh.TASK_ID,taskId);
        LOG.info("********** Task ID : " + taskId);
        LOG.info("********** assigne : " + assignee);
        
        if (assignee != null) {
            User user = identityService.createUserQuery().userId(assignee).singleResult();
            LOG.info("********** assigne : " + assignee);
            LOG.info("********** user : " + user);
        if (user != null) {
                try {

                    // paramêtre requête rasa sur le slot "ent_ts_mng_validation_usertask_id" en y mettant taskid
                    RasaEvent rasaEvent = new RasaEvent("slot", rasaTaskIdSlot, taskId);

                    //création header
                    HttpHeaders requestHeader = new HttpHeaders();
                    HttpEntity<RasaEvent> requestEntitySetSlot = new HttpEntity<>(rasaEvent, requestHeader);

                    //récupération tracker id manager
                    String managerTrackerId = (String) delegateTask.getExecution().getVariable(ProcessConstantsRh.MANAGER_TRACKER_ID);

                    //déploiement de la requete
                    rasaService.setSlot(managerTrackerId,requestEntitySetSlot);
                    LOG.info("********** Mail manager à notifier : "+user.getEmail());

                    //paramétrage et envoie du mail depuis camunda (mail qui renvoie sur la demande à valider)
                    // SimpleMailMessage mail = new SimpleMailMessage();
                    // mail.setTo(user.getEmail());
                    // mail.setFrom(env.getProperty("spring.mail.username"));
                    // mail.setSubject("Test user task.");
                    // mail.setText("Lien vers le user task : https://dev.7lieues.io/mvp-achat-camunda/app/tasklist/default/#/task=" + taskId);
                    // javaMailSender.send(mail);

                } catch (Exception e) {
                    LOG.warn("Could not send email to assignee", e);
                }

            } else {
                    LOG.info("user is null");
            }
        }
    }
}