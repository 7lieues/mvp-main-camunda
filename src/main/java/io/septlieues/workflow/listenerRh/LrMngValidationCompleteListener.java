package io.septlieues.workflow.listenerRh;


import io.septlieues.workflow.model.RequestStatus;
import io.septlieues.workflow.services.UmbrellaNotification;
import io.septlieues.workflow.utils.ProcessConstantsRh;
import org.camunda.bpm.engine.delegate.DelegateTask;
import org.camunda.bpm.engine.delegate.TaskListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class LrMngValidationCompleteListener implements TaskListener {
    private static final Logger LOG = LoggerFactory.getLogger(LrMngValidationCompleteListener.class);

    @Autowired
    RestTemplate restTemplate;

    @Value("${umbrella.host}")
    private String umbrellaHost;


    @Autowired
    private UmbrellaNotification umbrellaNotification;

    @Override
    public void notify(DelegateTask delegateTask) {
        LOG.info("*************** Dans la classe LrMngValidationCompleteListener ****************");
        // On récup du context Cam: TrackerId, applicant, ManagerMail. On fabrique le requestId à partir du trackerId
        String process_definition_id = (String) delegateTask.getProcessDefinitionId();
        String trackerId = (String) delegateTask.getExecution().getVariable(ProcessConstantsRh.TRACKER_ID_LR);
        String applicant = (String) delegateTask.getVariable(ProcessConstantsRh.APPLICANT);
        String AdressMailManager = (String) delegateTask.getVariable(ProcessConstantsRh.MANAGER_EMAIL);
        String step_name = "Validation manager effectuée";
        String request_id = trackerId.split("::")[1];
        String process_ext_id = delegateTask.getProcessInstanceId();
        String manager_decision = null;
        // On l'ouvre en format Bool afin de le réutiliser plus tard dans la gate
        Boolean approved = (Boolean) delegateTask.getExecution().getVariable(ProcessConstantsRh.APPROVED);
        LOG.info("*************** Approved "+approved+" ****************");


        // On fabrique les variables de return selon le choix du Mng
        if (approved) {
            manager_decision = "1::".concat(AdressMailManager);
       
        } else {
            manager_decision = "0::".concat(AdressMailManager);
        }
        // Ce bloque permet de mettre à jours le status de la requette Umbrella
        try {
            LOG.info("*************** Status de la requette vers Umbrella ***************");
            umbrellaNotification.umbrellaCompleteRequest(request_id, approved,
                                                        RequestStatus.on_validation.getStatus(), process_ext_id, process_definition_id,
                                                                                                                manager_decision,"3","3",step_name, applicant);
        } catch (Exception e) {
            LOG.error("ERROR TO SEND REQUEST UMBRELLA COMPLETE USER TASK " + e);
        }
        LOG.info("*************** Data vers Umbrella : approved : "+approved+", RequestStatus.on_validation :"+RequestStatus.on_validation.getStatus(),", process_ext_id :"+process_ext_id+", manager_decision :"
                +manager_decision+", step_name :"+step_name+", applicant :"+applicant+"***************");

    }
}