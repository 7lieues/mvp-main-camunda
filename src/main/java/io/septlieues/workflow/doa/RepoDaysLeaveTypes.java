package io.septlieues.workflow.doa;

import io.septlieues.workflow.model.Contract;
import io.septlieues.workflow.model.DayLeaveType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.Collection;
import java.util.List;

@Repository
public interface RepoDaysLeaveTypes extends JpaRepository<DayLeaveType, BigInteger> {
    @Query("SELECT d FROM DayLeaveType d WHERE d.description = :typeConge")
    DayLeaveType retrieveDayLeaveTypeBydescrip(@Param("typeConge") String typeConge);

    @Query("SELECT d FROM DayLeaveType d WHERE d.leaveType = :typeConge")
    DayLeaveType retrieveDayLeaveTypeByTypeConge(@Param("typeConge") String typeConge);


   // @Query(value = "update DayLeaveType u set u.annualBalance = :balance WHERE u.id = :id")
   // int updatesolde(@Param("balance") int balance ,@Param("listid") Collection<String> listId);

}
