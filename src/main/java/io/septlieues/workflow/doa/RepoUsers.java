package io.septlieues.workflow.doa;

import io.septlieues.workflow.model.Contract;
import io.septlieues.workflow.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.List;

@Repository
public interface RepoUsers extends JpaRepository<User, BigInteger> {
    @Query("SELECT u.id FROM User u")
    List<String> retrieveAllUserId();

    @Query("SELECT u FROM User u WHERE u.email = :email")
    User retrieveUserByEmail(@Param("email") String email);

}
