package io.septlieues.workflow.doa;

import io.septlieues.workflow.model.Contract;
import io.septlieues.workflow.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.math.BigInteger;
@Repository
public interface RepoProducts extends JpaRepository<Product ,BigInteger> {


    @Query("SELECT p FROM Product p WHERE p.scale = :scaleProduct or p.scale = :scaleProd ")
    List<Product> retrieveProductScale (@Param("scaleProduct") String scaleProduct,@Param("scaleProd") String scaleProd);


    @Query("SELECT p FROM Product p WHERE p.ref = :reference")
    Product retrieveProductRef (@Param("reference") String ref);

    @Query("SELECT p FROM Product p WHERE p.quantity > 0 and p.type = :typeProduct and p.scale = :scaleProduct")
    List<Product> retrieveProductbyTelOneGam (@Param("typeProduct") String typeProduct ,@Param("scaleProduct") String scaleProduct);
}
