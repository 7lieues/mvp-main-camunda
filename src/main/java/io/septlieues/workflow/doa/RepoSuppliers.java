package io.septlieues.workflow.doa;

import io.septlieues.workflow.model.Supplier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


import java.math.BigInteger;
@Repository
public interface RepoSuppliers extends JpaRepository<Supplier, BigInteger> {
}
