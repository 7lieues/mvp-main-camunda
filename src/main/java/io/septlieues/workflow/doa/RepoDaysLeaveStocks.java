package io.septlieues.workflow.doa;

import io.septlieues.workflow.model.DayLeaveStock;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
@Repository
public interface RepoDaysLeaveStocks extends JpaRepository<DayLeaveStock , BigInteger> {
}
