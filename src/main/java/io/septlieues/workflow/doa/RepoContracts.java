package io.septlieues.workflow.doa;

import io.septlieues.workflow.model.Contract;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.List;

@Repository
public interface RepoContracts extends JpaRepository<Contract, BigInteger> {

    @Query("SELECT c FROM Contract c WHERE c.technicalName = :technicalName")
    Contract retrieveContractsByTechName(@Param("technicalName") String technicalName);
}
