package io.septlieues.workflow.services;


import org.springframework.http.HttpEntity;

public interface RasaService {

    void utter(String utterName, String trackerId) throws Exception;

    void rasaSetSlot(String slotName, String slotValue, String trackerId) throws Exception;

    default void setSlot(String trackerId,HttpEntity<?> httpEntity) {
    }


}
