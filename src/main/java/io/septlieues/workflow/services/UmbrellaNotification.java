package io.septlieues.workflow.services;


import org.json.JSONObject;

public interface UmbrellaNotification {

    void umbrellaStartRequest(String request_id, String status, String process_ext_id, String process_definition_id, String ranking, String total_steps, String step_name,String applicant) throws Exception;
    void umbrellaCompleteRequest (String request_id, Boolean approved, String status, String process_ext_id, String process_definition_id, String decision, String ranking, String total_steps,String step_name,String applicant) throws Exception;
    JSONObject readLeaveRequest(String TrackerID, String Leave_type, String Start_date, String End_date) throws Exception;
    JSONObject createLeaveRequest(String TrackerID, String Leave_type, String Start_date, String End_date, int validation_status) throws Exception;
    JSONObject updateLeaveRequest(int leave_request_id) throws Exception;
    JSONObject cancelLeaveRequest(int leave_request_id) throws Exception;
    JSONObject stockLeaveRequest(String TrackerID) throws Exception;
    JSONObject createLeaveRequestNbrDays(String TrackerID, String Leave_type, String Start_date, int nbr_days, int validation_status) throws Exception;

}
