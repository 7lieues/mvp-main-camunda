package io.septlieues.workflow.services;

import io.septlieues.workflow.model.RasaAction;
import io.septlieues.workflow.model.RasaEvent;

import org.camunda.bpm.engine.delegate.BpmnError;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Map;
import java.util.HashMap;

import com.google.gson.Gson;

@Service
public class RasaServiceImpl implements RasaService {

    private static final Logger LOG = LoggerFactory.getLogger(RasaServiceImpl.class);
    
    @Autowired
    private Gson gson;

    @Autowired
    private RestTemplate restTemplate;

    @Value("${rasa.action.listen.name}")
    private String rasaActionListenName;

    @Value("${rasa.action.listen.policy}")
    private String rasaActionPolicy;

    @Value("${rasa.action.listen.confidence}")
    private String rasaActionConfidence;

    @Value("${rasa.host.collab}")
    private String rasaHostCollab;

    @Value("${rasa.host.manager}")
    private String rasaHostManager;

    @Value("${rasa.default.utter.policy}")
    private String rasaUtterPolicy;

    @Value("${rasa.default.utter.confidence}")
    private String rasaConfidence;

    @Override
    public void utter(String rasaUtterName, String trackerId) throws Exception {
        // add json forma to header
        HttpHeaders requestHeader = new HttpHeaders();
        requestHeader.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        // build first uri for rasa set slot

        String typeUser = trackerId.split("::")[2];

        StringBuilder rasaURIListen = new StringBuilder();
        if(typeUser.equals("1")){
            rasaURIListen.append(rasaHostManager).append("/conversations/").append(trackerId)
                    .append("/execute?output_channel=callback");
        }else{
            rasaURIListen.append(rasaHostCollab).append("/conversations/").append(trackerId)
                    .append("/execute?output_channel=callback");
        }

        LOG.info("**************  RASA ACTION TO " + rasaURIListen.toString() );

        UriComponentsBuilder rasaURIListenBuilder = UriComponentsBuilder.fromUriString(rasaURIListen.toString());

        rasaAction(requestHeader, rasaURIListenBuilder, rasaUtterName, trackerId);// call the action
        rasaActionListen(requestHeader, rasaURIListenBuilder, trackerId);// make rasa listen

    }

    @Override
    public void setSlot(String trackerId, HttpEntity<?> httpEntity) {
        HttpHeaders requestHeader = new HttpHeaders();
        String typeUser = trackerId.split("::")[2];
        requestHeader.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        // build first uri for rasa set slot
        StringBuilder rasaURISlot = new StringBuilder();
        if(typeUser.equals("1")){

            rasaURISlot.append(rasaHostManager).append("/conversations/").append(trackerId).append("/tracker/events");
            LOG.info("*************         MANAGER   ************************* " );
        }else{
          rasaURISlot.append(rasaHostCollab).append("/conversations/").append(trackerId).append("/tracker/events?include_events=APPLIED");}
        UriComponentsBuilder rasaURISlotBuilder = UriComponentsBuilder.fromUriString(rasaURISlot.toString());
        LOG.info("******************************************************************* " );
        LOG.info("**************  setSlot to   "  + rasaURISlot.toString() );
        LOG.info("******************************************************************* " );

        //ResponseEntity<Map> response = null;
        try {
            restTemplate.postForEntity(rasaURISlotBuilder.toUriString(), httpEntity, Map.class);
        } catch (HttpStatusCodeException e) {
            LOG.error("HttpStatusCodeException :", e);
            throw new BpmnError("WebServiceError", e.getMessage());
        } catch (Exception e) {
            LOG.error("An error occured while set rasa slot", e);
            throw new BpmnError("WebServiceError", e.getMessage());
        }

    }

    // Fonction d'abstraction rapide (sûrement pas besoin de ré-invoquer le RasaEvent [et/ou comprendre pourquoi il est invoqué dans ce cas et pas dans le cas du Utter], mais pas le temps de simplifier le code)

 @Override
    public void rasaSetSlot(String slotName, String slotValue, String trackerId) throws Exception {
    
        HttpHeaders requestHeader = new HttpHeaders();
        requestHeader.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);

        RasaEvent rasaEvent = new RasaEvent("slot", slotName, slotValue);
        HttpEntity<RasaEvent> requestEntitySetSlot = new HttpEntity<>(rasaEvent, requestHeader);

        LOG.info("******************************************************************* " );
        LOG.info("**************  RasaSetSlot to  **********************************  "  );

        try {
            setSlot(trackerId, requestEntitySetSlot);

        } catch (HttpStatusCodeException e) {
            LOG.error("HttpStatusCodeException :", e);
            throw new BpmnError("WebServiceError", e.getMessage());
        } catch (Exception e) {
            LOG.error("An error occured while set rasa slot", e);
            throw new BpmnError("WebServiceError", e.getMessage());
        }
    
    }


    private void rasaAction(HttpHeaders requestHeader, UriComponentsBuilder uri, String rasaUtterName,
            String trackerId) {
        RasaAction rasaUtter = new RasaAction(rasaUtterName, rasaUtterPolicy, 0.98);  //rasaConfidence
        LOG.info("**************  rasaAction  ************************");
        /*
        Map<String, Object> inputMap = new HashMap<>();
        inputMap.put("name", rasaUtterName);
        inputMap.put("policy", rasaUtterPolicy);
        inputMap.put("confidence", 0.987232);
        */
        // convert map to JSON String
       // String rasaUtter = gson.toJson(inputMap);
        HttpEntity<RasaAction> requestEntityRasaUtter = new HttpEntity<>(rasaUtter, requestHeader);
        try {
           restTemplate.postForEntity(uri.toUriString(), requestEntityRasaUtter, Map.class);
        } catch (HttpStatusCodeException e) {
            LOG.error("HttpStatusCodeException :", e);
            throw new BpmnError("WebServiceError", e.getMessage());
        } catch (Exception e) {
            LOG.error("An exception occured while send action  to rasa", e);
            throw new BpmnError("WebServiceError", e.getMessage());
        }

    }

    private void rasaActionListen(HttpHeaders requestHeader, UriComponentsBuilder uri, String trackerId) {
        LOG.info("**************  rasaAction  ************************");
        LOG.info(" The uri for rasa action listen is : " + uri.toUriString());
        RasaAction rasaUtterListen = new RasaAction(rasaActionListenName, rasaActionPolicy, 0.98);
        HttpEntity<RasaAction> requestEntityUtterListen = new HttpEntity<>(rasaUtterListen, requestHeader);
        //ResponseEntity<Map> response = null;
        try {
            restTemplate.postForEntity(uri.toUriString(), requestEntityUtterListen, Map.class);
        } catch (HttpStatusCodeException e) {
            LOG.error("Error sending request HttpStatusCodeException :", e);
            throw new BpmnError("WebServiceError", e.getMessage());
        } catch (Exception e) {
            LOG.error("An exception occured while send action listen to rasa", e);
            throw new BpmnError("WebServiceError", e.getMessage());
        }

    }

}
