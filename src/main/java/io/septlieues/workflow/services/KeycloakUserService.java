package io.septlieues.workflow.services;


import io.septlieues.workflow.conf.KeycloakIdentityProvider;
import org.camunda.bpm.extension.keycloak.KeycloakContextProvider;
import org.camunda.bpm.extension.keycloak.KeycloakIdentityProviderSession;
import org.camunda.bpm.extension.keycloak.KeycloakUserNotFoundException;
import org.camunda.bpm.extension.keycloak.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;


@Component
public class KeycloakUserService {
    @Autowired
    KeycloakIdentityProvider keycloakIdentityProvider;
    @Autowired
    RestTemplate restTemplate;

    @Autowired
    KeycloakContextProvider keycloakContextProvider;

    @Autowired
    KeycloakIdentityProviderSession keycloakIdentityProviderSession;

    public String getKeycloakUserAttributesByUserEmail(String userId)
            throws KeycloakUserNotFoundException, RestClientException {
        ResponseEntity<String> response = restTemplate.exchange(
                keycloakIdentityProvider.getKeycloakAdminUrl() + "/users?email=" + userId, HttpMethod.GET,
                keycloakContextProvider.createApiRequestEntity(), String.class);


        String userAttributes = new JSONArray(response.getBody()).getJSONObject(0).getString("attributes");
        String userUserName = new JSONArray(response.getBody()).getJSONObject(0).getString("username");
        String keycloakJson = userAttributes.substring(0, userAttributes.length() - 1) + ",\"username\":\""+ userUserName+"\"}";
        
        //return new JSONArray(response.getBody()).getJSONObject(0).getString("attributes");
        return keycloakJson;

    }

    public String searchUserByCustomAttribute(String attributeName, String attributeValue)
            throws KeycloakUserNotFoundException, RestClientException {
        ResponseEntity<String> response = restTemplate.exchange(
                keycloakIdentityProvider.getKeycloakIssuerUrl() + "/admin/user-attribute?name=" + attributeName
                        + "&value=" + attributeValue,
                HttpMethod.GET, keycloakContextProvider.createApiRequestEntity(), String.class);
        // manage null value send mail to camunda
        return new JSONArray(response.getBody()).getJSONObject(0).get("email").toString();

    }

}
