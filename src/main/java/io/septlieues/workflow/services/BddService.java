package io.septlieues.workflow.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;


@Component
public class BddService {
  private static final Logger LOG = LoggerFactory.getLogger(BddService.class);

  @Autowired
  private Environment env;

  public List<LocalDate> getHolidays(String nom_table, String nom_condition, LocalDate startDate, LocalDate endDate )  

  {
    List <LocalDate> holidays = new ArrayList<>();
    
    try (
      Connection con = DriverManager.getConnection(
        env.getProperty("spring.datasource-business.url"),
        env.getProperty("spring.datasource-business.username"),
        env.getProperty("spring.datasource-business.password")
      );
      Statement stmt = con.createStatement();
    ) {
      String sql = "SELECT " + nom_condition + " from " + nom_table + " where " + nom_condition + " >= '" + startDate + "' and " + nom_condition + " <= '" + endDate + "'";
      LOG.info("********** SQL Request for getTable Fonction***** :   " + sql);
      ResultSet res = stmt.executeQuery(sql);

      while (res.next() ) {
        Date date = res.getDate(nom_condition);
        LocalDate dl =date.toLocalDate();
        holidays.add(dl);
       
      }
    } catch (SQLException e) {
      LOG.error(" can not find your request", e);
    } for (int i=0;i<holidays.size();i++){LOG.info("le "+i+"er element du tableau des jours féries compris dans la demande de conges est :  "+holidays.get(i));}
    return holidays ;
  }

  public String sqlRequestOneCondition(String info, String table, String nomCondition, String condition) {
    
    String resultList = "";
    String impossible = "impossible";
    
    try  {

      String url = env.getProperty("spring.datasource-business.url");
      String username = env.getProperty("spring.datasource-business.username");
      String password = env.getProperty("spring.datasource-business.password");

      Connection conn = DriverManager.getConnection(url, username, password);
      Statement stmt = conn.createStatement();
      String sql = ("Select " + info + " from " + table + " where " + nomCondition + " = " + "'" + condition + "'");

      ResultSet rs = stmt.executeQuery(sql);
      ResultSetMetaData resultMeta = rs.getMetaData();


      while (rs.next()) {
        resultList = resultList + rs.getString(info);
      }
      conn.close();
    } catch (SQLException e) {
        LOG.error("******** can not make SQL request");
    }

    int resultListLength = resultList.length();

    if (resultListLength == 0)
      return impossible;
    else 
      return resultList;
  }


public String sqlRequestTwoCondition(String info, String table, String nomCondition, String condition, String nomCondition2, String condition2) {
    
  String resultList = "";
  String impossible = "impossible";

  try  {

    String url = env.getProperty("spring.datasource-business.url");
    String username = env.getProperty("spring.datasource-business.username");
    String password = env.getProperty("spring.datasource-business.password");

    Connection conn = DriverManager.getConnection(url, username, password);
    Statement stmt = conn.createStatement();
    String sql = ("Select " + info + " from " + table + " where " + nomCondition + " = " + "'" + condition + "'" + " and " + nomCondition2 + " = " + "'" + condition2 + "'");
    
    ResultSet rs = stmt.executeQuery(sql);
    ResultSetMetaData resultMeta = rs.getMetaData();


    while (rs.next()) {
      resultList = resultList + rs.getString(info);
    }
    conn.close();
  } catch (SQLException e) {
      LOG.error("******** can not make SQL request");
  }
      
  int resultListLength = resultList.length();

  if (resultListLength == 0)
    return impossible;
  else 
    return resultList;
  }

  public List<String> sqlRequestOneConditionList(String info, String table, String nomCondition, String condition) {
    
    List<String> resultList = new ArrayList<>();
    try  {

      String url = env.getProperty("spring.datasource-business.url");
      String username = env.getProperty("spring.datasource-business.username");
      String password = env.getProperty("spring.datasource-business.password");

      Connection conn = DriverManager.getConnection(url, username, password);
      Statement stmt = conn.createStatement();
      String sql = ("Select " + info + " from " + table + " where " + nomCondition + " = " + "'" + condition + "'");
      
      ResultSet rs = stmt.executeQuery(sql);
      ResultSetMetaData resultMeta = rs.getMetaData();


      while (rs.next()) {
        String finalList = new String(rs.getString(info));
        resultList.add(finalList);
      }
      conn.close();
    } catch (SQLException e) {
        LOG.error("******** can not make SQL request");
    }

    int test = resultList.size();

    if (test == 0)
    resultList.add("impossible");

    return resultList;
  }

  public List<String> sqlRequestTwoConditionList(String info, String table, String nomCondition, String condition, String nomCondition2, String condition2) {
    
    List<String> resultList = new ArrayList<>();
    try  {

      String url = env.getProperty("spring.datasource-business.url");
      String username = env.getProperty("spring.datasource-business.username");
      String password = env.getProperty("spring.datasource-business.password");

      Connection conn = DriverManager.getConnection(url, username, password);
      Statement stmt = conn.createStatement();
      String sql = ("Select " + info + " from " + table + " where " + nomCondition + " = " + "'" + condition + "'" + " and " + nomCondition2 + " = " + "'" + condition2 + "'");

      ResultSet rs = stmt.executeQuery(sql);
      ResultSetMetaData resultMeta = rs.getMetaData();


      while (rs.next()) {
        String finalList = new String(rs.getString(info));
        resultList.add(finalList);
      }
      conn.close();
    } catch (SQLException e) {
        LOG.error("******** can not make SQL request");
    }

    int test = resultList.size();

    if (test == 0)
    resultList.add("impossible");

    return resultList;
  }

  public void UpdateRequestOneCondition(String table, String nomColonneInfoSet, String infoSet, String nomCondition, String condition) {
    
    try  {

      String url = env.getProperty("spring.datasource-business.url");
      String username = env.getProperty("spring.datasource-business.username");
      String password = env.getProperty("spring.datasource-business.password");

      Connection conn = DriverManager.getConnection(url, username, password);
      Statement stmt = conn.createStatement();
      String sql = ("Update" + table + "set" + nomColonneInfoSet + "= '" + infoSet + "'" + "where" + nomCondition + "= '" + condition + "'");
      
      int update = stmt.executeUpdate(sql);

      conn.close();
    } catch (SQLException e) {
        LOG.error("******** can not make SQL request");
    }
  }

  public void UpdateRequestTwoCondition(String table, String nomColonneInfoSet, String infoSet, String nomCondition, String condition, String nomCondition2, String condition2) {
    
    try  {

      String url = env.getProperty("spring.datasource-business.url");
      String username = env.getProperty("spring.datasource-business.username");
      String password = env.getProperty("spring.datasource-business.password");

      Connection conn = DriverManager.getConnection(url, username, password);
      Statement stmt = conn.createStatement();
      String sql = ("Update " + table + " set " + nomColonneInfoSet + " = '" + infoSet + "'" + " where " + nomCondition + " = '" + condition + "' and " + nomCondition2 + " = '" + condition2 + "'");
      LOG.info("requete sql : " + sql);
      int update = stmt.executeUpdate(sql);

      conn.close();
    } catch (SQLException e) {
        LOG.error("******** can not make SQL request");
    }
  }

  ///////////////////////////////// mvp-camunda-achat  //////////////////


  public int getValue(String nom_table,String colonne_filtre1,int valeur_filtre1,String colonne_filtre2,int valeur_filtre2,String valeur_retour )
  {
    int valeur_retour_final = 0;
    try (
            Connection con = DriverManager.getConnection(
                    env.getProperty("spring.datasource-business.url"),
                    env.getProperty("spring.datasource-business.username"),
                    env.getProperty("spring.datasource-business.password")
            );
            Statement stmt = con.createStatement();
    ) {boolean test=true ;
      String sql = "SELECT *  from  " + nom_table;
      LOG.info("********** SQL Request for getValue Fonction***** :   " + sql);
      ResultSet res = stmt.executeQuery(sql);

      while (res.next() && test == true) {
        int col1 = res.getInt(colonne_filtre1);
        int col2 = res.getInt(colonne_filtre2);
        if ((col1 == valeur_filtre1) && (col2 == valeur_filtre2)) {
          test = false;
          valeur_retour_final = res.getInt(valeur_retour);

          LOG.info( "********** valeur  " + valeur_retour + "  demandé est  : " + valeur_retour_final);
        }

      }
    } catch (SQLException e) {
      LOG.error(" can not find your request", e);
    }
    return valeur_retour_final;
  }

  // une fonction qui retourne une colonne demandé avec un seul filtre
  public int getValueOne( String nom_table, String colonne_filtre1,String valeur_filtre1,String colonne_retour ) {
    int valeur_retour_final = 0;
    try (
            Connection con = DriverManager.getConnection(
                    env.getProperty("spring.datasource-business.url"),
                    env.getProperty("spring.datasource-business.username"),
                    env.getProperty("spring.datasource-business.password")
            );
    ) {
      String sql ="SELECT " +colonne_retour +"  from  " +nom_table +" where  " +colonne_filtre1 +" = ?";
      LOG.info("********** SQL Request for GetValueOne function  :   " + sql);
      PreparedStatement ps = con.prepareStatement(sql);
      ps.setString(1, valeur_filtre1);

      ResultSet rs = ps.executeQuery();
      while (rs.next()) {
        valeur_retour_final = rs.getInt(colonne_retour);
      }
    } catch (SQLException e) {
      LOG.error(" can not find the user", e);
    }
    return valeur_retour_final;
  }

  public void setValue( String nom_table, String colonne_filtre1,int valeur_filtre1,String colonne_filtre2,int valeur_filtre2,String colonne_set,int valeur_set ) {
    try (
            Connection con = DriverManager.getConnection(
                    env.getProperty("spring.datasource-business.url"),
                    env.getProperty("spring.datasource-business.username"),
                    env.getProperty("spring.datasource-business.password")
            );
    ) {
      String sql2 = "update " + nom_table +" set  " +colonne_set +" =? where " +colonne_filtre1 +" =? and " +colonne_filtre2 +" =? ";
      LOG.info("********** SQL Request update  :   " + sql2);

      PreparedStatement stmt2 = con.prepareStatement(sql2);

      try {
        stmt2.setInt(1, valeur_set);
        stmt2.setInt(2, valeur_filtre1);
        stmt2.setInt(3, valeur_filtre2);
        stmt2.executeUpdate();
        LOG.info("********** The value : "+valeur_set+"  is added to  "+colonne_set + " for : "+colonne_filtre1+ " : "+valeur_filtre1 +" with : "+colonne_filtre2 +" : "+valeur_filtre2 );
      } catch (SQLException e) {
        LOG.error(" can not update", e);
      }
    } catch (SQLException e) {
      LOG.error(" can not connect", e);
    }
  }


}