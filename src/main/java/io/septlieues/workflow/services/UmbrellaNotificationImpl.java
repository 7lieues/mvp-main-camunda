package io.septlieues.workflow.services;


import com.google.gson.Gson;

import io.septlieues.workflow.model.UmbrellaRequest;
import org.camunda.bpm.engine.IdentityService;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

@Service
public class UmbrellaNotificationImpl implements UmbrellaNotification {

    private static final Logger LOG = LoggerFactory.getLogger(UmbrellaNotificationImpl.class);

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private RasaService rasaService;

    @Autowired
    IdentityService identityService;

    @Autowired
    private Environment env;



    @Autowired
    private Gson gson;

    @Value("${umbrella.host}")
    private String umbrellaHost;

    @Override
    public void umbrellaStartRequest(String request_id, String status, String process_ext_id, String process_definition_id, String ranking, String total_steps, String step_name,String applicant) throws Exception {
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        //requestHeaders.add("Accept", "application/json");
        UmbrellaRequest umbrellaRequest = new UmbrellaRequest(request_id, status,process_ext_id,process_definition_id, ranking, total_steps,step_name,applicant);
        System.out.println("value umbrella : "+umbrellaRequest.toString());
        HttpEntity requestHttpEntity = new HttpEntity(umbrellaRequest, requestHeaders);
        StringBuilder umbrellaURI = new StringBuilder();
        umbrellaURI.append(umbrellaHost)
                .append("/api/camunda_postback");

        ResponseEntity<Map> response = restTemplate.postForEntity(umbrellaURI.toString(), requestHttpEntity, Map.class);
        LOG.info("############# STATUS REQUEST UMBRELLA########################## " + response.getStatusCode());

        try {

        } catch (Exception e) {
            LOG.error("ERROR TO SEND A REQUEST TO UMBRELLA" + e);
        }
    }

    @Override
    public void umbrellaCompleteRequest(String request_id, Boolean approved, String status, String process_ext_id, String process_definition_id, String decision,String ranking, String total_steps,String step_name,String applicant) throws Exception {
        HttpHeaders requestHeaders = new HttpHeaders();

        requestHeaders.add("Accept", "application/json");
        UmbrellaRequest umbrellaRequest = new UmbrellaRequest(request_id,approved,status,process_ext_id,process_definition_id,decision,ranking,total_steps,step_name,applicant);
        HttpEntity requestHttpEntity = new HttpEntity(umbrellaRequest, requestHeaders);
        StringBuilder umbrellaURI = new StringBuilder();
        umbrellaURI.append(umbrellaHost)
                .append("/api/camunda_postback");

        try {
            ResponseEntity<Map> response = restTemplate.postForEntity(umbrellaURI.toString(), requestHttpEntity, Map.class);
            LOG.info("############# STATUS REQUEST UMBRELLA########################## " + response.getStatusCode());
        } catch (Exception e) {
            LOG.error("ERROR TO SEND A REQUEST TO UMBRELLA COMPLETE USER TASK " + e);
        }
    }






    @Override
    public JSONObject readLeaveRequest(String TrackerID, String Leave_type, String Start_date, String End_date) throws Exception {

        HttpHeaders requestHeader = new HttpHeaders();
        requestHeader.setContentType(MediaType.APPLICATION_JSON);
        LOG.info("Tacker ID : " + TrackerID);

        String url = umbrellaHost+"/api/leave_postback/leave_request?tracker_id=" + TrackerID + "&leave_type=" + Leave_type + "&start_date=" + Start_date + "&end_date=" + End_date;
        
        //
        HttpEntity<String> requestEntityumbrella = new HttpEntity<String>(requestHeader);
        try {
            ResponseEntity<String> umbrellaResult = restTemplate.exchange(url, HttpMethod.GET, requestEntityumbrella , String.class);

            JSONObject myJson = new JSONObject(umbrellaResult.getBody());
            return myJson;

        } catch (HttpStatusCodeException exception) {
            int statusCode = exception.getStatusCode().value();
            System.out.println("===== failure with umbrella postback ===== " + statusCode);
            System.out.println("===== failure with umbrella postback, trace detail ===== " + exception.toString());
            return null;
        } catch (Exception unk_exception) {
            System.out.println("===== failure with umbrella postback, trace detail ===== " + unk_exception.toString());
            return null;
        }
    }

    @Override
    public JSONObject createLeaveRequest(String TrackerID, String Leave_type, String Start_date, String End_date, int validation_status) throws Exception {

        HttpHeaders requestHeader = new HttpHeaders();
        requestHeader.setContentType(MediaType.APPLICATION_JSON);
        LOG.info("Tacker ID : " + TrackerID);

        String body = "{\"tracker_id\":\"" + TrackerID + "\",\"leave_type\":\"" + Leave_type + "\",\"validation_status\":" + validation_status + ",\"motive\":\"Ceci est une description\",\"start_date\":\"" + Start_date + "\",\"end_date\":\"" + End_date + "\"}";

        String url = umbrellaHost+"/api/leave_postback/leave_request";
        
        HttpEntity<String> requestEntityumbrella = new HttpEntity<String>(body, requestHeader);

        try {
            String umbrellaResult = restTemplate.postForObject(url, requestEntityumbrella, String.class);

            JSONObject myJson = new JSONObject(umbrellaResult);
            return myJson;

        } catch (HttpStatusCodeException exception) {
            int statusCode = exception.getStatusCode().value();
            System.out.println("===== failure with umbrella postback ===== " + statusCode);
            System.out.println("===== failure with umbrella postback, trace detail ===== " + exception.toString());
            return null;
        } catch (Exception unk_exception) {
            System.out.println("===== failure with umbrella postback, trace detail ===== " + unk_exception.toString());
            return null;
        }
    }

    @Override
    public JSONObject createLeaveRequestNbrDays(String TrackerID, String Leave_type, String Start_date, int nbr_days, int validation_status) throws Exception {

        HttpHeaders requestHeader = new HttpHeaders();
        requestHeader.setContentType(MediaType.APPLICATION_JSON);
        LOG.info("Tacker ID : " + TrackerID);

        String body = "{\"tracker_id\":\"" + TrackerID + "\",\"leave_type\":\"" + Leave_type + "\",\"validation_status\":" + validation_status + ",\"motive\":\"Ceci est une description\",\"start_date\":\"" + Start_date + "\",\"nbr_days\":" + nbr_days + "}";

        String url = umbrellaHost+"/api/leave_postback/leave_request";
        
        HttpEntity<String> requestEntityumbrella = new HttpEntity<String>(body, requestHeader);

        try {
            String umbrellaResult = restTemplate.postForObject(url, requestEntityumbrella, String.class);

            JSONObject myJson = new JSONObject(umbrellaResult);
            return myJson;

        } catch (HttpStatusCodeException exception) {
            int statusCode = exception.getStatusCode().value();
            System.out.println("===== failure with umbrella postback ===== " + statusCode);
            System.out.println("===== failure with umbrella postback, trace detail ===== " + exception.toString());
            return null;
        } catch (Exception unk_exception) {
            System.out.println("===== failure with umbrella postback, trace detail ===== " + unk_exception.toString());
            return null;
        }
    }

    @Override
    public JSONObject updateLeaveRequest(int leave_request_id) throws Exception {

        HttpHeaders requestHeader = new HttpHeaders();
        requestHeader.setContentType(MediaType.APPLICATION_JSON);

        String body = "{\"validation_status\":1}";

        String url = umbrellaHost+"/api/leave_postback/leave_request/" + leave_request_id;
        
        HttpEntity<String> requestEntityumbrella = new HttpEntity<String>(body, requestHeader);

        try {
            ResponseEntity<String> umbrellaResult = restTemplate.exchange(url, HttpMethod.PUT, requestEntityumbrella, String.class);

            JSONObject myJson = new JSONObject(umbrellaResult.getBody());
            return myJson;
            

        } catch (HttpStatusCodeException exception) {
            int statusCode = exception.getStatusCode().value();
            System.out.println("===== failure with umbrella postback ===== " + statusCode);
            System.out.println("===== failure with umbrella postback, trace detail ===== " + exception.toString());
            return null;
        } catch (Exception unk_exception) {
            System.out.println("===== failure with umbrella postback, trace detail ===== " + unk_exception.toString());
            return null;
        }
    }

    @Override
    public JSONObject cancelLeaveRequest(int leave_request_id) throws Exception {

        HttpHeaders requestHeader = new HttpHeaders();
        requestHeader.setContentType(MediaType.APPLICATION_JSON);

        String url = umbrellaHost+"/api/leave_postback/leave_request/" + leave_request_id;
        
        HttpEntity<String> requestEntityumbrella = new HttpEntity<String>(requestHeader);

        try {
            ResponseEntity<String> umbrellaResult = restTemplate.exchange(url, HttpMethod.DELETE, requestEntityumbrella, String.class);

            JSONObject myJson = new JSONObject(umbrellaResult.getBody());
            return myJson;

        } catch (HttpStatusCodeException exception) {
            int statusCode = exception.getStatusCode().value();
            System.out.println("===== failure with umbrella postback ===== " + statusCode);
            System.out.println("===== failure with umbrella postback, trace detail ===== " + exception.toString());
            return null;
        } catch (Exception unk_exception) {
            System.out.println("===== failure with umbrella postback, trace detail ===== " + unk_exception.toString());
            return null;
        }
    }

    @Override
    public JSONObject stockLeaveRequest(String TrackerID) throws Exception {

        HttpHeaders requestHeader = new HttpHeaders();
        requestHeader.setContentType(MediaType.APPLICATION_JSON);

        String body = "{\"tracker_id\":\"" + TrackerID + "\"}";

        String url = umbrellaHost+"/api/leave_postback/leave_stock";
        
        HttpEntity<String> requestEntityumbrella = new HttpEntity<String>(body, requestHeader);

        try {
            String umbrellaResult = restTemplate.postForObject(url, requestEntityumbrella, String.class);

            JSONObject myJson = new JSONObject(umbrellaResult);
            return myJson;

        } catch (HttpStatusCodeException exception) {
            int statusCode = exception.getStatusCode().value();
            System.out.println("===== failure with umbrella postback ===== " + statusCode);
            System.out.println("===== failure with umbrella postback, trace detail ===== " + exception.toString());
            return null;
        } catch (Exception unk_exception) {
            System.out.println("===== failure with umbrella postback, trace detail ===== " + unk_exception.toString());
            return null;
        }
    }
}
