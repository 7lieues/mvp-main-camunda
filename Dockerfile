FROM openjdk:11
#ADD target/user-0.0.1-SNAPSHOT.jar user-0.0.1-SNAPSHOT.jar
EXPOSE 8080

RUN mkdir  /app

RUN apt-get update
RUN apt install maven -y


# Copy the Project Object Model file
COPY ./pom.xml ./pom.xml

ENV MAVEN_OPTS "-Xmx1024m"
RUN mvn dependency:go-offline -B

COPY . /srv/app
WORKDIR /srv/app
RUN mvn package spring-boot:repackage



RUN ls /srv/app
RUN ls /srv/app/target

ENTRYPOINT ["java","-jar","/srv/app/target/mvp-main-camunda-1.0.0-SNAPSHOT.jar","–-trace"]